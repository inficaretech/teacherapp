<?php
error_reporting(0);
/**
*
*/
class Categories extends CI_Controller
{

	function __construct()
	{
	  parent::__construct();
	   $this->load->model('Category');
     $this->load->library('session');
		 if(empty($this->input->post_get('access_token'))){
			 if(!$this->session->userdata('logged_in'))
			 {
				 redirect('login');
			 }
		 }
	}

		function index(){
		  $data['results']=$this->Category->get_entries();
		  $this->load->view('admin/category', $data);
		}

	 function addcategory(){
        ?>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
         <form method="POST" role="form" id="up_status" action='<?= base_url(); ?>index.php/Categories/addnewcategory' enctype="multipart/form-data" >
		  <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Category</h4>

            </div>
            <div class="modal-body">
                 <div class="form-group">
                    <strong>Category</strong>
                   <input type="text" name="category" value="" required/>
               </div>
              </div>

              <div class="modal-body">
                 <div class="form-group">
                    <strong>Origional Image</strong>
                   <input type="file" name="image1" value="" required />
               </div>
              </div>

              <div class="modal-body">
                 <div class="form-group">
                    <strong>Hover Image</strong>
                   <input type="file" name="image2" value="" required />
               </div>
              </div>
              <div class="modal-footer">
             <input type="submit" class="btn btn-default" value="Submit" class="form-submit" />
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
           </form>

           <?php  }


     function addnewcategory(){
      $data['basic_image'] = $this->validateUpload();
      $data['hover_image'] = $this->validateUpload1();
      $data = array('category' => $this->input->post('category'),
                     'basic_image' => $data['basic_image'],
                     'hover_image' => $data['hover_image'],
                     'created_by' => $this->session->logged_in['id'],
                     'modified_time' => time()
                      );
        $chkdata = $this->Category->Chk_data_exist($data['category']);
        if($chkdata != '1'){
        	$insert = $this->db->insert('master_category',$data);
           $this->session->set_flashdata('flassuccess',"Inserted successfully");
         if($insert){
           $base_url=base_url();
           redirect("$base_url"."index.php/Categories/index");
          }
        }else{
          $this->session->set_flashdata('flaserror',"Already exist");
          $base_url=base_url();
          redirect("$base_url"."index.php/Categories/index");
        }

      }


    function editcategory($id){
            $originalimage_url = "../../uploads/category_images/basic_image/";
            $hoverimage_url = "../../uploads/category_images/hover_image/";
            $this->db->select('*');
            $this->db->from('master_category');
            $this->db->where('id',$id);
            $query = $this->db->get();
            $result = $query->result();
            //echo $this->db->last_query();
            if(empty($result[0]->basic_image)){
              $required = "required";
            } ?>
            // echo $html = '
         <form method="POST" role="form"  action="updatecategory/" enctype="multipart/form-data" >
          <div class="modal-dialog updatedData<?php echo $id;?>">
          <input type="hidden" name="id" value="<?php echo $id;?>">
            <!-- Modal content-->
            <div class="modal-content" id="status-popup">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit Category</h4>
              </div>
              <div class="modal-body">
                 <div><input type="text" name="category" value="<?php echo $result[0]->category; ?>" required></div>
              </div>
                <div class="modal-body">
                 <div class="form-group">
                    <strong>Origional Image</strong> <br/>
                    <?php if(!empty($result[0]->basic_image)){ ?>
                    <img src="<?php echo $originalimage_url.$result[0]->basic_image ?>"  height="50" width="50" / >
                   <?php } ?>
                   <input type="file" name="image1" value="" <?php echo $required ;?>  />
               </div>
              </div>

              <div class="modal-body">
                 <div class="form-group">
                    <strong>Hover Image</strong><br/>
                     <?php if(!empty($result[0]->hover_image)){ ?>
                    <img src="<?php echo $hoverimage_url.$result[0]->hover_image ?>"  height="50" width="50" / >
                    <?php }?>
                   <input type="file" name="image2" value="" <?php echo $required ;?>  />
               </div>
              </div>
              <div class="modal-footer">

              <input type="submit" class="btn btn-default" value="Update" class="form-submit" />
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
           </form>
        <?php } 


    function updatecategory($id){
      $this->load->library('upload');
      $data['basic_image'] = $this->validateUpload();
      $data['hover_image'] = $this->validateUpload1();
      if(empty($_FILES['image1']['tmp_name']) && empty($_FILES['image2']['tmp_name'])){
           $data = array('category' => $this->input->post('category'),
                    'created_by' => $this->session->logged_in['id'],
                     'modified_time' => time()
                      );
      }else
      if(!empty($_FILES['image1']['tmp_name']) && empty($_FILES['image2']['tmp_name'])){

            $data = array('category' => $this->input->post('category'),
                     'basic_image' => $data['basic_image'],
                     'created_by' => $this->session->logged_in['id'],
                     'modified_time' => time()
                      );
      }else
      if(!empty($_FILES['image1']['tmp_name']) && !empty($_FILES['image2']['tmp_name'])){
            $data = array('category' => $this->input->post('category'),
                     'basic_image' => $data['basic_image'],
                     'hover_image' => $data['hover_image'],
                     'created_by' => $this->session->logged_in['id'],
                     'modified_time' => time()
                      );
      }else
      if(empty($_FILES['image1']['tmp_name']) && !empty($_FILES['image2']['tmp_name'])){
            $data = array('category' => $this->input->post('category'),
                     'hover_image' => $data['hover_image'],
                     'created_by' => $this->session->logged_in['id'],
                     'modified_time' => time()
                      );
      }
       $id = $_REQUEST['id'];
       $chkdata = $this->Category->Chk_data_exist($data['category'], $id);
            if($chkdata != '1'){
										$this->db->where('id',$id);
										$this->db->update('master_category', $data);
                 //   echo $this->db->last_query();
                    // $this->db->select('*');
                    // $this->db->where('id',$id);
                    // $this->db->from('master_category');
                    // $query = $this->db->get();
                    // $result = $query->result();
    //                 echo $html = '
    // <td>'.$result[0]->category.'</td>
    // <td>
    //    <a href="javascript:void(0)" title="Edit" onclick="editcategory('.$result[0]->id.')" data-toggle="modal" data-target="#domain" class="icon-1 info-tooltip">Edit</a>
    //    <a href="javascript:void(0)" class="hide"  id="hide'.$result[0]->id.'">Please wait...</a>
		// 	 <a href="javascript:void(0)" title="Delete" onclick="deleteCategory('.$result[0]->id.')" class="icon-2 info-tooltip">Delete</a>';
        $this->session->set_flashdata('flassuccess',"Updated successfully");
			  $base_url=base_url();
			  redirect("$base_url"."index.php/Categories/index");
       }else{
              // echo "already exist";
              $this->session->set_flashdata('flaserror',"Already exist");
							// $base_url=base_url();
							// redirect("$base_url"."index.php/Categories/index");
      }
   }

    function deletecategory($id){
      $this->db->where('id',$id);
      $this->db->delete('master_category');
     }


public function validateUpload(){
    if ($_FILES AND isset($_FILES['image1']['name']) ){
        $timestamp = date("YmdHis", time());
        $config['upload_path'] = './uploads/category_images/basic_image/';
        $config['allowed_types'] = 'png|gif|jpg|jpeg';
        $config['max_size'] = '999999';
        $config['file_name'] = $timestamp."_".$_FILES['image1']['name'];
        $this->load->library("upload",$config);
        $this->upload->initialize($config);
       if(!$this->upload->do_upload("image1")){
           return $this->upload->display_errors();
        }else{
          return $config['file_name'];
        }
    }
}

public function validateUpload1(){
    if($_FILES AND isset($_FILES['image2']['name'])){
        $timestamp = date("YmdHis", time());
        $config1['upload_path'] = './uploads/category_images/hover_image/';
        $config1['allowed_types'] = 'png|gif|jpg|jpeg';
        $config1['max_size'] = '999999';
        $config1['file_name'] = $timestamp."_".$_FILES['image2']['name'];
        $this->load->library("upload",$config1);
        $this->upload->initialize($config1);
        if(!$this->upload->do_upload("image2")){
           return $this->upload->display_errors();
        }else{
          return $config1['file_name'];
        }
    }
  }

  }
