<?php
	error_reporting(0);
	defined('BASEPATH') OR exit('No direct script access allowed');
	require (APPPATH.'libraries/REST_Controller.php');

 class CategoriesAPI extends REST_Controller
 {

 	function __construct()
 	{
 	  parent::__construct();
    $this->load->model('Category');
 	}

  function get_all_categories_get(){
    $posts = array();
     $accesstoken_exist = $this->Category->chk_access_token($this->get('access_token'));
    if($accesstoken_exist == 1){
      $this->db->select('category_id');
      $this->db->from('master_subcategory');
      $this->db->group_by('category_id');
      $query1 = $this->db->get();
      $data1 = $query1->result_array();
      foreach ($data1 as $value) {
       $this->db->select('*');
       $this->db->from('master_category');
       $this->db->where('id', $value['category_id']);
       $query = $this->db->get();
       $data = $query->result_array();
       // echo $this->db->last_query();
     foreach ($data as $cat) {
      $basicurl = base_url()."uploads/category_images/basic_image/";
      $hoverurl = base_url()."uploads/category_images/hover_image/";
      $result = "";
      $result->id = $cat['id'];
      $result->category = $cat['category'];
      $result->selected = $this->Category->check_if_selected_category($this->get('teacher_id'), $cat['id']);
      if(!empty($cat['basic_image'])){
       $result->basic_image = $basicurl.$cat['basic_image'];
      }else{
        $result->basic_image = "";
       }

      if(!empty($cat['hover_image'])){
       $result->hover_image = $hoverurl.$cat['hover_image'];
      }else{
       $result->hover_image = "";
      }
      array_push($posts, $result);

     }
     }
      echo json_encode(array("result"=>$posts , "success"=> "success"));
    }else{
      // $posts  = "Session expired";
     echo json_encode(array("error"=>"Session expired"));
    }

  }


}
