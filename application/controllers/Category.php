<?php

/**
*
*/
class Category extends CI_Model
{
	function __construct()
	{
	  parent::__construct();
	}

	public function get_entries(){
 	 $query = $this->db->get('master_category');
     return $query->result();
	}

	function Chk_data_exist($category, $id){
	  $this->db->select('*');
      $this->db->from('master_category');
      $this->db->where('category',$category);
			$this->db->where('id !=',$id);
		//		echo $this->db->last_query(); die;
      $query = $this->db->get();
	  if($query->num_rows() > 0){
	  	$rows = '1';
	  }else{
	  	$rows = '0';
	  }
	  return $rows;
	}

	function chk_access_token($access_token){
	  $this->db->select('*');
      $this->db->from('access_token');
      $this->db->where('access_token',$access_token);
      $query = $this->db->get();
    // echo $this->db->last_query();
	  if($query->num_rows() > 0){
	  	$rows = '1';
	  }else{
	  	$rows = '0';
	  }
	  return $rows;
	}

}

?>
