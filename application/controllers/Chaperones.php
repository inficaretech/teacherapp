<?php
	error_reporting(0);
	defined('BASEPATH') OR exit('No direct script access allowed');
	require (APPPATH.'libraries/REST_Controller.php');

 class Chaperones extends REST_Controller
 {

 	function __construct()
 	{
 	  parent::__construct();
      $this->load->model('Chaperon');
 	}

	function get_all_get()
	{
		if(!empty($this->get('teacher_id'))){
		$accesstoken_exist = $this->Chaperon->chk_access_token($this->get('access_token'));
		if($accesstoken_exist == 1){
		$result1['result'] = $this->Chaperon->get_chaperon_entries($this->get('teacher_id'));
		if (!empty($result1)){
			$result1['success'] = ("success");
			$this->set_response($result1, REST_Controller::HTTP_OK);
		}else {
			$result2['error'] = ("nil");
			$this->set_response($result2, REST_Controller::HTTP_OK);
		}
	}else {
		$result2['error'] = ("Session expired");
		$this->set_response($result2, REST_Controller::HTTP_OK);
	}
	}else{
		$result2['error'] = ("invalid teacher_id");
		$this->set_response($result2, REST_Controller::HTTP_OK);
	}
}


function get_chaperon_assign_get()
	{
		if(!empty($this->get('teacher_id'))){
		$accesstoken_exist = $this->Chaperon->chk_access_token($this->get('access_token'));
		if($accesstoken_exist == 1){
		$result1['result'] = $this->Chaperon->get_chaperon_entries_for_group($this->get('teacher_id'));
		if (!empty($result1)){
			$result1['success'] = ("success");
			$this->set_response($result1, REST_Controller::HTTP_OK);
		}else {
			$result2['error'] = ("nil");
			$this->set_response($result2, REST_Controller::HTTP_OK);
		}
	}else {
		$result2['error'] = ("Session expired");
		$this->set_response($result2, REST_Controller::HTTP_OK);
	}
	}else{
		$result2['error'] = ("invalid teacher_id");
		$this->set_response($result2, REST_Controller::HTTP_OK);
	}
}

  function add_chaperones_post(){
  	 $this->load->helper('string');
  	 $access_code = random_string('alnum', 6);
 		$data = array(
			'name' => $this->post('name'),
			'school' => $this->post('school'),
      		'email' => $this->post('email'),
      		'contact' => $this->post('phoneno'),
      		'accesscode' => $access_code,
      		'chaperonIs' => $this->post('chaperon'),
			'created_by' => $this->post('teacher_id'),
      		'modified_time' => time()
			);
 		if(empty($this->post('teacher_id')))
   		 {
          $info->error = 'Enter teacher id';
          $this->set_response($info, REST_Controller::HTTP_OK);
    	 }elseif(empty($this->post('chaperon')))
   		 {
          $info->error = 'Enter Chaperon';
          $this->set_response($info, REST_Controller::HTTP_OK);
    	 }elseif(empty($this->post('name')))
   		 {
          $info->error = 'Enter Name';
          $this->set_response($info, REST_Controller::HTTP_OK);
    	 }
       elseif(empty($this->post('email')))
   		 {
          $info->error = 'Enter Email';
          $this->set_response($info, REST_Controller::HTTP_OK);
    	 }elseif(empty($this->post('phoneno')))
       {
          $info->error = 'Enter phoneno';
          $this->set_response($info, REST_Controller::HTTP_OK);
       }elseif((strlen($this->post('phoneno')) < 10))
       {
          $info->error = 'invalid phoneno';
          $this->set_response($info, REST_Controller::HTTP_OK);
       }else{
    	 	  $query = $this->db->select("*")
                 ->from("chaperon")
                 ->where("email",$this->post('email'))
                 ->get();
                 //echo $this->db->last_query();
   		 if ($query->num_rows() != 0){
               $info->error = 'already exist';
               $this->set_response($info, REST_Controller::HTTP_OK);
       	 }else{
       	  $accesstoken_exist = $this->Chaperon->chk_access_token($this->post('access_token'));

		   if($accesstoken_exist == 1){
             $this->db->insert('chaperon',$data);
             $id = $this->db->insert_id(); // id of last inserted
             $result1['id'] = $id;
             $result1['success'] = ("Added");
             $this->set_response($result1, REST_Controller::HTTP_OK);
      	   }else{
      	  	$info->error = 'Session expired';
        	  $this->set_response($info, REST_Controller::HTTP_OK);
       	 	 }
           }
    	 }
 	}

  function edit_chaperones_post(){
    $accesstoken_exist = $this->Chaperon->chk_access_token($this->post('access_token'));
		// $data = array();

   if($accesstoken_exist == 1){
       $this->db->select("*");
       $this->db->from('chaperon');
       $this->db->where('id',$this->post('id'));
       $query = $this->db->get();
       $result = $query->result_array();

    if (!empty($result)){


			if(empty($this->post('chaperon')))
			{
			  $chaperon = $result[0]['chaperonIs'];
			}else{
			  $chaperon = $this->post('chaperon');
			}

			if(empty($this->post('teacher_id')))
			{
			  $teacher_id = $result[0]['created_by'];
			}else{
			  $teacher_id = $this->post('teacher_id');
			}

			if(empty($this->post('name')))
			{
			  $name = $result[0]['name'];
			}else{
			  $name = $this->post('name');
			}

			if(empty($this->post('school')))
			{
			  $school = $result[0]['school'];
			}else{
			  $school = $this->post('school');
			}

			if(empty($this->post('email')))
			{
			  $email = $result[0]['email'];
			}else{
			  $email = $this->post('email');
			}

			if(empty($this->post('phoneno')))
			{
			  $phoneno = $result[0]['contact'];
			}else{
			  		if((strlen($this->post('phoneno')) > 9)){
								$phoneno = $this->post('phoneno');
						}else{
							$error_count = 1;
						}
		  }

     if ($error_count <  1){
			 $data = array(
				 'name' => $name,
				 'school' => $school,
				 'email' => $email,
				 'contact' => $phoneno,
				 'chaperonIs' => $chaperon,
				 'created_by' => $teacher_id,
				 'modified_time' => time()
			 );

			 if(!empty($data)){
				 $this->db->where('id', $result[0]['id']);
				 $this->db->update('chaperon',$data);
				 $this->db->select("*");
				 $this->db->from('chaperon');
				 $this->db->where('id',$this->post('id'));
				 $query = $this->db->get();
				 $response = $query->result();
				$posts = array();
				foreach ($response as $chepvalue) {
					$result = "";
					$result->id = $chepvalue->id;
					$result->name = $chepvalue->name;
					$result->contact = $chepvalue->contact;
					$result->email = $chepvalue->email;
					$result->school = $chepvalue->school;
					$result->chaperonIs = $chepvalue->chaperonIs;
				}
				$result1 = "updated";
				echo json_encode(array("result"=>$result , "success" => $result1));

			 }
		 }else{
			 $info->error = 'invalid phoneno';
			 $this->set_response($info, REST_Controller::HTTP_OK);
		 }

     }else{
       ###### result data
        $info->error = 'invalid id';
        $this->set_response($info, REST_Controller::HTTP_OK);
     }

    }else{
      ###### access_token
      $info->error = 'Session expired';
      $this->set_response($info, REST_Controller::HTTP_OK);
    }

  }


	function remove_chaperones_post(){
	 $data  = array('id' => $this->post('id')
									);
		$accesstoken_exist = $this->Chaperon->chk_access_token($this->post('access_token'));
		$id = $this->Chaperon->chk_id($this->post('id'));
			if($accesstoken_exist == 1){
				if($id == 1) {
					$this->db->where('id', $this->post('id'));
					$this->db->delete('chaperon');
					$result1['success'] = ("deleted");
					$this->set_response($result1, REST_Controller::HTTP_OK);
				}else{
					$info->error = 'invalid id';
					$this->set_response($info, REST_Controller::HTTP_OK);
				}
			}else{
					 $info->error = 'Session expired';
					 $this->set_response($info, REST_Controller::HTTP_OK);
					}

	}



 //  function assign_chaperon_post(){
	// 	$accesstoken_exist = $this->Chaperon->chk_access_token($this->post('access_token'));
	// 	if($accesstoken_exist == 1){
	// 	$chaperon_id=  $this->post('chaperon_id');
 //  		$group_id=	$this->post('group_id');
 //  		$type=	$this->post('type');
	// 	$teacher_id=	$this->post('teacher_id');
	// 	$count = count($chaperon_id);
	// 	$ifteacher_exist = $this->Chaperon->check_if_correct_teacher_id($this->input->post('teacher_id'));
	//  for($i=0; $i < $count; $i++){
	// 	 $ifchaperon_exist = $this->Chaperon->check_if_correct_chaperon_id($chaperon_id[$i]);
	// 	 $ifchaperon_exist_same_grp = $this->Chaperon->chek_chaperon_for_same_group($chaperon_id[$i]);
	// 	 $ifgroup_id = $this->Chaperon->check_if_correct_group_id($group_id[$i]);
 //   if($ifgroup_id == "1"){
	// 	 if($ifteacher_exist == "1"){
	// 		 if($ifchaperon_exist_same_grp == "0"){
	// 			 if($ifchaperon_exist == "1"){
	// 				 $assign_chaperon = $this->Chaperon->assign_group($chaperon_id[$i],$group_id[$i],$type[$i],$teacher_id);
	// 				 $group_data = $this->Chaperon->get_group_data($assign_chaperon);
	// 				 $chaperon_data = $this->Chaperon->get_chaperon_data($group_data->chaperon_id);
	// 				 if($chaperon_data != "0") {
	// 				 	$this->send_mail($chaperon_data->email, $chaperon_data->accesscode, $chaperon_data->name);
	// 				 }
	// 				 if($assign_chaperon != "0"){
	// 					 $result1['success'] = ("assign");
	// 				 }else{
	// 					 $info->error = 'invalid chaperon';
	// 				 }

	// 			 }else{
	// 				 $info->error = 'invalid chaperon_id';
	// 			 }
	// 		 }else{
	// 			 $info->error = 'already assign';
	// 		 }

	// 	 	}else{
	// 		 $info->error = 'invalid teacher_id';
	// 	 	}
	//  	}else{
	// 	 $info->error = 'invalid group_id';
	//   }
	//  }
	//  if(!empty($info)){
	// 	 $this->set_response($info, REST_Controller::HTTP_OK);
	//  }else{
	// 	 $this->set_response($result1, REST_Controller::HTTP_OK);
	//  }
 // }else{
	//  $info->error = 'Session expired';
	//  $this->set_response($info, REST_Controller::HTTP_OK);
 // }

 // }


	function assign_chaperon_post(){
		$accesstoken_exist = $this->Chaperon->chk_access_token($this->post('access_token'));
		if($accesstoken_exist == 1){
			$mapping_id = $this->post('mapping_id');
			$platform= $this->post('platform');
       		    $ss =  json_decode($mapping_id);
       		    $c= count($ss);
                   if($platform == "1"){
                   	foreach ($ss as $key => $value) {
                   		 $chaperon_id[$key] = $value->chaperon_id;
				 		 			 		$type[$key] = $value->type;
                   	}
                   }else{
                   	 	foreach ($mapping_id as $key => $value) {
				 		 $chaperon_id[$key] = $value['chaperon_id'];
				 		 $type[$key] = $value['type'];

				 	}
                   }


		// $chaperon_id=  $this->post('chaperon_id');
  		$group_id=	$this->post('group_id');

  		// $type=	$this->post('type');

		$teacher_id=	$this->post('teacher_id');

		 $count = count($chaperon_id);
		$ifteacher_exist = $this->Chaperon->check_if_correct_teacher_id($teacher_id);

		 $this->db->where('created_by', $teacher_id);
		$delete = $this->db->delete('group_chaperon');
				  if($delete){

	 for($i=0; $i < $count; $i++){

		 $ifchaperon_exist = $this->Chaperon->check_if_correct_chaperon_id($chaperon_id[$i]);
		 $ifchaperon_exist_same_grp = $this->Chaperon->chek_chaperon_for_same_group($chaperon_id[$i]);
		 $ifgroup_id = $this->Chaperon->check_if_correct_group_id($group_id);

   if($ifgroup_id == "1"){
		 if($ifteacher_exist == "1"){
			 if($ifchaperon_exist_same_grp == "0"){

				 if($ifchaperon_exist == "1"){

       	
					 $chaperons = $chaperon_id[$i];
					 $types = $type[$i];
					 $sql = "INSERT INTO group_chaperon (chaperon_id, group_id, type, created_by) VALUES (".$this->db->escape($chaperons).", ".$this->db->escape($group_id).",".$this->db->escape($types).", ".$this->db->escape($teacher_id).")";
					 $this->db->query($sql);
					 $assign_chaperon = $this->db->insert_id();

					 $group_data = $this->Chaperon->get_group_data($assign_chaperon);
					 $chaperon_data = $this->Chaperon->get_chaperon_data($group_data->chaperon_id);
					 if($chaperon_data != "0") {

						 $this->send_mail($chaperon_data->email, $chaperon_data->accesscode, $chaperon_data->name);
					 }
					 if($assign_chaperon != "0"){
						 $result1 = "assign";
					 }else{
						 $info = 'invalid chaperon';
					 }


				 }else{
					 $info = 'invalid chaperon_id';
				 }
			 }else{
				 $info = 'already assign';
			 }

		 	}else{
			 $info = 'invalid teacher_id';
		 	}
	 	}else{
		 $info = 'invalid group_id';
	  }
	 }
	 				 }else{
					 $info = 'server error';
				 }
	 if(!empty($info)){
	 	echo json_encode(array('result' =>$info));
		 // $this->set_response($info, REST_Controller::HTTP_OK);
	 }else{
	 		 	echo json_encode(array('result' =>$result1, "success"=> "success"));

		 // $this->set_response($result1, REST_Controller::HTTP_OK);
	 }
 }else{
	 $info->error = 'Session expired';
	 // $this->set_response($info, REST_Controller::HTTP_OK);
	 	 	echo json_encode(array('result' =>$info));

 }

}


 function send_mail($email, $access_code, $name){

                  /******** EMAIL FUNCTION *********/
                $body = $this->email_body($email, $access_code, $name);
                // print_r($body);die;
                $config = array();
                $config['useragent'] = "CodeIgniter";
                $config['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
                $config['protocol'] = "smtp";
                $config['smtp_host'] = "ssl://smtp.gmail.com";
                $config['smtp_port'] = "465";
                $config['smtp_user'] = "testingteaminficare@gmail.com";
                $config['smtp_pass'] = "inficare123";
                $config['mailtype'] = 'html';
                $config['charset']  = 'utf-8';
                $config['newline']  = "\r\n";
                $config['wordwrap'] = TRUE;
                $this->load->library('email');
                $this->email->initialize($config);
                $this->email->set_newline("\r\n");
                $this->email->from('testingteaminficare@gmail.com', 'Putnum');
                $this->email->to($email);
                $this->email->subject("Access Code");
                $this->email->message($body);
                $this->email->send();
                // $info = $this->email->print_debugger();
                /******** FINAL RESPONSE *********/
    }


 function email_body($email, $accesscode, $name){
   $body = "<!doctype html>
            <html>
            <head>

            </head>
            <body style='background:#eee;'>
            <div style=' text-align:left; width:500px; margin:0 auto; background:#fff; font-family:arial; border-radius:5px; line-height:18px;'>
            <table width='100%' border='0' cellspacing='0' cellpadding='0' style=' border-collapse:collapse; font-family:arial; border-radius:5px; '>
              <tbody>
                <tr>
                  <th style=' border-bottom:#eee solid 1px; color:#7064ca; font-size:18px; text-align:left; padding:10px; font-family:arial;'>Welcome to Teachers App</th>
                </tr>
                <tr>
                  <td style='color:#333; font-size:14px; text-align:left; padding:2px 10px; font-family:arial;'>&nbsp;</td>
                </tr>
                <tr>
                  <td style='color:#333; font-size:14px; text-align:left; padding:2px 10px; font-family:arial;'>Hi ".$name.",</td>
                </tr>
                <tr>
                  <td style='color:#333; font-size:14px; text-align:left; padding:2px 10px; font-family:arial;'>&nbsp;</td>
                </tr>
                <tr>
                  <td style='color:#333; font-size:14px; text-align:left; padding:2px 10px; font-family:arial;'>Your Access Code for Putnam TeacherApp is <b>".$accesscode.".</b> You can access your group details through this access code.
                </tr>
                <tr>
                  <td style='color:#333; font-size:14px; text-align:left; padding:2px 10px; font-family:arial;'>&nbsp;</td>
                </tr>
                <tr>
                  <td style='color:#333; font-size:14px; text-align:left; padding:2px 10px; font-family:arial;'>&nbsp;</td>
                </tr>
                <tr>
                  <td style='color:#333; font-size:14px; text-align:left; padding:2px 10px; font-family:arial;'>&nbsp;</td>
                </tr>
                <tr>
                  <td style='color:#333; font-size:14px; text-align:left; padding:2px 10px; font-family:arial;'>Cheers,<br>
           Team Putnam</td>
                </tr>
                <tr>
                  <td style='color:#333; font-size:14px; text-align:left; padding:2px 10px; font-family:arial;'>&nbsp;</td>
                </tr>

              </tbody>
            </table>
            </div>
            </body>
            </html>";

            return $body;
 }


}

?>
