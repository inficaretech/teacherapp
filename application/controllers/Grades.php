<?php
	error_reporting(0);
	defined('BASEPATH') OR exit('No direct script access allowed');
	require (APPPATH.'libraries/REST_Controller.php');

 class Grades extends REST_Controller
 {

 	function __construct()
 	{
 	  parent::__construct();
      $this->load->model('Grade');
 	}

  function index_get(){
      $accesstoken_exist = $this->Grade->chk_access_token($this->get('access_token'));
      if($accesstoken_exist == 1){
        $grades = $this->Grade->get_all();
        $this->set_response(array("result" =>$grades,"success"=>'Success'), REST_Controller::HTTP_OK);
      }else{
        $info->error = 'Session expired';
        $this->set_response($info, REST_Controller::HTTP_OK);
      }
  }


  function teacher_grades_post(){
	$accesstoken_exist = $this->Grade->chk_access_token($this->post('access_token'));
	if($accesstoken_exist == 1){
     $grade =$this->post('grades');
     $teacher_id =$this->post('teacher_id');
     if($grade > 5){
     	$info->error = 'Invalid grade';
		$this->set_response($info, REST_Controller::HTTP_OK);
     }else{
      $data = array('grade_type' => $grade);
  		$this->db->where('id', $teacher_id);
  		$this->db->update('admin', $data);
  		$info->success = 'updated';
		$this->set_response($info, REST_Controller::HTTP_OK);
		}
    }else{
		$info->error = 'Session expired';
		$this->set_response($info, REST_Controller::HTTP_OK);
	}
  }
}


?>
