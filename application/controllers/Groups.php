<?php
error_reporting(0);
class Groups extends CI_Controller
{

	function __construct()
	{
	  parent::__construct();
     $this->load->model('Group');
			$this->load->library('session');
	}

  function index(){
	  $data['results']=$this->Group->get_entries();
	  $this->load->view('admin/groups', $data);
	}


  function addgroup(){
    // $data['results']=$this->Question->get_questions_entries();
    $this->load->view('admin/addgroup');
  }

  function addnewgroup(){
     $name=$_POST['name'];
     $totalName=sizeof($name);
     for($i=0;$i<$totalName;$i++)
     {
     $name_val = $name[$i];
		 $chkdata = $this->Group->Chk_data_exist($name_val);
		  if($chkdata != '1'){
				     if (!empty($name_val)){
				       $data = array(
				         'name' => $name_val,
				         'modified_time' => time()
				       );
				      $insert = $this->db->insert('groups', $data);
				     }

      }
			else{
				$error_id[] = $name_val;
			}
	}

	if(!empty($error_id)){
		foreach ($error_id as $key => $value) {
			if (!empty($value)){
					$error_ids .= " ".$value.",";
				}
			}
			$errorshow = rtrim($error_ids, ",");
			$this->session->set_flashdata('flaserror', "Some is already exist $errorshow");
	}else {
		$this->session->set_flashdata('flassuccess', "Inserted ");
	}
    $base_url=base_url();
    redirect("$base_url"."index.php/Groups/index");

  }

  function edit($id){
         $this->db->select('*');
         $this->db->from('groups');
         $this->db->where('id',$id);
         $query = $this->db->get();
         $result = $query->result();
        ?>
      <form method="POST" role="form" id="edit_que_category<?php echo $result[0]->id; ?>">
      <div class="modal-dialog">
       <div class="modal-content">
         <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal">&times;</button>
             <h4 class="modal-title">Group Edit</h4>

         </div>
         <div class="modal-body">
            <div class="form-group">
               <strong>Name: </strong></br>
               <input type="text_area" name="name" value="<?php echo $result[0]->name; ?>" required/>
          </div>

           </div>
           <div class="modal-footer">
            <input type="button" class="btn btn-default" data-dismiss="modal" value="Update" onclick="Update(<?php echo $result[0]->id;  ?>)" class="form-submit" />
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
           </div>
         </div>
       </div>
        </form>
        <?php }

        function update($id){
            $data = array(
                          'name' => $this->input->post('name'),
                          'modified_time' => time()
                          );

						$chkdata = $this->Group->Chk_data_exist($data['name'], $id);
									if($chkdata != '1'){
                        $this->db->where('id',$id);
                        $this->db->update('groups',$data);
                        // $this->db->select('*');
                        // $this->db->where('id',$id);
                        // $this->db->from('groups');
                        // $query = $this->db->get();
                        // $result = $query->result();
                        // $result1= $this->Group->get_group_name_by_id($result[0]->id);
          //               echo $html = '
					//
          // <td>'.$result1.'</td>
          //  <td>
          //    <a href="javascript:void(0)" title="Edit" onclick="edit('.$result[0]->id.')" data-toggle="modal" data-target="#domain" class="icon-1 info-tooltip">Edit</a>
          //    <a href="javascript:void(0)" class="hide"  id="hide'.$result[0]->id.'">Please wait...</a>
          //    <a href="javascript:void(0)" title="Delete" onclick="deleteque('.$result[0]->id.')" class="icon-2 info-tooltip">Delete</a>';
						 $this->session->set_flashdata('flassuccess',"Updated successfully");
						 }else{
										// echo "already exist";
										$this->session->set_flashdata('flaserror',"Already exist");
									}

       }

      function delete($id){
          $this->db->where('id',$id);
          $this->db->delete('groups');
         }

}

?>
