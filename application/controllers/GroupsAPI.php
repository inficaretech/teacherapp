<?php
error_reporting(0);
require (APPPATH.'libraries/REST_Controller.php');

class GroupsAPI extends REST_Controller
{

	function __construct()
	{
	  parent::__construct();
     $this->load->model('Group');

	}


      function get_all_groups_get(){
        $accesstoken_exist = $this->Group->chk_access_token($this->get('access_token'));
      if($accesstoken_exist == 1){
       $data = $this->Group->get_entries();
      }else{
        $data  = "Session expired";
      }
      echo json_encode(array("result"=>$data));
      }



	function group_student_post(){
		$mapping_id = $this->post('mapping_id');
		$platform= $this->post('platform');
       		$ss =  json_decode($mapping_id);
       		$c= count($ss);
			$accesstoken_exist = $this->Group->chk_access_token($this->post('access_token'));
			if($accesstoken_exist == 1){
                if($platform == "1"){
                  foreach ($ss as $key => $value) {
                   	$student_id[$key] = $value->student_id;
				 	$group_id[$key] = $value->group_id;
                   	}
                   }else{
                   	 foreach ($mapping_id as $key => $value) {
				 	 $student_id[$key] = $value['student_id'];
				 	 $group_id[$key] = $value['group_id'];
				 	}
                   }

			$teacher_id = $this->post('teacher_id');
			$count = count($student_id);
            $correctteacherid = $this->Group->check_if_correct_teacher_id($this->post('teacher_id'));
				if($correctteacherid == "1"){
	            if($count > 0) {
		             for($i = 0; $i < $count; $i++){
							$ifstudent_exist = $this->Group->check_student_id_for_group($student_id[$i]);
	   						if($ifstudent_exist == "0"){
		              	    $ifsamestudent_forsamegroup = $this->Group->chek_student_for_same_group($student_id[$i], $group_id[$i]);
							if($ifsamestudent_forsamegroup == '0'){
								$correctstudentid = $this->Group->check_if_correct_student_id($student_id[$i]);
								$correctgroupid = $this->Group->check_if_correct_group_id($group_id[$i]);
							if($correctstudentid == '1' && $correctgroupid == '1'){
                               $inserted = $this->Group->assign_group_student_insert($student_id[$i],$group_id[$i],$teacher_id);
								if($inserted){
                              		 $success[] = 1;
									 }
								}else{
									$group_student_id_not_correct[] = 1;
									}
							}else{
                           $ifsamestudent_forsamegroup_exsist[] = 1;
											 }
										}else{
											$correctgroupid = $this->Group->check_if_correct_group_id($group_id[$i]);
											 if($correctgroupid == '1'){
												$updated = $this->Group->assign_group_student_update($student_id[$i],$group_id[$i],$teacher_id);
												if($updated){
													$success[] = 1;
												}
											}else{
													$group_student_id_not_correct[] = 1;
											}
										}
								 }
							}else{
								$invalid = "1";
							}
				  	}else{
							$teacher_id_invalid = "1";
						}
						if(!empty($group_student_id_not_correct)){
							$data  = "student and group id invalid";
							echo json_encode(array('error'=>$data));
						}elseif (!empty($ifsamestudent_forsamegroup_exsist)) {
							$data  = "student and group are already exist";
							echo json_encode(array('error'=>$data));
						}elseif (!empty($invalid)) {
							$data  = "invalid data";
							echo json_encode(array('error'=>$data));
						}elseif ($teacher_id_invalid) {
							$data  = "invalid TEACHER ID";
							echo json_encode(array('error'=>$data));
						}	elseif (!empty($success)) {
							$data  = "success";
							echo json_encode(array('success'=>$data));
						}
				 }else{
					 $data  = "Session expired";
					 echo json_encode(array('error'=>$data));
				 }
			 }



  function get_students_by_group_id_get(){
      $accesstoken_exist = $this->Group->chk_access_token($this->get('access_token'));
      if($accesstoken_exist == 1){
      	$teacher_id = $this->get('teacher_id');
      	$correctteacherid = $this->Group->check_if_correct_teacher_id($teacher_id);
		if($correctteacherid == "1"){
	        $grpid = $this->Group->check_if_correct_group_id($this->get('group_id'));
	        if($grpid == 1){  // check for correct group id
	          $data = $this->Group->get_students_by_group_id($this->get('group_id'), $teacher_id);
	         	// print_r($data);
	          if($data != "0"){
	            echo json_encode(array("result"=>$data, "success"=>"success"));
	          }else{
	            echo json_encode(array("error"=>"No student for this group"));
	          }
	        }else{
	          $data = "Incorrect group id";
	          echo json_encode(array("error"=>$data));
	        }
   		}else{
   		   $data = "invalid teacher_id";
   		   	echo json_encode(array("error"=>$data));
   		}
      }else{
        $data  = "Session expired";
        echo json_encode(array("error"=>$data));
      }
   }


  function  group_score_get(){
		$accesstoken_exist = $this->Group->chk_access_token($this->get('access_token'));
		if($accesstoken_exist == 1){
		$group_score_get = $this->Group->group_score_get_all();
		foreach ($group_score_get as $key => $value) {
				$this->db->select('*');
				$this->db->from('groups');
				$this->db->where('id',$value->group_id);
				$query1 = $this->db->get();
				$response1= $query1->result_array();
				$group_name = $response1[0]['name'];

				$this->db->select('*');
				$this->db->from('chaperon');
				$this->db->where('id',$value->cheperone_id);
				$query2 = $this->db->get();
				$response2 = $query2->result_array();
				$chaperon_name = $response2[0]['name'];
			    $result1["id"] = $value->id;
			    $result1["cheperone_id"] = $value->cheperone_id;
				$result1["group_name"] = $group_name;
				$result1["chaperon_name"] = $chaperon_name	;
				$result1["score"] = $value->score;
      			$result[] = $result1;
				}
					echo json_encode(array('result'=>$result));
				}else {
					$data  = "Session expired";
					echo json_encode(array('result'=>$data));
				}
	}

   function get_wall_of_fame_get(){
    $accesstoken_exist = $this->Group->chk_access_token($this->get('access_token'));
    if($accesstoken_exist == 1){
    }else{
          $data  = "Session expired";
          echo json_encode(array('result'=>$data));
        }

   }

	 function winner_post(){
		 $accesstoken_exist = $this->Chaperon->chk_access_token($this->post('access_token'));
		 if($accesstoken_exist == 1){
				$mapping_id = $this->post('mapping_id');
				$platform= $this->post('platform');
				$ss =  json_decode($mapping_id);
				$c= count($ss);
				if($platform == "1"){
				foreach ($ss as $key => $value) {
					$chaperon_id[$key] = $value->student_id;
					$type[$key] = $value->group_id;
				}
				}else{

				foreach ($mapping_id as $key => $value) {
					$chaperon_id[$key] = $value['student_id'];
					$type[$key] = $value['group_id'];
				}
				}

				foreach ($chaperon_id as $key => $value) {
						$this->db->select('*');
						$this->db->from('group_score');
						$this->db->where('cheperone_id',$value->chaperon_id);
						$query = $this->db->get();
						$result = $query->result_array();
						$group = $this->Group->get_group_name_by_id($result->group_id);
            $this->send_mail($group, $result->score);
				}
		 }
	 }

	 function send_mail($group, $score){

	                  /******** EMAIL FUNCTION *********/
	                $body = $this->email_boy($group, $score);
	                // print_r($body);die;
	                $config = array();
	                $config['useragent'] = "CodeIgniter";
	                $config['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
	                $config['protocol'] = "smtp";
	                $config['smtp_host'] = "ssl://smtp.gmail.com";
	                $config['smtp_port'] = "465";
	                $config['smtp_user'] = "testingteaminficare@gmail.com";
	                $config['smtp_pass'] = "inficare123";
	                $config['mailtype'] = 'html';
	                $config['charset']  = 'utf-8';
	                $config['newline']  = "\r\n";
	                $config['wordwrap'] = TRUE;
	                $this->load->library('email');
	                $this->email->initialize($config);
	                $this->email->set_newline("\r\n");
	                $this->email->from('testingteaminficare@gmail.com', 'Putnum');
	                $this->email->to($email);
	                $this->email->subject("Access Code");
	                $this->email->message($body);
	                $this->email->send();
	                // $info = $this->email->print_debugger();
	                /******** FINAL RESPONSE *********/
	    }


	 function email_boy($group, $score){
	   $body = "<!doctype html>
	            <html>
	            <head>

	            </head>
	            <body style='background:#eee;'>
	            <div style=' text-align:left; width:500px; margin:0 auto; background:#fff; font-family:arial; border-radius:5px; line-height:18px;'>
	            <table width='100%' border='0' cellspacing='0' cellpadding='0' style=' border-collapse:collapse; font-family:arial; border-radius:5px; '>
	              <tbody>
	                <tr>
	                  <th style=' border-bottom:#eee solid 1px; color:#7064ca; font-size:18px; text-align:left; padding:10px; font-family:arial;'>Welcome to Teachers App</th>
	                </tr>
	                <tr>
	                  <td style='color:#333; font-size:14px; text-align:left; padding:2px 10px; font-family:arial;'>&nbsp;</td>
	                </tr>
	                <tr>
	                  <td style='color:#333; font-size:14px; text-align:left; padding:2px 10px; font-family:arial;'>Hi ".$group.",</td>
	                </tr>
	                <tr>
	                  <td style='color:#333; font-size:14px; text-align:left; padding:2px 10px; font-family:arial;'>&nbsp;</td>
	                </tr>
	                <tr>
	                  <td style='color:#333; font-size:14px; text-align:left; padding:2px 10px; font-family:arial;'>Your Access Code for Putnam TeacherApp is <b>".$score.".</b> You can access your group details through this access code.
	                </tr>
	                <tr>
	                  <td style='color:#333; font-size:14px; text-align:left; padding:2px 10px; font-family:arial;'>&nbsp;</td>
	                </tr>
	                <tr>
	                  <td style='color:#333; font-size:14px; text-align:left; padding:2px 10px; font-family:arial;'>&nbsp;</td>
	                </tr>
	                <tr>
	                  <td style='color:#333; font-size:14px; text-align:left; padding:2px 10px; font-family:arial;'>&nbsp;</td>
	                </tr>
	                <tr>
	                  <td style='color:#333; font-size:14px; text-align:left; padding:2px 10px; font-family:arial;'>Cheers,<br>
	           Team Putnam</td>
	                </tr>
	                <tr>
	                  <td style='color:#333; font-size:14px; text-align:left; padding:2px 10px; font-family:arial;'>&nbsp;</td>
	                </tr>

	              </tbody>
	            </table>
	            </div>
	            </body>
	            </html>";

	            return $body;
	 }
}

?>
