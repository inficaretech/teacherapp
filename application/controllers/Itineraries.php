  <?php
	error_reporting(E_ALL);
	defined('BASEPATH') OR exit('No direct script access allowed');
	require (APPPATH.'libraries/REST_Controller.php');

 class Itineraries extends REST_Controller
 {

 	function __construct()
 	{
 	  parent::__construct();
    $this->load->model('Itinerary');
 	}


    function add_itinerary_post(){
        $grade =$this->post('grade');
        $teacher_id =$this->post('teacher_id');
        $itinerary_date =$this->post('itinerary_date');
        $accesstoken_exist = $this->Itinerary->chk_access_token($this->post('access_token'));
      if($accesstoken_exist == 1){
       
      if(empty($this->post('teacher_id')))
      {
       $info->error = 'Enter teacher id';
       $this->set_response($info, REST_Controller::HTTP_OK);
      }else if(empty($this->post('itinerary_date')))
      {
        $info->error = 'Enter Itinerary date';
        $this->set_response($info, REST_Controller::HTTP_OK);
      }elseif(empty($this->post('grade'))){
        $info->error = 'Enter grade';
        $this->set_response($info, REST_Controller::HTTP_OK);
      }elseif($grades > '5'){
        $info->error = 'Enter valid grade';
        $this->set_response($info, REST_Controller::HTTP_OK);
      }else{
           $chk_grade_date = $this->Itinerary->chk_grade_and_date($grade,$teacher_id,$itinerary_date);
           if($chk_grade_date == "1"){    //  to update existing field trip
                $trip_id = $this->Itinerary->chk_trip_id($grade,$teacher_id,$itinerary_date);
                 $subcat_id = $this->post('sub_category_id');
                 $subcatid = explode( ',' , $subcat_id);
                 $countid = count($subcatid);
                 for($i = 0; $i < $countid; $i++){
                  $teacher_id = $this->post('teacher_id');
                  $ifsubcat_exist = $this->Itinerary->check_if_correct_sub_category_id($subcatid[$i]);
                  $ifteacher_exist = $this->Itinerary->check_if_correct_teacher_id($teacher_id);
                  if($ifsubcat_exist != "1"){
                     $info->error = 'Invalid item id';
                      $this->set_response($info, REST_Controller::HTTP_OK);
                   }else if($ifteacher_exist != "1"){
                     $info->error = 'Invalid teacher_id';
                      $this->set_response($info, REST_Controller::HTTP_OK);
                   }else{
                       $data = array('grade_type' => $grade);
                       $this->db->where('id', $this->post('teacher_id'));
                       $this->db->update('admin', $data);
                       // echo $this->db->last_query();
                     for ($i=0; $i < $countid; $i++) {
                      $addtolist = $this->Itinerary->add_to_itinerary($this->post('teacher_id'),$subcatid[$i],$this->post('itinerary_date'),$grade , $trip_id);
                      }
                       if($addtolist == "1"){
                          $info['success'] = ("success");
                        }elseif($addtolist == "2"){
                          $info->error = 'already exist';
                        }else{
                          $info->error = 'invalid details';
                        }
                   }
                 }
              $this->set_response($info, REST_Controller::HTTP_OK);
           }else{  // to add new field trip
              $trip_id = $this->Itinerary->chk_max_trip_id($teacher_id);
              $tripnew = $trip_id+1;
              $tripname = 'Field Trip-'.$tripnew;
              $tripdata = array('name' => $tripname,
                            'admin_id' => $teacher_id,
                            'grade' => $grade);
              $inserttrip = $this->db->insert('trip', $tripdata);
              $lastid = $this->db->insert_id();
              $subcat_id = $this->post('sub_category_id');
              $subcatid = explode( ',' , $subcat_id);
              $countid = count($subcatid);
              for($i = 0; $i < $countid; $i++){
               $teacher_id = $this->post('teacher_id');
               $ifsubcat_exist = $this->Itinerary->check_if_correct_sub_category_id($subcatid[$i]);
               $ifteacher_exist = $this->Itinerary->check_if_correct_teacher_id($teacher_id);
               if($ifsubcat_exist != "1"){
                  $info->error = 'Invalid item id';
                   $this->set_response($info, REST_Controller::HTTP_OK);
                }else if($ifteacher_exist != "1"){
                  $info->error = 'Invalid teacher_id';
                   $this->set_response($info, REST_Controller::HTTP_OK);
                }else{
                    $data = array('grade_type' => $grade);
                    $this->db->where('id', $this->post('teacher_id'));
                    $this->db->update('admin', $data);
                    // echo $this->db->last_query();
                  for ($i=0; $i < $countid; $i++) {
                    $addtolist = $this->Itinerary->add_to_itinerary($this->post('teacher_id'),$subcatid[$i],$this->post('itinerary_date'),$grade , $lastid);
                   } 
                    if($addtolist == "1"){
                       $info['success'] = ("success");
                     }elseif($addtolist == "2"){
                       $info->error = 'already exist';
                     }else{
                       $info->error = 'invalid details';
                     }
                }
              }
              $this->set_response($info, REST_Controller::HTTP_OK);
           }

         }
         }else{
          $info->error = 'Session expired';
          $this->set_response($info, REST_Controller::HTTP_OK);
         }
        
    }

 	// function add_itinerary_post(){
  //     $grade =$this->post('grade');

  //   if(empty($this->post('teacher_id')))
  //     {
  //      $info->error = 'Enter teacher id';
  //      $this->set_response($info, REST_Controller::HTTP_OK);
  //     }else if(empty($this->post('itinerary_date')))
  //     {
  //       $info->error = 'Enter Itinerary date';
  //       $this->set_response($info, REST_Controller::HTTP_OK);
  //     }elseif(empty($this->post('grade'))){
  //       $info->error = 'Enter grade';
  //       $this->set_response($info, REST_Controller::HTTP_OK);
  //     }elseif($grades > '5'){
  //       $info->error = 'Enter valid grade';
  //       $this->set_response($info, REST_Controller::HTTP_OK);
  //     }else{
  //         $subcat_id = $this->post('sub_category_id');
  //         $subcatid = explode( ',' , $subcat_id);
  //         $countid = count($subcatid); 
  //         for($i = 0; $i < $countid; $i++){
  //          $teacher_id = $this->post('teacher_id');
  //          $ifsubcat_exist = $this->Itinerary->check_if_correct_sub_category_id($subcatid[$i]);
  //          $ifteacher_exist = $this->Itinerary->check_if_correct_teacher_id($teacher_id);
  //          if($ifsubcat_exist != "1"){
  //             $info->error = 'Invalid item id';
  //              $this->set_response($info, REST_Controller::HTTP_OK);
  //           }else if($ifteacher_exist != "1"){
  //             $info->error = 'Invalid teacher_id';
  //              $this->set_response($info, REST_Controller::HTTP_OK);
  //           }else{
  //             $data = array('grade_type' => $grade);
  //             $this->db->where('id', $this->post('teacher_id'));
  //             $this->db->update('admin', $data);
  //             // echo $this->db->last_query();
  //           for ($i=0; $i < $countid; $i++) { 
  //            $addtolist = $this->Itinerary->add_to_itinerary($this->post('teacher_id'),$subcatid[$i],$this->post('itinerary_date'),$grade );
  //          }
  //           if($addtolist == "1"){
  //            $info['success'] = ("success");
  //          }elseif($addtolist == "2"){
  //             $info->error = 'already exist';
  //          }else{
  //            $info->error = 'invalid details';
  //          }
           
  //         }
  //         }


  //     $this->set_response($info, REST_Controller::HTTP_OK);


  //     }
 	// }

   function remove_itinerary_post(){
    $data  = array('admin_id' => $this->post('teacher_id'),
                   'master_subcategory_id' => $this->post('sub_category_id')
                   );
       $accesstoken_exist = $this->Itinerary->chk_access_token($this->post('access_token'));
       if($accesstoken_exist == 1){
        $chkteacher = $this->Itinerary->check_if_correct_teacher_id($this->post('teacher_id'));
        $chkitem = $this->Itinerary->check_if_correct_sub_category_id($this->post('sub_category_id'));
        $chekentry = $this->Itinerary->check_if_data_present($this->post('sub_category_id'), $this->post('teacher_id'), $this->post('grade'));
         if($chkteacher == '1' && $chkitem == '1'){
           if($chekentry == '1'){
              $this->db->where('master_subcategory_id',$this->post('sub_category_id'));
              $this->db->where('admin_id',$this->post('teacher_id'));
              $this->db->delete('itinerary');
              $info->success = 'deleted';
              $this->set_response($info, REST_Controller::HTTP_OK);
            }else{
             $info->error = 'Invalid details';
             $this->set_response($info, REST_Controller::HTTP_OK);
       }
         }else{
           $info->error = 'Incorrect teacher_id or item id';
           $this->set_response($info, REST_Controller::HTTP_OK);
         }

       }else{
            $info->error = 'Session expired';
            $this->set_response($info, REST_Controller::HTTP_OK);
           }

   }

   // function current_itinerary_get(){
   //  $posts =array();
   //  $data  = array('admin_id' => $this->get('teacher_id'));
   //   $accesstoken_exist = $this->Itinerary->chk_access_token($this->get('access_token'));
   //     if($accesstoken_exist == 1){
   //      // $itinerary =  $this->Itinerary->get_itinerary_teacher($this->get('teacher_id'),$this->get('grade'));
   //      $date = $this->Itinerary->get_itinerary_date_teacher_id($this->get('teacher_id'),$this->get('grade'));
   //      $recommand_itinerary = $this->Itinerary->recom_itinerary($this->get('teacher_id'));
   //      $result->date = $date ;
   //      $result->current_itinerary=$this->Itinerary->get_itinerary_teacher($this->get('teacher_id'),$this->get('grade'), $date) ;
   //      // $result->recommand_title = $this->Itinerary->get_recom_itinerary_title();
   //      // $result->recommand_itinerary=$recommand_itinerary;
   //      array_push($posts, $result);
   //       echo json_encode(array('result'=>$posts, 'success'=>'success'));
   //      // $this->set_response($result, REST_Controller::HTTP_OK);
   //     }else{
   //          $info->error = 'Session expired';
   //          $this->set_response($info, REST_Controller::HTTP_OK);
   //         }
   // }



   function current_itinerary_get(){
    $posts =array();
    $data  = array('admin_id' => $this->get('teacher_id'));
     $accesstoken_exist = $this->Itinerary->chk_access_token($this->get('access_token'));
       if($accesstoken_exist == 1){
          $this->db->select('*');
          $this->db->from('itinerary');
          $this->db->where('admin_id' , $this->get('teacher_id'));
          $this->db->where('grade' , $this->get('grade'));
          $this->db->group_by('itinerary_datetime');
          $query = $this->db->get();
          $data = $query->result_array();
          foreach ($data as $value) {
            $result = "";
            $current_itenery = $this->Itinerary->get_itinerary_teacher($this->get('teacher_id'),$this->get('grade'),  $value['itinerary_datetime']) ;
            if(!empty($current_itenery)){
              $result->date = $value['itinerary_datetime'];
              $result->current_itinerary = $current_itenery;
              array_push($posts, $result);
            }
            
          }
          $recommand_title = $this->Itinerary->get_recom_itinerary_title();
          $recommand_itinerary=$this->Itinerary->recom_itinerary($this->get('teacher_id'));;
         echo json_encode(array('result'=>$posts,'recommand_title' =>$recommand_title, 'recommand_itinerary' =>$recommand_itinerary, 'success'=>'success'));
        // $this->set_response($result, REST_Controller::HTTP_OK);
       }else{
            $info->error = 'Session expired';
            $this->set_response($info, REST_Controller::HTTP_OK);
           }
   }

   function save_itinerary_post(){
      $data  = array('itinerary_datetime' => $this->post('datetime'));
      $accesstoken_exist = $this->Itinerary->chk_access_token($this->post('access_token'));
       if($accesstoken_exist == 1){
          $this->db->where_in('master_subcategory_id', $this->post('item_id'));
          $this->db->update('itinerary', $data);
          $result->result= "saved";
          $this->set_response($result, REST_Controller::HTTP_OK);
       }else{
            $info->error = 'Session expired';
            $this->set_response($info, REST_Controller::HTTP_OK);
           }
   }

   function recomanded_itinerary_get(){
     $accesstoken_exist = $this->Itinerary->chk_access_token($this->get('access_token'));
        if($accesstoken_exist == 1){
          $result['result'] = $this->Itinerary->recom_itinerary();
          $this->set_response($result, REST_Controller::HTTP_OK);
       }else{
            $info->error = 'Session expired';
            $this->set_response($info, REST_Controller::HTTP_OK);
           }
   }

 }


?>
