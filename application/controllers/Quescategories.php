<?php
error_reporting(0);
/**
*
*/
class Quescategories extends CI_Controller
{

	function __construct()
	{
	  parent::__construct();
	   $this->load->model('Quescategory');
		 $this->load->library('session');
		if(empty($this->input->post_get('access_token'))){
			if(!$this->session->userdata('logged_in'))
			{
				redirect('login');
			}
		}
	}

	function index(){
	  $data['results']=$this->Quescategory->get_all_entries_with_category_name();
	  $this->load->view('admin/quescategory', $data);
	}

	 function add(){
     $data =$this->Quescategory->get_all_categories();

        ?>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
         <form method="POST" role="form" id="up_status" action='<?= base_url(); ?>index.php/Quescategories/addnew' enctype="multipart/form-data" >
		  <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Question Category</h4>

            </div>
            <div class="modal-body">
            <div class="form-group">
                    <strong>Master Category</strong> </br>
                   <select name="category">
                    <?php  foreach($data as $value){ ?>
                    <option value="<?php echo $value->id; ?>"><?php echo $value->category; ?></option>
                    <?php } ?>
                   </select>
               </div>
                 <div class="form-group">
                    <strong>Question Category</strong>
                   <input type="text" name="ques_category" value="" />
               </div>
              </div>
              <div class="modal-footer">
            <input type="submit" class="btn btn-default" value="Submit" class="form-submit" />
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
           </form>

           <?php  }


     function addnew(){
      	$data = array('master_category_id' => $this->input->post('category'),
                      'question_category_name' => $this->input->post('ques_category'),
      				        'created_by' => $this->session->logged_in['id'],
              		    'modified_time' => time()
      		            );
		$chkdata = $this->Quescategory->Chk_data_exist($data['question_category_name']);
			if($chkdata != '1'){
      	$insert = $this->db->insert('master_question_category',$data);
        if($insert){
					$this->session->set_flashdata('flassuccess',"Inserted successfully");

         $base_url=base_url();
         redirect("$base_url"."index.php/Quescategories/index");
        }
			}else{
				$this->session->set_flashdata('flaserror',"Already exist");
				$base_url=base_url();
				redirect("$base_url"."index.php/Quescategories/index");
			}
      }


     function edit($id){
            $this->db->select('*');
            $this->db->from('master_question_category');
            $this->db->where('id',$id);
            $query = $this->db->get();
            $result = $query->result();
           ?>
         <form method="POST" role="form" id="edit_que_category<?php echo $result[0]->id; ?>">
         <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Question category</h4>

            </div>
            <div class="modal-body">
              <div class="form-group">
                    <strong>Master Category</strong> </br>
                    <?php echo $this->Quescategory->get_category_name_by_id($result[0]->master_category_id); ?>
               </div>
                 <div class="form-group">
                    <strong>Question Category</strong></br>
                    <input type="text" name="ques_category" value="<?php echo $result[0]->question_category_name; ?>" required/>
               </div>


              </div>
              <div class="modal-footer">
               <input type="button" class="btn btn-default" data-dismiss="modal" value="Update" onclick="Update(<?php echo $result[0]->id;  ?>)" class="form-submit" />
               <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
           </form>
           <?php }


    function update($id){
       $data = array('question_category_name'=>$this->input->post('ques_category'),
                    );

						$chkdata = $this->Quescategory->Chk_data_exist($data['question_category_name'], $id);
							if($chkdata != '1'){
                    $this->db->where('id',$id);
                    $this->db->update('master_question_category',$data);
                  // echo $this->db->last_query();
    //                 $this->db->select('*');
    //                 $this->db->where('id',$id);
    //                 $this->db->from('master_question_category');
    //                 $query = $this->db->get();
    //                 $result = $query->result();
    //                 echo $html = '
    // <td>'.$this->Quescategory->get_category_name_by_id($result[0]->master_category_id).'</td>
    // <td>'.$result[0]->question_category_name.'</td>
    //  <td>
    //    <a href="javascript:void(0)" title="Edit" onclick="edit('.$result[0]->id.')" data-toggle="modal" data-target="#domain" class="icon-1 info-tooltip">Edit</a>
    //    <a href="javascript:void(0)" class="hide"  id="hide'.$result[0]->id.'">Please wait...</a>
		// 	 <a href="javascript:void(0)" title="Delete" onclick="deleteque('.$result[0]->id.')" class="icon-2 info-tooltip">Delete</a>';
			 $this->session->set_flashdata('flassuccess',"Updated successfully");
		 }else{
			 // echo "already exist";
			 $this->session->set_flashdata('flaserror',"Already exist");
		 }
   }

    function delete($id){
      $this->db->where('id',$id);
      $this->db->delete('master_question_category');
     }

  }
