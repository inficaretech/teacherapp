<?php
error_reporting(0);
class Questions extends CI_Controller
{

	function __construct()
	{
	  parent::__construct();
	   $this->load->model('Quescategory');
     $this->load->model('Question');
		 $this->load->library('session');
		if(empty($this->input->post_get('access_token'))){
			if(!$this->session->userdata('logged_in'))
			{
				redirect('login');
			}
		}

	}

	function index(){
	  $data['results']=$this->Question->get_questions_entries();
	  $this->load->view('admin/questions', $data);
	}


  function add(){
    $data =$this->Question->get_entries();
    $gradetype = $this->config->item('grade_array'); 
     ?>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
        <form method="POST" role="form" id="up_status" action='<?= base_url(); ?>index.php/Questions/addnew_question' enctype="multipart/form-data" >
     <div class="modal-dialog">
         <div class="modal-content">
           <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title">Question</h4>
           </div>
           <div class="modal-body">
           <div class="form-group">
                   <strong>Question Category: </strong>
                  <select name="category" class="form-control" required>
                   <?php  foreach($data as $value){ ?>
                   <option value="<?php echo $value->id; ?>"><?php echo $value->question_category_name; ?></option>
                   <?php } ?>
                  </select>
              </div>
              <div class="form-group">
                   <strong>Grade Level</strong> </br>
                   <select name="grade_level" class="form-control" required>
                    <?php for($i= 1; $i<= 5; $i++){?>
                    <option value="<?php echo $i;?>"><?php echo $gradetype[$i]?></option>
                    <?php }?>
                   </select>
               </div>
                <div class="form-group">
                   <strong>Question: </strong>
                  <input type="text" name="question" maxlength="100" value="" required class="form-control"/>
                </div>
                <div class="form-group">
                  <strong>Answer 1:</strong>
                  <input type="text" name="answer1" maxlength="70" value="" required class="form-control"/>
                </div>
                <div class="form-group">
                  <strong>Answer 2:</strong>
                  <input type="text" name="answer2" maxlength="70" value="" required class="form-control"/>
                </div>
                <div class="form-group">
                  <strong>Answer 3:</strong>
                  <input type="text" name="answer3" maxlength="70" value="" required class="form-control"/>
                </div>
                <div class="form-group">
                  <strong>Answer 4:</strong>
                  <input type="text" name="answer4" maxlength="70" value="" required class="form-control"/>
                </div>
               <div class="form-group">
                      <strong>Correct Answer</strong>
                     <select name="answer_option" required class="form-control">
                       <option value="1">1</option>
                       <option value="2">2</option>
                       <option value="3">3</option>
                       <option value="4">4</option>
                     </select>
                 </div>
                 <div class="form-group">
                   <strong>Marks:</strong>
                   <input type="text" name="marks" maxlength="6"  pattern="[0-9]+([\,|\.][0-9]+)?"  title="Please enter valid marks" value="" required class="form-control" />
                 </div>
             <div class="modal-footer">
           <input type="submit" class="btn btn-default" value="Submit" class="form-submit" />
             <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
             </div>
           </div>
         </div>
          </form>

          <?php  }


      function addnew_question(){
          $data = array('master_question_category_id' => $this->input->post('category'),
                        'grade_type' => $this->input->post('grade_level'),
                        'question' => $this->input->post('question'),
                        'ans_opt1' => $this->input->post('answer1'),
                        'ans_opt2' => $this->input->post('answer2'),
                        'ans_opt3' => $this->input->post('answer3'),
                        'ans_opt4' => $this->input->post('answer4'),
                        'correct_answer' => $this->input->post('answer_option'),
                        'marks' => $this->input->post('marks'),
                        'created_by' => $this->session->logged_in['id'],
                        'modified_time' => time()
                        );
			 $chkdata = $this->Question->Chk_data_exist($data['question']);
	 			if($chkdata != '1'){
             $insert = $this->db->insert('questions',$data);
             if($insert){
							$this->session->set_flashdata('flassuccess',"Inserted successfully");
              $base_url=base_url();
              redirect("$base_url"."index.php/Questions/index");
             }
					 }else{
						$this->session->set_flashdata('flaserror',"Already exist");
						$base_url=base_url();
						redirect("$base_url"."index.php/Questions/index");
					}
           }

   function edit($id){
            $this->db->select('*');
            $this->db->from('questions');
            $this->db->where('id',$id);
            $query = $this->db->get();
            $result = $query->result();
            $data =$this->Question->get_entries();
           ?>
           <form method="POST" role="form" id="edit_que_category<?php echo $result[0]->id; ?>">
           <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Question category</h4>
              </div>
              <div class="modal-body">
              <div class="form-group">
                         <strong>Question Category: </strong>
                        <select name="category" class="form-control" required>
                         <?php  foreach($data as $value){ ?>
                         <option value="<?php echo $value->id; ?>" <?php if( $value->id == $result[0]->master_question_category_id){ echo "selected";}?>><?php echo $value->question_category_name; ?></option>
                         <?php } ?>
                        </select>
                    </div>
                    <?php $gradetype = $this->config->item('grade_array'); ?>
            <div class="form-group">
                <strong>Grade type</strong> </br>
                <?php if(!empty($result[0]->grade_type)){?>
                <input type="hidden" name= "grade_level" value="<?php echo $result[0]->grade_type?>">
                <?php echo $gradetype[$result[0]->grade_type];?>
                <?php }else{ ?>
                <select name="grade_level" class="form-control" required>
                <?php for($i= 1; $i<= 5; $i++){?>
                <option value="<?php echo $i;?>"><?php echo $gradetype[$i]?></option>
                <?php }?>
               </select>
               <?php }?>
           </div>
                 <div class="form-group">
                    <strong>Question: </strong></br>
                    <input type="text_area" class="form-control" name="question" value="<?php echo $result[0]->question; ?>" required/>
               </div>
               <div class="form-group">
                  <strong>Answer 1:</strong></br>
                  <input type="text" class="form-control" name="answer1" value="<?php echo $result[0]->ans_opt1; ?>" required/>
             </div>
             <div class="form-group">
                <strong>Answer 2:</strong></br>
                <input type="text" class="form-control" name="answer2" value="<?php echo $result[0]->ans_opt2; ?>" required/>
           </div>
           <div class="form-group">
              <strong>Answer 3:</strong></br>
              <input type="text" class="form-control" name="answer3" value="<?php echo $result[0]->ans_opt3; ?>" required/>
           </div>
           <div class="form-group">
                 <strong>Answer 4:</strong></br>
                 <input type="text" class="form-control" name="answer4" value="<?php echo $result[0]->ans_opt4; ?>" required/>
            </div>
            <div class="form-group">
                <strong>Correct Answer</strong>
                <select name="answer_option" class="form-control" required>
                  <option value="1" <?php if($result[0]->correct_answer == 1){ echo "selected";} ?>>1</option>
                  <option value="2"<?php if($result[0]->correct_answer == 2){ echo "selected";} ?>>2</option>
                  <option value="3"<?php if($result[0]->correct_answer == 3){ echo "selected";} ?>>3</option>
                  <option value="4"<?php if($result[0]->correct_answer == 4){ echo "selected";} ?>>4</option>
                </select>
            </div>
            <div class="form-group">
                     <strong>Marks:</strong></br>
                     <input type="text" class="form-control" name="marks" value="<?php echo $result[0]->marks; ?>" required/>
              </div>
                </div>
                <div class="modal-footer">
                 <input type="button" class="btn btn-default" data-dismiss="modal" value="Update" onclick="Update(<?php echo $result[0]->id;  ?>)" class="form-submit" />
                 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
             </form>
             <?php }


          function update($id){
              $data = array('master_question_category_id' => $this->input->post('category'),
                            'grade_type' => $this->input->post('grade_level'),
                            'question' => $this->input->post('question'),
                            'ans_opt1' => $this->input->post('answer1'),
                            'ans_opt2' => $this->input->post('answer2'),
                            'ans_opt3' => $this->input->post('answer3'),
                            'ans_opt4' => $this->input->post('answer4'),
                            'correct_answer' => $this->input->post('answer_option'),
                            'marks' => $this->input->post('marks'),
                            'created_by' => $this->session->logged_in['id'],
                            'modified_time' => time()
                            );
							echo		$chkdata = $this->Question->Chk_data_exist($data['question'], $id);
										if($chkdata != '1'){
                          $this->db->update('questions',$data);
										   		$this->db->where('id',$id);
                          echo $this->db->last_query(); die;
            //               $this->db->select('*');
            //               $this->db->where('id',$id);
            //               $this->db->from('questions');
            //               $query = $this->db->get();
            //               $result = $query->result();
            //               $result1= $this->Question->get_question_name_by_id($result[0]->id);
            //               $result2 =  $this->Question->get_ques_category_name_by_id($result[0]->master_question_category_id);
            //               echo $html = '
						//
            // <td>'.$result1.'</td>
            // <td>'.$result2.'</td>
            //  <td>
            //    <a href="javascript:void(0)" title="Edit" onclick="edit('.$result[0]->id.')" data-toggle="modal" data-target="#domain" class="icon-1 info-tooltip">Edit</a>
            //    <a href="javascript:void(0)" class="hide"  id="hide'.$result[0]->id.'">Please wait...</a>
						// 	 <a href="javascript:void(0)" title="Delete" onclick="deleteque('.$result[0]->id.')" class="icon-2 info-tooltip">Delete</a>';
							 $this->session->set_flashdata('flassuccess',"Updated successfully");
						 }else{
							 // echo "already exist";
							 $this->session->set_flashdata('flaserror',"Already exist");
						 }
         }

          function delete($id){
            $this->db->where('id',$id);
            $this->db->delete('questions');
           }


			//
      // function get_question_detail(){
      //    $accesstoken_exist = $this->Question->chk_access_token($this->post('access_token'));
      //  if($accesstoken_exist == 1){
      //   $data =$this->Question->get_all_ques();
      //  }else{
      //    $data  = "Session expired";
      //  }
      //  echo json_encode(array("result"=>$data));
      // }


      // function finish_quiz(){
      //   $posts = array();
      //    $accesstoken_exist = $this->Question->chk_access_token($this->input->post('access_token'));
      //    $data = array('group_id' => $this->input->post('group_id'),
      //                  'cheperone_id' => $this->input->post('cheperone_id'),
      //                  'score' => $this->input->post('score') ,
      //                  'modified_time' => time());
      //    if(empty($this->input->post('group_id')))
      //    {
      //     $result1->error = ("group_id required");
      //     array_push($posts, $result1);
      //    }
			//
      //    if(empty($this->input->post('cheperone_id')))
      //    {
      //     $result->error = ("cheperone_id required");
      //     array_push($posts, $result);
      //    }
			//
      //    if(empty($this->input->post('score')))
      //    {
      //     $result2->error = ("score required");
      //     array_push($posts, $result2);
      //    }
      //    $correctchepreonid = $this->Question->check_if_correct_chepron_id($this->input->post('cheperone_id'));
      //    $correctgroupid = $this->Question->check_if_correct_group_id($this->input->post('group_id'));
      //   if($correctchepreonid == 1){
      //    }else{
      //     $result3->error = ("Incorrect cheperone_id");
      //     array_push($posts, $result3);
      //    }
			//
      //   if($correctgroupid == 1){
      //    }else{
      //     $result4->error = ("Incorrect group_id");
      //     array_push($posts, $result4);
      //    }
			//
      //  if(!empty($posts)){
      //   echo json_encode(array("result"=>$posts));
      //  }
      //   if(empty($posts)){
      //  if($accesstoken_exist == 1){
      //       $this->db->insert('group_score',$data);
      //       $result['success'] = ("Added");
      //       echo json_encode(array("result"=>$result));
			//
      //  }else{
      //    $error  = "Session expired";
      //    echo json_encode(array("result"=>$error));
			//
      //  }
      //  }
			//
			//
      // }
}

?>
