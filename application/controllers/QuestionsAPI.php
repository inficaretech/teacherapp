<?php
	error_reporting(0);
	defined('BASEPATH') OR exit('No direct script access allowed');
	require (APPPATH.'libraries/REST_Controller.php');

 class QuestionsAPI extends REST_Controller
 {

	function __construct()
	{
	  parent::__construct();
	   $this->load->model('Quescategory');
     $this->load->model('Question');
	}

      function get_question_detail_get(){
         $accesstoken_exist = $this->Question->chk_access_token($this->get('access_token'));
       if($accesstoken_exist == 1){
        $data =$this->Question->get_all_ques($this->get('teacher_id'));
         echo json_encode(array("result"=>$data, "success"=> "success"));
       }else{
         $data  = "Session expired";
          echo json_encode(array("error"=>$data));
       }
      
      }


      function finish_quiz(){
        $posts = array();
         $accesstoken_exist = $this->Question->chk_access_token($this->post('access_token'));
         $data = array('group_id' => $this->post('group_id'),
                       'cheperone_id' => $this->post('cheperone_id'),
                       'score' => $this->post('score') ,
                       'modified_time' => time());
         if(empty($this->post('group_id')))
         {
          $result1->error = ("group_id required");
          array_push($posts, $result1);
         }

         if(empty($this->post('cheperone_id')))
         {
          $result->error = ("cheperone_id required");
          array_push($posts, $result);
         }

         if(empty($this->post('score')))
         {
          $result2->error = ("score required");
          array_push($posts, $result2);
         }
         $correctchepreonid = $this->Question->check_if_correct_chepron_id($this->post('cheperone_id'));
         $correctgroupid = $this->Question->check_if_correct_group_id($this->post('group_id'));
        if($correctchepreonid == 1){
         }else{
          $result3->error = ("Incorrect cheperone_id");
          array_push($posts, $result3);
         }

        if($correctgroupid == 1){
         }else{
          $result4->error = ("Incorrect group_id");
          array_push($posts, $result4);
         }

       if(!empty($posts)){
        echo json_encode(array("error"=>$posts));
       }
        if(empty($posts)){
       if($accesstoken_exist == 1){
            $this->db->insert('group_score',$data);
            $result['success'] = ("Added");
            echo json_encode(array("result"=>$result, "success"=>"success"));
       }else{
         $error  = "Session expired";
         echo json_encode(array("error"=>$error));
       }
       }

      }

      function check_answer_post(){
        if($this->Question->chk_access_token($this->post('access_token')) == "1"){
          $attempt = $this->post('attempt');
          $question_id = $this->post('question_id');
          $answer = $this->post('answer');
          $group_id = $this->post('group_id');
          $cheperone_id = $this->post('cheperone_id');
          $correct_answer = $this->Question->get_correct_ans_by_question_id($question_id);
          $group_score = $this->Question->get_group_total_score($group_id, $cheperone_id);
          if($this->Question->check_if_correct_question_id($question_id) != "1"){
            $error  = "Invalid question id";
            echo json_encode(array("error"=>$error));
          }else{ 
             if($attempt == 1){
                if($answer == $correct_answer){
                   $result  = $this->Question->get_marks_by_question_id($question_id);
                   $total_score = $result+$group_score;
                   $groupscores = $this->Question->update_group_scores($group_id, $cheperone_id,$total_score);
                   echo json_encode(array("result"=>$result, "success" => "success"));
                }else{
                   $error  = "Try again";
                   echo json_encode(array("error"=>$error));
                } 

             }elseif($attempt == 2){
                if($answer == $correct_answer){
                   $result  = $this->Question->get_half_marks_by_question_id($question_id);
                   $total_score = $result+$group_score;
                   $groupscores = $this->Question->update_group_scores($group_id, $cheperone_id,$total_score);
                   
                   echo json_encode(array("result"=>$result, "success" => "success"));
                }else{
                   $error  = "Next question";
                   echo json_encode(array("error"=>$error));
                } 
             }else{
               $error  = "Wrong attempt";
               echo json_encode(array("error"=>$error));
             }
          }

        }else{
         $error  = "Session expired";
         echo json_encode(array("error"=>$error));
        }
      }
}

?>
