<?php
error_reporting(0);
class RecommandedItems extends CI_Controller
{

	function __construct()
	{
	  parent::__construct();
		$this->load->model('RecommandedItem');
    $this->load->model('Subcategory');
		$this->load->library('session');
		if(empty($this->input->post_get('access_token'))){
			if(!$this->session->userdata('logged_in'))
			{
				redirect('login');
			}
		}
	}


	function index(){
    $data['results'] =$this->RecommandedItem->get_entries();
	  $this->load->view('admin/recommandeditems', $data);
	}

	function additem(){
		$data1 =$this->RecommandedItem->Chk_data_exist();
      if($data1 == false){
				$data2 =$this->Subcategory->get_entries();
				?>
				<script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
				<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
				<form method="POST" role="form" id="up_status" action='<?= base_url(); ?>index.php/RecommandedItems/addnewitem' enctype="multipart/form-data" >
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title">Recommanded Items</h4>

							</div>
							<div class="modal-body">
								<div class="form-group">
									<strong>Title: </strong>
									<input type="text" name="title" value="" required/>
								</div>

								<strong>Select Recommended Category: </strong></br>
								<div class = "check-scroll">
									<div class="form-group">
										<?php  foreach($data as $value){ ?>
											<input type="checkbox" name="check_list[]" value="<?php echo $value->id; ?>"><?php echo $value->title; ?></br>
											<?php } ?>
										</div>
									</div>

									<div class="modal-footer">
										<input type="submit" class="btn btn-default" value="Submit" class="form-submit" />
										<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<input type="submit" class="btn btn-default" value="Submit" class="form-submit" />
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</form>

			<?php  }

			}

	function addnewitem(){
		$data =$this->RecommandedItem->Chk_data_exist();
		  if($data == false){
				$name=$_POST['check_list'];
				$title = $this->input->post('title');
				$totalName=sizeof($name);
				for($i=0;$i<$totalName;$i++)
				{
					$name_val = $name[$i];
					if (!empty($name_val)){
						$data = array(
							'rec_title' => $title,
							'master_subcategory_id' => $name_val,
							'modified_time' => time()
						);
						$insert = $this->db->insert('recommanded_items', $data);
						$base_url=base_url();
						redirect($base_url."index.php/Categories/index");
					}
				}
			}

     }



		 function edit($id){
	          $this->db->select('*');
	          $this->db->from('recommanded_items');
	          $this->db->where('id',$id);
	          $query = $this->db->get();
	          $result = $query->result();
	         ?>
	       <form method="POST" role="form" id="edit_que_category<?php echo $result[0]->id; ?>">
	       <div class="modal-dialog">
	        <div class="modal-content">
	          <div class="modal-header">
	              <button type="button" class="close" data-dismiss="modal">&times;</button>
	              <h4 class="modal-title">Group Edit</h4>

	          </div>
	          <div class="modal-body">
	             <div class="form-group">
	                <strong>Name: </strong></br>
	                <input type="text_area" name="name" value="<?php echo $result[0]->rec_title; ?>" required/>
	           </div>

	            </div>
	            <div class="modal-footer">
	             <input type="button" class="btn btn-default" data-dismiss="modal" value="Update" onclick="Update(<?php echo $result[0]->id;  ?>)" class="form-submit" />
	             <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	            </div>
	          </div>
	        </div>
	         </form>
	         <?php }

	         function update($id){
	             $data = array(
	                           'name' => $this->input->post('name'),
	                           'modified_time' => time()
	                           );

	 						$chkdata = $this->Group->Chk_data_exist($data['name'], $id);
	 									if($chkdata != '1'){
	                         $this->db->where('id',$id);
	                         $this->db->update('groups',$data);
	                         // $this->db->select('*');
	                         // $this->db->where('id',$id);
	                         // $this->db->from('groups');
	                         // $query = $this->db->get();
	                         // $result = $query->result();
	                         // $result1= $this->Group->get_group_name_by_id($result[0]->id);
	           //               echo $html = '
	 					//
	           // <td>'.$result1.'</td>
	           //  <td>
	           //    <a href="javascript:void(0)" title="Edit" onclick="edit('.$result[0]->id.')" data-toggle="modal" data-target="#domain" class="icon-1 info-tooltip">Edit</a>
	           //    <a href="javascript:void(0)" class="hide"  id="hide'.$result[0]->id.'">Please wait...</a>
	           //    <a href="javascript:void(0)" title="Delete" onclick="deleteque('.$result[0]->id.')" class="icon-2 info-tooltip">Delete</a>';
	 						 $this->session->set_flashdata('flassuccess',"Updated successfully");
	 						 }else{
	 										// echo "already exist";
	 										$this->session->set_flashdata('flaserror',"Already exist");
	 									}

	        }

	       function delete($id){
	           $this->db->where('id',$id);
	           $this->db->delete('groups');
	          }



}

?>
