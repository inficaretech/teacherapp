<?php
	error_reporting(0);
	defined('BASEPATH') OR exit('No direct script access allowed');
	require (APPPATH.'libraries/REST_Controller.php');

 class Students extends REST_Controller
 {

 	function __construct()
 	{
 	  parent::__construct();
      $this->load->model('Student');
 	}


	function get_all_get()
	{
    if(!empty($this->get('teacher_id'))){
			$accesstoken_exist = $this->Student->chk_access_token($this->get('access_token'));
			if($accesstoken_exist == 1){
				$result1['result'] = $this->Student->get_students_entries($this->get('teacher_id'));
				if (!empty($result1)){
					$result1['success'] = ("success");
					$this->set_response($result1, REST_Controller::HTTP_OK);
				}else {
					$result2['error'] = ("nil");
					$this->set_response($result2, REST_Controller::HTTP_OK);
				}
			}else {
				$result2['error'] = ("Session expired");
				$this->set_response($result2, REST_Controller::HTTP_OK);	}
		}else{
			$result2['error'] = ("invalid teacher_id");
			$this->set_response($result2, REST_Controller::HTTP_OK);
		}
	}


  function add_students_post(){
    //  print_r(getallheaders());die;
		 $file = $_FILES[csv][tmp_name];
		//  $error_id = array();
    if(empty($file)){
		 ###### without CSV ######
 		$data = array(
			'f_name' => $this->post('first_name'),
			'l_name' => $this->post('last_name'),
      		'class' => $this->post('class'),
     		'contact1' => $this->post('ice_contact_1'),
    		'contact2' => $this->post('ice_contact_2'),
			'created_by' => $this->post('teacher_id'),
            'modified_time' => time()
			);

 		if(empty($this->post('teacher_id')))
   		 {
          $info->error = 'Enter teacher id';
          $this->set_response($info, REST_Controller::HTTP_OK);
    	 }elseif(empty($this->post('first_name')))
   		 {
          $info->error = 'Enter first name';
          $this->set_response($info, REST_Controller::HTTP_OK);
    	 }elseif(empty($this->post('last_name')))
   		 {
          $info->error = 'Enter last name';
          $this->set_response($info, REST_Controller::HTTP_OK);
    	 }
      //  elseif(empty($this->post('class')))
   		 // {
      //     $info->error = 'Enter class';
      //     $this->set_response($info, REST_Controller::HTTP_OK);
    	 // }
    	 // elseif(empty($this->post('ice_contact_1')) and empty($this->post('ice_contact_2')))
      //  {
      //     $info->error = 'Enter ice contact';
      //     $this->set_response($info, REST_Controller::HTTP_OK);
      //  }
			//  elseif((!empty($this->post('ice_contact_1'))) and (!empty($this->post('ice_contact_2'))))
      //  {
			// 	 if(($this->post('ice_contact_1')) == ($this->post('ice_contact_2'))){
			// 		 $info->error = 'contanct are same';
			// 		 $this->set_response($info, REST_Controller::HTTP_OK);
			// 	 }
			 //
      //  }
			//  elseif(!empty($this->post('ice_contact_1')))
      //  {
			// 	  if(($this->post('ice_contact_1') > 10) and ($this->post('ice_contact_1') > 15))
      //     {
			// 			$info->error = 'Contact length invalid';
			// 			$this->set_response($info, REST_Controller::HTTP_OK);
			// 		}
			 //
      //  }
			//  elseif(!empty($this->post('ice_contact_2')))
      //  {
			// 	  if(($this->post('ice_contact_2') > 10) and ($this->post('ice_contact_2') > 15))
      //     {
			// 			$info->error = 'Contact length invalid';
			// 			$this->set_response($info, REST_Controller::HTTP_OK);
			// 		}
			 //
      //  }
			//  elseif(empty($this->post('photography_allowed')))
	    // 		 {
	    //        $info->error = 'Enter photography_allowed';
	    //        $this->set_response($info, REST_Controller::HTTP_OK);
	    //  	 }
			// 	 elseif(empty($this->post('special_need')))
			// 		{
			// 			$info->error = 'Enter special_need';
			// 			$this->set_response($info, REST_Controller::HTTP_OK);
			// 	}
			// 	elseif(empty($this->post('medicine')))
			// 	{
			// 		 $info->error = 'Enter medicine';
			// 		 $this->set_response($info, REST_Controller::HTTP_OK);
			// 	}
			// 	elseif(empty($this->post('spending_money')))
			// 	{
			// 		 $info->error = 'Enter spending_money';
			// 		 $this->set_response($info, REST_Controller::HTTP_OK);
			// 	}
			// 	elseif(empty($this->post('allergies')))
			// 	{
			// 		 $info->error = 'Enter allergies';
			// 		 $this->set_response($info, REST_Controller::HTTP_OK);
			// 	}
			 else{

       	 $accesstoken_exist = $this->Student->chk_access_token($this->post('access_token'));
		     if($accesstoken_exist == 1){
             $this->db->insert('student',$data);
             $id = $this->db->insert_id(); // id of last inserted

############################################################################

	    if($this->post('special_need') == 1){
			$special_need = $this->post('special_need');
			if(!empty($this->post('special_need_type')))
				{
				 if(strlen($this->post('special_need_type')) >100){
				 	$info['error'] = "Max 100 character allowed for special need";
				 }else{
				  $special_need_type = $this->post('special_need_type');
				  }
				}else{
				 $info['error'] = "invalid special_need_type";
				}
				}else{
					$special_need = 0;
					}

			 if($this->post('spending_money') == 1){
				 	$spending_money = $this->post('spending_money');
				 if(!empty($this->post('spending_money_amt')))
				 {
					 if(is_numeric($this->post('spending_money_amt'))){
					 	if(strlen($this->post('spending_money_amt')) >15){
				 		$info['error'] = "Max 15 character allowed for spending money amount";
				 		}else{
						 $spending_money_amt = $this->post('spending_money_amt');
						}
					 }else{
						 $info['error'] = "invalid spending_money_amt";
					 }
				 }else{
					 $info['error'] = "invalid spending_money_amt";
				 }
			 }else{
				 $spending_money = 0;
			 }

			 if($this->post('allergies') == 1){
			  	 $allergies = $this->post('allergies');
				 if(!empty($this->post('allergy_details')))
				 {
				 	if(strlen($this->post('allergy_details')) >100){
				 	 $info['error'] = "Max 100 character allowed for allergy";
				 	}else{
					 $allergy_details = $this->post('allergy_details');
					}
				 }else{
					 $info['error'] = "invalid allergy_details";
				 }
			 }else{
				 $allergies = 0;
			 }

###############################################################################

		if(!empty($info)){
			$this->set_response($info, REST_Controller::HTTP_OK);
		}else {

			$this->db->set('student_id', $id);
			$this->db->set('Is_photography_allowed', $this->post('photography_allowed'));
			$this->db->set('Is_special_need', $special_need);
			if(!empty($special_need_type)){
				$this->db->set('special_need_type', $special_need_type);
			}

			$this->db->set('Is_medicine', $this->post('medicine'));
			$this->db->set('Is_spending_money', $spending_money);
			if(!empty($spending_money_amt)){
				$this->db->set('spending_money_amt', $spending_money_amt);
			}
			$this->db->set('Is_allergies', $allergies);
			if(!empty($allergy_details)){
				$this->db->set('allergy_details', $allergy_details);
			}
			$this->db->set('created_by', $this->post('teacher_id'));
			$this->db->set('modified_time', time());
			$this->db->insert('student_checklist');

			$this->db->select('*');
			$this->db->from('student_checklist');
			$this->db->where('student_id', $id);
			$query = $this->db->get();
			$reult = $query->result_array();
			$this->db->last_query();
			$result1['id'] = $id;
			$result1['check_list'] = $reult;
			$result1['success'] = ("Added");
			$this->set_response($result1, REST_Controller::HTTP_OK);
		}
      	   }else{
      	  	$info->error = 'Session expired';
        	  $this->set_response($info, REST_Controller::HTTP_OK);
       	 	 }
           }
				 }else{
					 ####### CSV Method  ########
					 $accesstoken_exist = $this->Student->chk_access_token($this->post('access_token'));
					 if($accesstoken_exist == 1){
						 if((!empty($this->post('teacher_id')))){

					 $handle = fopen($file,"r");
					 $mimes = array('application/vnd.ms-excel','text/plain','text/csv','text/tsv');
					 if(in_array($_FILES[csv][type],$mimes)){
						while(! feof($handle))
							{
								$my[] = fgetcsv($handle);
							}
							fclose($file);
							 $count_f = count($my);
	###################################################################
	############################# CSV method ##########################

		if ( $count_f > 0) {
			for ($i=0; $i < $count_f; $i++) {
				$n_id =  str_replace(' ', '',$my[$i][3]);
				$n1_id =  str_replace("'", '',$n_id);
				$n_id1 =  str_replace(' ', '',$my[$i][4]);
				$n1_id1 =  str_replace("'", '',$n_id1);
				$ice_id1 = $n1_id;
				$ice_id2 = $n1_id1;
				$first_name = $my[$i][0];
				$last_name = $my[$i][1];
				$c = $my[$i][2];
				$c1 = strtolower($c);
				$class = $c1;
			// 	if(!empty($c1)){
			// 		$this->db->select('id');
			// 		$this->db->from('grades');
			// 		$this->db->like('grade', $c1);
			// 		$q = $this->db->get();
			// 		$r = $q->result_array();
			// 	if(!empty($r)){
			// 		$class = $r[0]['id'];
			// 	}
			// }
			$photography_allowed = $my[$i][5];
			$special_need = $my[$i][6];
			$special_need_type = $my[$i][7];
			$medicine = $my[$i][8];
			$spending_money = $my[$i][9];
			$spending_money_amt = $my[$i][10];
			$allergies = $my[$i][11];
			$allergy_details = $my[$i][12];
			$modified_time = time();

	##############################################################
	########################3 CSV insert #########################

		if ((!empty($ice_id1)) or (!empty($ice_id2))) {
			if ((is_numeric($ice_id1)) or (is_numeric($ice_id2))) {
					// if((is_numeric($ice_id1)) and (is_numeric($ice_id1)) and (is_numeric($ice_id1)) and (is_numeric($ice_id1)) and (is_numeric($ice_id1)) and (is_numeric($ice_id1))) {
				$this->db->set('f_name', $first_name, TRUE);
			    $this->db->set('l_name',$last_name, TRUE);
				$this->db->set('class',$class, FALSE);
					if(!empty($ice_id1)){
						$this->db->set('contact1',$ice_id1, FALSE);
						}
					if (!empty($ice_id2)) {
						$this->db->set('contact2',$ice_id2, FALSE);
					}
						$this->db->set('created_by',$this->post('teacher_id'), FALSE);
						$this->db->set('modified_time',$modified_time, FALSE);
						$insert = $this->db->insert('student');
						$id = $this->db->insert_id();
						$this->db->set('student_id', $id);
						if((is_numeric($photography_allowed)) and ($photography_allowed == 1)){
							$this->db->set('Is_photography_allowed', 1);
						}else{
							$this->db->set('Is_photography_allowed', 0);
						}
						if((is_numeric($special_need)) and ($special_need == 1)){
							$this->db->set('Is_special_need', 1);
						}else{
							$this->db->set('Is_special_need', 0);
							}
						if(!empty($special_need_type)){
							$this->db->set('special_need_type', $special_need_type);
							}

						if((is_numeric($medicine)) and ($medicine == 1)){
							$this->db->set('Is_medicine', 1);
						}else {
							$this->db->set('Is_medicine', 0);
							}
						if((is_numeric($spending_money)) and ($spending_money == 1)){
							$this->db->set('Is_spending_money', 1);
							}else {
							$this->db->set('Is_spending_money', 0);
							}
						if(!empty($spending_money_amt)){
							$this->db->set('spending_money_amt', $spending_money_amt);
							}
						if((is_numeric($allergies)) and ($allergies == 1)){
							$this->db->set('Is_allergies', 1);
							}else{
							$this->db->set('Is_allergies', 0);
							}
						if(!empty($allergy_details)){
							$this->db->set('allergy_details', $allergy_details);
							}
							$this->db->set('created_by', $this->post('teacher_id'));
							$this->db->set('modified_time', time());
							$this->db->insert('student_checklist');
							}else {
							$error_id[] = $first_name;
							}
							}else {
							$error_id[] = $first_name;
							}

		###############################################################
		######################### CSV insert End ######################
							}

			if(!empty($error_id)){
				if(count($error_id) > 2){
					$result1['error'] = ("some values is invalid");
				}
			}
			$result1['success'] = ("Added");
			$this->set_response($result1, REST_Controller::HTTP_OK);
     ####################################################################

			}

	 ######################################################################
	 ########################## CSV method End ##############################
		 }else {
				 ###### access_token
 				$info->error = 'invalid CSV';
 				$this->set_response($info, REST_Controller::HTTP_OK);
				}
			}else{
				$info->error = 'invalid teacher id';
				$this->set_response($info, REST_Controller::HTTP_OK);
				}
			 }else{
			 	$info->error = 'Session expired';
				$this->set_response($info, REST_Controller::HTTP_OK);
			}
				 }

     	}


			function create_csv_get()
			{

			$accesstoken_exist = $this->Student->chk_access_token($this->get('access_token'));
			if($accesstoken_exist == 1){
					$this->load->dbutil();
					$this->load->helper('file');
					$this->load->helper('download');
					$delimiter = ",";
					$newline = "\r\n";
					$filename = "student.csv";
					header('Content-Type: application/excel');
					header('Content-Disposition: attachment; filename="sample.csv"');
				$user_CSV[0] = array('First Name','Last Name','Class','Ice contact 1','Ice Contact 2','Photography Allowed','Special Need','Special Need Type','Medicine', 'Spending Money','Spending Money Amount','Allergies','Allergy Details');
					$fp = fopen('php://output', 'w');
					fputcsv($fp, $user_CSV[0]);
					fclose($fp);
	  		}else{
				$info->error = 'Session expired';
			 $this->set_response($info, REST_Controller::HTTP_OK);
			}

			}


	function edit_student_post(){
		$posts = array();
		$accesstoken_exist = $this->Student->chk_access_token($this->post('access_token'));
	 if($accesstoken_exist == 1){
			 $this->db->select("*");
			 $this->db->from('student');
			 $this->db->where('id',$this->post('id'));
			 $query = $this->db->get();
			 $result = $query->result_array();
			 $this->db->select("*");
			 $this->db->from('student_checklist');
			 $this->db->where('student_id',$this->post('id'));
			 $query1 = $this->db->get();
			 $result_chk_list = $query1->result_array();
		if (!empty($result)){
			if(empty($this->post('first_name')))
			{
				$info1->error_code = "invalid first name";
				array_push($posts, $info1);
				$first_name = $result[0]['f_name'];
			}else{
				$first_name = $this->post('first_name');
			}
			if(empty($this->post('last_name')))
			{
				$info2->error_code = ("invalid lastname");
				array_push($posts, $info2);
				$last_name = $result[0]['l_name'];
			}else{
				$last_name = $this->post('last_name');
			}
			if(empty($this->post('class')))
			{
				// $info3->error_code = ("invalid class");
				// array_push($posts, $info3);
				$class = $result[0]['class'];
			}else{
				$class = $this->post('class');
			}
			if(empty($this->post('ice_contact_1')))
			{
				// $info5->error_code = ("invalid ice contact 1");
				// array_push($posts, $info5);
				$ice_contact_1 = $result[0]['contact1'];
			}else{
				$ice_contact_1 = $this->post('ice_contact_1');
			}
			if(empty($this->post('ice_contact_2')))
			{
				// $info6->error_code = ("invalid ice contact 2");
				// array_push($posts, $info6);
				$ice_contact_2 = $result[0]['contact2'];
			}else{
				$ice_contact_2 = $this->post('ice_contact_2');
			}
			if(!empty($posts)){
				$this->response($posts, REST_Controller::HTTP_OK);
			}

		$data = array(
 		'f_name' => $first_name,
 		'l_name' => $last_name,
        'class' => $class,
        'contact1' => $ice_contact_1,
        'contact2' => $ice_contact_2,
 		'created_by' => $this->post('teacher_id'),
        'modified_time' => time()
 		);
			if (!empty($data)){
				if(!empty($result_chk_list)){
					if(empty($this->post('photography_allowed')))
					 {
						 $photography_allowed = $result[0]['Is_photography_allowed'];
					 }else{
						 $photography_allowed = $this->post('photography_allowed');
					 }
					 if(empty($this->post('medicine')))
					 {
						 $medicine = $result[0]['Is_medicine'];
					 }else{
						 $medicine = $this->post('medicine');
					 }

					 if(empty($this->post('special_need')))
					 {
						 $special_need = $result[0]['Is_special_need'];
					 }else{
						$special_need = $this->post('special_need');
						  if($special_need == 1) {
							if(empty($this->post('special_need_type'))){
								 $info10->error_code = 'invalid special_need_type';
								 array_push($posts, $info10);
							}else{
								 $special_need_type = $this->post('special_need_type');
							}
						 }
					 }
				if(empty($this->post('spending_money')))
					{
					 $spending_money = $result[0]['Is_spending_money'];
					}else{
					 $spending_money = $this->post('spending_money');
			    if($spending_money == 1) {
					if(empty($this->post('spending_money_amt'))){
						$info12->error_code = 'invalid spending_money_amt';
						array_push($posts, $info12);
					}else{
						$spending_money_amt = $this->post('spending_money_amt');
					}
				}
			 }

			if(empty($this->post('allergies')))
				{
			    	$allergies = $result[0]['Is_allergies'];
				}else{
					 $allergies = $this->post('allergies');
				if($allergies == 1) {
					if(empty($this->post('allergy_details'))){
						$info14->error_code = 'invalid allergy_details';
						array_push($posts, $info14);
					}else{
						$allergy_details = $this->post('allergy_details');
					}
				 }
					 }

			$data1 = array(
				 'student_id' => $result[0]['id'],
				 'Is_photography_allowed' => $photography_allowed,
				 'Is_medicine' => $medicine,
				 'Is_special_need' => $special_need,
				 'special_need_type' => $special_need_type,
				 'Is_spending_money' => $spending_money,
				 'spending_money_amt' => $spending_money_amt,
				 'Is_allergies' => $allergies,
				 'allergy_details' => $allergy_details,
				 'created_by' => $this->post('teacher_id'),
				 'modified_time' => time()
			);

			if(!empty($posts)){
				$this->response($posts, REST_Controller::HTTP_OK);
			 }else{
				 $this->db->where('id', $result[0]['id']);
				 $this->db->update('student',$data);
				 $this->db->where('student_id',$result[0]['id']);
				 $this->db->update('student_checklist',$data1);
				 $result_response["success"] = "updated";
				 $this->response($result_response, REST_Controller::HTTP_OK);
			    }
				 }else{
      ###########################################
					if(empty($this->post('photography_allowed')))
			   		 {
						$photography_allowed = 0;
						$info14->error_code = 'invalid photography_allowed';
						array_push($posts, $info14);
			    	 }else{
						$photography_allowed = $this->post('photography_allowed');
						 }
						 if(empty($this->post('medicine')))
		   			 {
						$medicine = 0;
						$info15->error_code = 'invalid medicine';
						array_push($posts, $info15);
			    	 }else {
							 $medicine = $this->post('medicine');
						 }

					if(empty($this->post('special_need')))
	   				 {
						$special_need= 0;
						$info16->error_code = 'invalid special_need';
						array_push($posts, $info16);
			    	 }else {
						$special_need = $this->post('special_need');
			    	 }

			       if(empty($this->post('spending_money')))
			   		 {
						$spending_money = 0;
						$info17->error_code = 'invalid spending_money';
						array_push($posts, $info17);
    			     }else{
						 $spending_money = $this->post('spending_money');
						 }

					if(empty($this->post('allergies')))
			       {
					  $allergies = 0;
					  $info18->error_code = 'invalid allergies';
					  array_push($posts, $info18);
			       }else {
					 $allergies = $this->post('allergies');
			         }

         if($this->post('special_need') == 1){
							 if (empty($this->post('special_need_type'))) {
								 $info19->error_code = 'invalid special_need_type';
								 array_push($posts, $info19);
							 }else{
								 $special_need_type = $this->post('special_need_type');
							 }
						 }

        if($this->post('spending_money') == 1){
						 if (empty($this->post('spending_money_amt'))) {
			    				$info20->error_code = 'invalid spending_money_amt';
									array_push($posts, $info20);
			       }else{
							 $spending_money_amt = $this->post('spending_money_amt');
						 }
					 }

					 if($this->post('allergies') == 1){
						 if (empty($this->post('allergy_details'))) {
							 $info21->error_code = 'invalid allergy_details';
							 array_push($posts, $info21);
			       }else{
							 $allergy_details = $this->post('allergy_details');
						 }
					 }
					 if(!empty($posts)){
						 $this->response($posts, REST_Controller::HTTP_OK);
					}else{
					$data1 = array(
							'student_id' => $result[0]['id'],
							'Is_photography_allowed' => $photography_allowed,
							'Is_medicine' => $medicine,
							'Is_special_need' => $special_need,
							'special_need_type' => $special_need_type,
							'Is_spending_money' => $spending_money,
							'spending_money_amt' => $spending_money_amt,
							'Is_allergies' => $allergies,
							'allergy_details' => $allergy_details,
							'created_by' => $this->post('teacher_id'),
							'modified_time' => time()
						);

							 if(!empty($data1)){
								 $this->db->insert('student_checklist', $data1);
								 $result_response["success"] = "updated";
								 $this->set_response($result_response, REST_Controller::HTTP_OK);

							 }
						 }


      ############################################
				 }
			 }
		 }else{
			 ###### result data
				$info->error = 'invalid id';
				$this->set_response($info, REST_Controller::HTTP_OK);
		 }
		}else{
			###### access_token
			$info->error = 'Session expired';
			$this->set_response($info, REST_Controller::HTTP_OK);
		}
	}
	function remove_student_post(){
	 $data  = array('id' => $this->post('id'));
		$accesstoken_exist = $this->Student->chk_access_token($this->post('access_token'));
		$id = $this->Student->chk_id($this->post('id'));
			if($accesstoken_exist == 1){
				if($id == 1) {
					$this->db->where('id', $this->post('id'));
					$this->db->delete('student');
					$this->db->where('student_id', $this->post('id'));
					$this->db->delete('student_checklist');
					$result1['success'] = ("deleted");
					$this->set_response($result1, REST_Controller::HTTP_OK);
				}else{
					$info->error = 'invalid id';
					$this->set_response($info, REST_Controller::HTTP_OK);
				}
			}else{
					 $info->error = 'Session expired';
					 $this->set_response($info, REST_Controller::HTTP_OK);
					}

	}


   function students_by_group_id_get(){
   	$posts =array();
   	$student_id = $this->Student->get_student_id_by_group_id($this->get('group_id'),$this->get('teacher_id'));
   	$accesstoken_exist = $this->Student->chk_access_token($this->get('access_token'));
   	if($accesstoken_exist == 1){
			$ifteacher_exist = $this->Student->check_if_correct_teacher_id($this->get('teacher_id'));
		 if($ifteacher_exist == "1"){
   		if($this->Student->check_if_correct_group_id($this->get('group_id')) != "1"){
   		 $info->error = 'Invalid group id';
		 	 $this->set_response($info, REST_Controller::HTTP_OK);
   		}else{
		    foreach($student_id as $key=>$value) {
		      $this->db->select('*');
		      $this->db->from('student_checklist');
		      $this->db->where('student_id',$student_id[$key]);
		      $query = $this->db->get();
		      // echo $this->db->last_query();
		      $result = $query->result_array();
		      foreach($result as $value) {
		      	$results = "";
		      	$results->name= $this->Student->get_student_name_by_id($value['student_id']);
		      	$results->photography_allowed = $value['Is_photography_allowed'];
		      	$results->special_need = $value['Is_special_need'];
		      	$results->special_need_details = $value['special_need_type'];
		      	$results->medicine = $value['Is_medicine'];
		      	$results->spending_money = $value['Is_spending_money'];
		      	$results->spending_money_amount = $value['spending_money_amt'];
		      	$results->allergies = $value['Is_allergies'];
		      	$results->allergy_details = $value['allergy_details'];
		      	array_push($posts, $results);
		       }
		    	}
		       echo json_encode(array('result'=>$posts, 'success'=>'success'));
  	 }
	 		}else{
					 $info->error = 'invalid teacher_id';
			 $this->set_response($info, REST_Controller::HTTP_OK);
	 		}
   		}else{
   		$info->error = 'Session expired';
		$this->set_response($info, REST_Controller::HTTP_OK);
   		}
   }


   function student_and_group_get(){
	$accesstoken_exist = $this->Student->chk_access_token($this->get('access_token'));
  	 	if($accesstoken_exist == 1){
      $chkteacher = $this->Student->check_if_correct_teacher_id($this->get('teacher_id'));
      if($chkteacher == '1'){
    	$teacher_id =$this->get('teacher_id');
	    $student =	$this->Student->get_all_student_with_group($this->get('teacher_id'));
  	 	 	$group =	$this->Student->get_all_group();
  		 	echo json_encode(array("result"=>$student, "groups" =>$group, "success"=> "success"));
      }else{
     	$info->error = 'invalid teacher_id';
		$this->set_response($info, REST_Controller::HTTP_OK);
     }

    }else{
    	$info->error = 'Session expired';
		$this->set_response($info, REST_Controller::HTTP_OK);
    }
   }

   function sort_students_get(){
   	$ordertype = $this->get('ordertype'); // 1 for asc order, 2 for desc order student name
   	$teacher_id = $this->get('teacher_id');
   	$accesstoken_exist = $this->Student->chk_access_token($this->get('access_token'));
  	 if($accesstoken_exist == 1){
      $chkteacher = $this->Student->check_if_correct_teacher_id($this->get('teacher_id'));
      if($chkteacher == '1'){
       if($ordertype == "1" or $ordertype == "2"){
        $student =	$this->Student->get_all_student_with_student_orderyby($this->get('teacher_id'),$ordertype);
  	 	$group =	$this->Student->get_all_group();
  		echo json_encode(array("result"=>$student, "groups" =>$group, "success"=> "success"));
       }else{
       	$info->error = 'invalid order type';
		$this->set_response($info, REST_Controller::HTTP_OK);
       }
  	 }else{
     	$info->error = 'invalid teacher_id';
		$this->set_response($info, REST_Controller::HTTP_OK);
     }

   }else{
    	$info->error = 'Session expired';
		$this->set_response($info, REST_Controller::HTTP_OK);
    }
}

	function sort_group_get(){
   	$ordertype = $this->get('ordertype'); // 1 for asc order, 2 for desc order student name
   	$teacher_id = $this->get('teacher_id');
   	$accesstoken_exist = $this->Student->chk_access_token($this->get('access_token'));
  	 if($accesstoken_exist == 1){
      $chkteacher = $this->Student->check_if_correct_teacher_id($this->get('teacher_id'));
      if($chkteacher == '1'){
       if($ordertype == "1" or $ordertype == "2"){
        $student =	$this->Student->get_all_student_with_group_orderyby($this->get('teacher_id'),$ordertype);
  	 	$group =	$this->Student->get_all_group_by_order($ordertype);
  		echo json_encode(array("result"=>$student, "groups" =>$group, "success"=> "success"));
       }else{
       	$info->error = 'invalid order type';
		$this->set_response($info, REST_Controller::HTTP_OK);
       }
  	 }else{
     	$info->error = 'invalid teacher_id';
		$this->set_response($info, REST_Controller::HTTP_OK);
     }

   }else{
    	$info->error = 'Session expired';
		$this->set_response($info, REST_Controller::HTTP_OK);
    }
}


  function get_csv_email_get(){
  	$accesstoken_exist = $this->Student->chk_access_token($this->get('access_token'));
  	 if($accesstoken_exist == 1){
  	 	$email = $this->get('email');
  	 	// ****************** EMAIL **************** //
  	 	$this->load->library('email');
		$this->load->helper('email');
     	$this->email->set_mailtype("html");
	    $file =  base_url().'uploads/student.csv';
		$this->email->attach($file, 'inline');
		  $body = $this->email_body();
            // print_r($body);die;
            $config = array();
            $config['useragent'] = "CodeIgniter";
            $config['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
            $config['protocol'] = "smtp";
            $config['smtp_host'] = "ssl://smtp.gmail.com";
            $config['smtp_port'] = "465";
            $config['smtp_user'] = "testingteaminficare@gmail.com";
            $config['smtp_pass'] = "inficare123";
            $config['mailtype'] = 'html';
            $config['charset']  = 'utf-8';
            $config['newline']  = "\r\n";
            $config['wordwrap'] = TRUE;
            $this->load->library('email');
            $this->email->initialize($config);
            $this->email->set_newline("\r\n");
            $this->email->from('testingteaminficare@gmail.com', 'Putnum');
            $this->email->to($email);
            $this->email->subject("Putnam teacher app");
            $this->email->message($body);
            $this->email->send();
  	 }else{
    	$info->error = 'Session expired';
		$this->set_response($info, REST_Controller::HTTP_OK);
    }
  }

   function email_body(){
   $body = "<!doctype html>
            <html>
            <head>
            </head>
            <body style='background:#eee;'>
            <div style=' text-align:left; margin:0 auto; background:#fff; font-family:arial;'>
            <table width='100%' border='0' cellspacing='0' cellpadding='0' style=' border-collapse:collapse; font-family:arial; border-radius:5px; '>
              <tbody>
                   <tr>
                  <td style='color:#333; font-size:14px; text-align:left; padding:2px 10px; font-family:arial;'>&nbsp;</td>
                </tr>
                <tr>
                  <td style='color:#333; font-size:14px; text-align:left; padding:2px 10px; font-family:arial;'>Hi,</td>
                </tr>
                <tr>
                  <td style='color:#333; font-size:14px; text-align:left; padding:2px 10px; font-family:arial;'>&nbsp;</td>
                </tr>
                <tr>
                  <td style='color:#333; font-size:14px; text-align:left; padding:2px 10px; font-family:arial;'>
                   Please find attached csv file.
                </tr>
                <tr>
                  <td style='color:#333; font-size:14px; text-align:left; padding:2px 10px; font-family:arial;'>&nbsp;</td>
                </tr>
                <tr>
                  <td style='color:#333; font-size:14px; text-align:left; padding:2px 10px; font-family:arial;'>
                   Hope you are enjoying using Putnam teacher app.
                  </td>
                </tr>
                <tr>
                  <td style='color:#333; font-size:14px; text-align:left; padding:2px 10px; font-family:arial;'>&nbsp;</td>
                </tr>
                <tr>
                  <td style='color:#333; font-size:14px; text-align:left; padding:2px 10px; font-family:arial;'>Cheers,<br>
           Team Putnam</td>
                </tr>
                <tr>
                  <td style='color:#333; font-size:14px; text-align:left; padding:2px 10px; font-family:arial;'>&nbsp;</td>
                </tr>

              </tbody>
            </table>
            </div>
            </body>
            </html>";

            return $body;
 }







}

?>
