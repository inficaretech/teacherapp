<?php
error_reporting(0);
/**
*
*/
class Subcategories extends CI_Controller
{

	function __construct()
	{
	  parent::__construct();
	   $this->load->model('Subcategory');
      $this->load->library('session');
      if(!$this->session->userdata('logged_in'))
        {
          redirect('login');
        }
			
	}


	function index(){
	 $data['results']=$this->Subcategory->get_all_entries_with_category_name();
	 $this->load->view('admin/subcategory', $data);
	}


	function addsubcategory(){
	     $data =$this->Subcategory->get_all_categories();
       $gradetype = $this->config->item('grade_array'); 
          ?>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
         <form method="POST" role="form" id="up_status" action='<?= base_url(); ?>index.php/Subcategories/addnewsubcategory' enctype="multipart/form-data" >
		  <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Change status</h4>
            </div>
            <div class="modal-body">
              <div class="form-group">
                   <strong>Category</strong> </br>
                   <select name="category" class="form-control" required>
                   	<?php  foreach($data as $value){ ?>
                    <option value="<?php echo $value->id; ?>"><?php echo $value->category; ?></option>
                    <?php } ?>
                   </select>
               </div>
                <div class="form-group">
                   <strong>Grade Level</strong> </br>
                   <select name="grade_level" class="form-control" required>
                    <?php for($i= 1; $i<= 5; $i++){?>
                    <option value="<?php echo $i;?>"><?php echo $gradetype[$i]?></option>
                    <?php }?>
                   </select>
               </div>
                 <div class="form-group">
                    <strong>Title</strong></br>
                    <input type="text" name="title" value="" class="form-control" maxlength="80" required/>
               </div>
                <div class="form-group">
                    <strong>Time Duration</strong></br>
                    <input type="text" name="time_duration" value="" class="form-control" maxlength="30" required/>
               </div>
                <div class="form-group">
                    <strong>Description</strong></br>
                    <textarea name="description" class="form-control" required></textarea>
               </div>
               <div class="form-group">
                    <strong>Image</strong></br>
                   <input type="file" name = "image" />
               </div>
              </div>
              <div class="modal-footer">
             <input type="submit" class="btn btn-default" value="Submit" class="form-submit" />
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
           </form>

      <?php  }


     function addnewsubcategory(){
       $data['image'] = $this->validateUpload();
       $data = array('category_id' => $this->input->post('category'),
      				        'title' => $this->input->post('title'),
                      'image' => $data['image'],
                      'grade_type' => $this->input->post('grade_level'),
                      'time_duration' => $this->input->post('time_duration'),
      				        'description' => $this->input->post('description'),
      				        'created_by' => $this->session->logged_in['id'],
              		    'modified_time' => time()
      		           );
		 $chkdata = $this->Subcategory->Chk_data_exist($data['title']);
 			if($chkdata != '1'){
      	 $insert = $this->db->insert('master_subcategory',$data);
         if($insert){
					 $this->session->set_flashdata('flassuccess',"Inserted successfully");

         $base_url=base_url();
         redirect("$base_url"."index.php/Subcategories/index");
        }
			}else{
			 $this->session->set_flashdata('flaserror',"Already exist");
			 $base_url=base_url();
			 redirect("$base_url"."index.php/Subcategories/index");
		 }
      }

public function validateUpload(){
    if ($_FILES AND isset($_FILES['image']['name']) ){
        $timestamp = date("YmdHis", time());
        $config['upload_path'] = base_url().'uploads/subcategory_images/';
        $config['allowed_types'] = 'png|gif|jpg|jpeg';
        $config['max_size'] = '999999';
        $config['file_name'] = $timestamp."_".$_FILES['image']['name'];
        $this->load->library("upload",$config);
        $this->upload->initialize($config);
       if(!$this->upload->do_upload("image")){
           return $this->upload->display_errors();
        }else{
          return $config['file_name'];
        }
    }
}
    function editsubcategory($id){
      $image_url = "../../uploads/subcategory_images/";
            $this->db->select('*');
            $this->db->from('master_subcategory');
            $this->db->where('id',$id);
            $query = $this->db->get();
            $result = $query->result();
           ?>
         <form method="POST" role="form"  action="updatesubcategory/" enctype="multipart/form-data" >
         <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Sub category</h4>
                <input type="hidden" name="id" value="<?php echo $result[0]->id; ?>">
            </div>
            <div class="modal-body">
              <div class="form-group">
                  <strong>Category</strong> </br>
                  <?php echo $this->Subcategory->get_category_name_by_id($result[0]->category_id); ?>
               </div>
               <?php $gradetype = $this->config->item('grade_array'); ?>
                <div class="form-group">
                    <strong>Grade type</strong> </br>
                    <?php if(!empty($result[0]->grade_type)){?>
                    <input type="hidden" name= "grade_level" value="<?php echo $result[0]->grade_type?>">
                    <?php echo $gradetype[$result[0]->grade_type];?>
                    <?php }else{ ?>
                    <select name="grade_level" class="form-control" required>
                    <?php for($i= 1; $i<= 5; $i++){?>
                    <option value="<?php echo $i;?>"><?php echo $gradetype[$i]?></option>
                    <?php }?>
                   </select>
                   <?php }?>
               </div>
                 <div class="form-group">
                    <strong>Title</strong></br>
                    <input type="text" name="title" class="form-control" value="<?php echo $result[0]->title; ?>" maxlength="80" required/>
               </div>
                <div class="form-group">
                    <strong>Time Duration</strong></br>
                    <input type="text" name="time_duration" value="<?php echo $result[0]->time_duration; ?>" maxlength="30" class="form-control" required/>
               </div>
                <div class="form-group">
                    <strong>Description</strong></br>
                    <textarea name="description" class="form-control" required><?php echo $result[0]->description; ?></textarea>
               </div>

                <div class="form-group">
                    <strong>Image</strong></br>
                    <img src="<?php echo $image_url.$result[0]->image; ?>"  height="50" width="50"/ >
                    <input type="file" name="image" value="" />
               </div>
              </div>
              <div class="modal-footer">
                <input type="submit" class="btn btn-default" value="Update" class="form-submit" />

              <!--  <input type="button" class="btn btn-default" data-dismiss="modal" value="Update" onclick="UpdatesubCategory(<?php echo $result[0]->id;  ?>)" class="form-submit" />
               -->
               <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
           </form>
           <?php }


    function updatesubcategory($id){
      $this->load->library('upload');
       $data['image'] = $this->validateUpload();
      if(empty($_FILES['image']['tmp_name'])){
       $data = array('title'=>$this->input->post('title'),
                     'description'=>$this->input->post('description'),
                     'grade_type' => $this->input->post('grade_level'),
                     'time_duration' => $this->input->post('time_duration'),
                     'modified_time'=> time(),
                    );
     }else{
         $data = array('title'=>$this->input->post('title'),
                     'description'=>$this->input->post('description'),
                     'image' => $data['image'],
                     'time_duration' => $this->input->post('time_duration'),
                     'modified_time'=> time(),
                    );
         }
         $id = $_REQUEST['id'];
       		$chkdata = $this->Subcategory->Chk_data_exist($data['title'], $id);
								if($chkdata != '1'){
                    $this->db->where('id',$id);
                    $this->db->update('master_subcategory',$data);
                  // echo $this->db->last_query(); die;
    //                 $this->db->select('*');
    //                 $this->db->where('id',$id);
    //                 $this->db->from('master_subcategory');
    //                 $query = $this->db->get();
    //                 $result = $query->result();
    //                 echo $html = '
    // <td>'.$this->Subcategory->get_category_name_by_id($result[0]->category_id).'</td>
    // <td>'.$result[0]->title.'</td>
    // <td>'.$result[0]->description.'</td>
    // <td>
    //    <a href="javascript:void(0)" title="Edit" onclick="editsubcategory('.$result[0]->id.')" data-toggle="modal" data-target="#domain" class="icon-1 info-tooltip">Edit</a>
    //    <a href="javascript:void(0)" class="hide"  id="hide'.$result[0]->id.'">Please wait...</a>
    //    <a href="javascript:void(0)" title="Delete" onclick="deletesubCategory('.$result[0]->id.')" class="icon-2 info-tooltip">Delete</a>';
				$this->session->set_flashdata('flassuccess',"Updated successfully");
        $base_url=base_url();
        redirect("$base_url"."index.php/Subcategories/index");
				}else{
					// echo "already exist";
					$this->session->set_flashdata('flaserror',"Already exist");
				}
   }

   function deletesubCategory($id){
      $this->db->where('id',$id);
      $this->db->delete('master_subcategory');
      //echo $this->db->last_query();
   }

 function submitvalid(){
  
 }

  }
