<?php
error_reporting(0);
require (APPPATH.'libraries/REST_Controller.php');

class SubcategoriesAPI extends REST_Controller
{

	function __construct()
	{
	  parent::__construct();
	   $this->load->model('Subcategory');
	}


	function get_sub_categories_post(){
		$posts = array();
		$pp = array();
		$accesstoken_exist = $this->Subcategory->chk_access_token($this->post('access_token'));
		$teacher_id = $this->Subcategory->get_teacher_id_by_access_token($this->post('access_token'));
		if($accesstoken_exist == 1){
		$ii = $this->post('id'); // array of category id
		$gi = $this->post('grade'); // array of category id
		$ids = explode( ',' , $ii);
		$teacherid =$this->post('teacher_id');
		// $grades = explode( ',' , $gi);

		$totalcategory = count($ids);
		//######## CODE FOR Insert category group ###########//
		$this->db->select('*');
		$this->db->from('group_students');
		$this->db->where('created_by', $teacherid);
		$query1 = $this->db->get();
		//echo $this->db->last_query();
		    $data1 = $query1->result_array();
		        foreach ($data1 as $key => $value) {
		          $group_id[$key] = $value['group_id'];
		        }
		         $delete_data = $this->Subcategory->delete_data($teacherid);
				 $group_count = count($group_id);
		          for ($i=0; $i < $group_count; $i++) {
		        		for ($j=0; $j < $totalcategory; $j++) {
								  $addcat_group  = $this->Subcategory->assign_category_group($group_id[$i], $ids[$j],$teacherid );
								}
							}

		       // ######## CODE GET Subcategories ############# //
			 for ($k=0; $k < $totalcategory; $k++) {
		        $result = "";
				//echo $gi;die;
				$result = $this->Subcategory->Get_sub_categroy_details_by_category($ids[$k],$gi,$teacherid);
				array_push($posts, $result);
		      }
		        echo json_encode(array("result"=> $posts, "success" => "success"));
		      }else{
		          echo json_encode(array("error"=>"Session expired"));
		      }
    }


    function get_sub_categories_demo_post(){
		$posts = array();
		$pp = array();
		$ii = $this->post('id'); // array of category id
		$ids = explode( ',' , $ii);
		$totalcategory = count($ids);
		       // ######## CODE GET Subcategories ############# //
			 for ($k=0; $k < $totalcategory; $k++) {
		         $result = "";
				 $result = $this->Subcategory->Get_sub_categroy_details_by_category($ids[$k]);
				 array_push($posts, $result);
		   }
		        echo json_encode(array("result"=> $posts, "success" => "success"));

    }

  }
