<?php

/**
*
*/
class Subcategory extends CI_Model
{

	function __construct()
	{
	 parent::__construct();
	}

	function get_entries(){
		$query = $this->db->get('master_subcategory');
        return $query->result();
	}

	function get_all_categories(){
  			$this->db->select('*');
            $this->db->from('master_category');
            $this->db->order_by('category','asc');
            $query = $this->db->get();
	       return  $query->result();
	}


	function get_category_name_by_id($id){
			$this->db->select('category');
            $this->db->from('master_category');
            $this->db->where('id',$id);
            $query = $this->db->get();
	        $response = $query->result_array();
	    	return  $response[0]['category'];
	}


	function get_all_entries_with_category_name(){
	   $this->db->select('master_subcategory.*,master_category.category');
       $this->db->from('master_subcategory');
       $this->db->join('master_category', 'master_subcategory.category_id = master_category.id', 'right outer');
       $this->db->where('master_subcategory.category_id IS NOT NULL', null, false);
       $query = $this->db->get();
      return $query->result();
	}

	function chk_access_token($access_token){
	  $this->db->select('*');
      $this->db->from('access_token');
      $this->db->where('access_token',$access_token);
      $query = $this->db->get();
     // echo $this->db->last_query();
	  if($query->num_rows() > 0){
	  	$rows = '1';
	  }else{
	  	$rows = '0';
	  }
	  return $rows;
	}


	function get_subcategory_by_category($ids){
        $this->db->select('*');
        $this->db->from('master_subcategory');
        $this->db->where_in('category_id',$ids);
         $query = $this->db->get();
         // echo $this->db->last_query();
     	return $query->result();
	}



	function Chk_data_exist($name, $id){
		$this->db->select('*');
			$this->db->from('master_subcategory');
			$this->db->where('title',$name);
			$this->db->where('id !=',$id);
		//		echo $this->db->last_query(); die;
			$query = $this->db->get();
		if($query->num_rows() > 0){
			$rows = '1';
		}else{
			$rows = '0';
		}
		return $rows;
	}

	function get_teacher_id_by_access_token($access_token){
	  $this->db->select('admin_id');
      $this->db->from('access_token');
      $this->db->where('access_token',$access_token);
      $query = $this->db->get();
      $result =  $query->result();
      // print_r($result);
      if($query->num_rows() > 0){
      	foreach ($result as $results) {
      	$rows = $results->admin_id;
      	}
	  }else{
	  	$rows = '0';
	  }
	  return $rows;
	}


	function assign_category_group($group_id, $category_id, $teacher_id){
		$data = array(
		'master_category_id' => $category_id,
		'group_id' => $group_id,
		'teacher_id' => $teacher_id,
		'modified_time' => Time(),
		);
		$result =  $this->db->insert('group_category', $data);
		// echo $this->db->last	_query();
		if(!empty($result)){
			return  "1";
		}else{
			return "0";
		}
		}


		function delete_data($id)
		{
			$this->db->select('teacher_id');
				$this->db->from('group_category');
				$this->db->where('teacher_id',$id);
				$query = $this->db->get();
				$result =  $query->result();
				// return $result;die;
				if($query->num_rows() > 0){
           $this->db->where('teacher_id', $id);
					 $this->db->delete('group_category');
					//  $query = $this->db->get();
					//  $result1 =  $query->result();
					//  echo $this->db->last_query();

				}
				// return $result1;

		}



}



?>
