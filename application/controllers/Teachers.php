<?php
error_reporting(0);
defined('BASEPATH') OR exit('No direct script access allowed');
require (APPPATH.'libraries/REST_Controller.php');


class Teachers extends REST_Controller
{

	function __construct()
	{
	 parent::__construct();
     $this->load->model('Teacher');
	}

 function teacher_signup_post(){
    $this->load->helper('string');
		$data = array(
			'email' => $this->post('email'),
			'password' => md5($this->post('password')),
			'name' => $this->post('name'),
			'contact_number' => $this->post('contact_number'),
      'role' => '2',
      'platform' =>  $this->post('platform'),
			'modified_time' => time()
			);
  	if(empty($this->post('email')))
    {
          $info->error = 'Enter email id';
          $this->set_response($info, REST_Controller::HTTP_OK);
    }
    else if(!filter_var($this->post('email'), FILTER_VALIDATE_EMAIL))
    {
    	    $info->error = 'Enter correct email id';
          $this->set_response($info, REST_Controller::HTTP_OK);
    }
    else if(empty($this->post('password')))
    {
          $info->error = 'Enter 8 digits password';
          $this->set_response($info, REST_Controller::HTTP_OK);
    }
     else if(strlen($this->post('password')) < 8)
    {
          $info->error = 'Enter strong password';
          $this->set_response($info, REST_Controller::HTTP_OK);
    }
    else if(empty($this->post('school_name')))
    {
          $info->error = 'Enter school name';
          $this->set_response($info, REST_Controller::HTTP_OK);
    }
    else{
        $query =  $this->db->select("*")
                 ->from("admin")
                 ->where("email",$data['email'])
                 ->get();
   		 if ($query->num_rows() != 0){
               $info->error = 'already exist';
               $this->set_response($info, REST_Controller::HTTP_OK);
       	 }else{
             $accesstoken = random_string('alnum', 20);
             $this->db->insert('admin',$data);

             $teacher = new stdClass();
             $teacher->teacher_id = $this->db->insert_id(); // id of last inserted teacher

             $dataschool = array(
                  'school' => $this->post('school_name'),
                  'created_by' => $teacher->teacher_id,
                  'modified_time' => time()
                   );

             $this->db->insert('school',$dataschool);
             $school = new stdClass();
             $school->school_id = $this->db->insert_id();  // id of last inserted school
             $schluparray  = array('school_id' =>$school->school_id);
             $this->db->where('id',  $teacher->teacher_id);
             $this->db->update('admin', $schluparray);
             $this->db->last_query();

             $this->db->insert('access_token',array('admin_id' =>$teacher->teacher_id, 'access_token'=>$accesstoken ));
             $info['id'] = ($teacher->teacher_id);
             $info['name'] = $this->Teacher->get_teachername_by_id($teacher->teacher_id);
             $info['access_token'] = ($accesstoken);
            $this->set_response($info, REST_Controller::HTTP_OK);
           }
    }

	}


	function login_post()
	{

    $this->load->helper('string');
    $info =  new stdClass();
    $email = $this->post('email');
    $password = $this->post('password');
    $accesstoken = random_string('alnum',20);
    $teacher_result= $this->Teacher->Teacherlogin($email,$password);
    $uid=$teacher_result[0]['id'];

    $chkpassword = $this->Teacher->check_password_for_email($email,$password);

    if(empty($email)){
      $info->error = 'Please enter email';
      $this->set_response($info, REST_Controller::HTTP_OK);
    }else
    if(empty($password)){
     $info->error = 'Please enter password';
     $this->set_response($info, REST_Controller::HTTP_OK);
    }else
    if($chkpassword != "1"){
     $info->error = 'Invalid password';
     $this->set_response($info, REST_Controller::HTTP_OK);
    }else
    if($teacher_result)
    {
    $this->db->where('id',$uid);
    $this->db->select('id,email,name');
    $this->db->from('admin');
    $this->db->where('id',$uid);
    $loginquery = $this->db->get();

    $userdetail=$loginquery->result_array();
    $this->db->insert('access_token',array('admin_id' =>$userdetail[0]['id'], 'access_token'=>$accesstoken ));
    $user_sess_array['id'] =$userdetail[0]['id'];
		$user_sess_array['name'] =$userdetail[0]['name'];
    $user_sess_array['access_token'] = $accesstoken;
    $user_sess_array['success']="Login successfully";
    $this->session->set_userdata('logged_in', $user_sess_array);
    // $this->response($user_sess_array, 200);
		$this->set_response($user_sess_array, REST_Controller::HTTP_OK);

    }
    else
    {
    $info->error = 'Invalid email or password';
    // $this->response($info, 400);
		$this->set_response($info, REST_Controller::HTTP_OK);

    }

}


function chaperon_login_post(){
	$this->load->helper('string');
	 $accesstoken_exist = $this->Teacher->chk_access_code($this->post('access_code'));
	 if($accesstoken_exist != "0"){
		 $accesscode_id = $this->Teacher->chk_access_code_id($accesstoken_exist[0]['id']);
		 if($accesscode_id == "0"){
			 if((!empty($accesstoken_exist)) and ($accesstoken_exist != 0)){
				 $access_code = random_string('alnum', 20);
				 $insert_result = $this->Teacher->insert_accesstoken($accesstoken_exist[0]['id'],$access_code);
				 $new_access_token = $this->Teacher->get_access_token($accesstoken_exist[0]['id']);
				 $result = "";
				 $access_token = $new_access_token[0]['access_token'];
				 $teacher_data = $this->Teacher->get_teacher_data($access_token);
				 $group_data = $this->Teacher->get_group_data($teacher_data->id);
				  if(!empty($group_data)){
					 $result->id = $teacher_data->id;
					 $result->access_token = $access_token;
					 $result->teacher_id = $teacher_data->created_by;
					 $result->group_id = $group_data->id;
					 $result->group_name = $group_data->name;
           $result->score = $this->Teacher->check_score_for_chapereone($teacher_data->id);
					 $this->set_response($result, REST_Controller::HTTP_OK);
				 }else{
					 $info->error = "No group assign";
					 $this->set_response($info, REST_Controller::HTTP_OK);
				 }
        //  print_r($group_data);die;
			 }
		 }else{
			  $access_code = random_string('alnum', 20);
			  $update_result = $this->Teacher->update_accesstoken($accesstoken_exist[0]['id'],$access_code);
		  	$new_access_token = $this->Teacher->get_access_token($accesstoken_exist[0]['id']);
				$access_token = $new_access_token[0]['access_token'];
				$teacher_data = $this->Teacher->get_teacher_data($access_token);
				$group_data = $this->Teacher->get_group_data($teacher_data->id);
				// print_r($group_data);die;
				 if(!empty($group_data)){
				$result = "";
				$result->id = $teacher_data->id;
				$result->access_token = $access_token;
				$result->teacher_id = $teacher_data->created_by;
				$result->group_id = $group_data->id;
				$result->group_name = $group_data->name;
        $result->score = $this->Teacher->check_score_for_chapereone($teacher_data->id);
        $result->success = "success";
			  $this->set_response($result, REST_Controller::HTTP_OK);
			}else{
				$info->error = "No group assign";
				$this->set_response($info, REST_Controller::HTTP_OK);
			}
		 }
	 }else {
		 $info->error = 'Invalid code';
		 $this->set_response($info, REST_Controller::HTTP_OK);
	 }
}


function logout_post()
{
    $id=$this->post('id');
    $accesstoken = $this->post('access_token');
    $this->db->where('access_token', $accesstoken);
    $this->db->where('admin_id', $id);
    $this->db->delete('access_token');

    if(empty($id) && empty($accesstoken)){
    $info->error = 'Invalid entry';
    $this->set_response($info, REST_Controller::HTTP_OK);
    }

  if($this->db->affected_rows() == 1){
    $info->success = 'Logout successfully';
 		$this->set_response($info, REST_Controller::HTTP_OK);
  }else{
    $info->error = 'Session expired';
  	$this->set_response($info, REST_Controller::HTTP_OK);

   }
}

  function reset_password_get(){
    $this->load->view('admin/reset_password');
  }

 function change_password_post(){
   $data = array(
      'email' => $this->post('email'),
      'password' => md5($this->post('password')),
      'modified_time' => time()
      );

    $this->db->where('email', $this->post('email'));
    $this->db->update('admin', $data);
    $rowseffect = $this->db->affected_rows();
    if($rowseffect >0){
      $this->load->view('admin/thanku');
    }
 }
 function forget_password_post(){
	    $email = $this->post('email');
      $encode_email = base64_encode($email);
      $email_verify = $this->Teacher->email_verify($email);
      $name = $this->Teacher->get_teachername_by_email($email);
      	   if ($email_verify != '0'){
							$body = "<!doctype html>
            <html>
            <head>

            </head>
            <body style='background:#eee;'>
            <div style=' text-align:left; width:500px; margin:0 auto; background:#fff; font-family:arial; border-radius:5px; line-height:18px;'>
            <table width='100%' border='0' cellspacing='0' cellpadding='0' style=' border-collapse:collapse; font-family:arial; border-radius:5px; '>
              <tbody>
                <tr>
                  <th style=' border-bottom:#eee solid 1px; color:#7064ca; font-size:18px; text-align:left; padding:10px; font-family:arial;'>Don't worry, we all forget sometimes</th>
                </tr>
                <tr>
                  <td style='color:#333; font-size:14px; text-align:left; padding:2px 10px; font-family:arial;'>&nbsp;</td>
                </tr>
                <tr>
                  <td style='color:#333; font-size:14px; text-align:left; padding:2px 10px; font-family:arial;'>Hi ".$name.",</td>
                </tr>
                <tr>
                  <td style='color:#333; font-size:14px; text-align:left; padding:2px 10px; font-family:arial;'>&nbsp;</td>
                </tr>
                <tr>
                  <td style='color:#333; font-size:14px; text-align:left; padding:2px 10px; font-family:arial;'>You've recently asked to reset the password for this Teacherapp account:
            <a style='text-decoration:underline; color:#7064ca; font-family:arial; ' href=''>".$email."</a></td>
                </tr>
                <tr>
                  <td style='color:#333; font-size:14px; text-align:left; padding:2px 10px; font-family:arial;'>&nbsp;</td>
                </tr>
                <tr>
                  <td style='color:#333; font-size:14px; text-align:left; padding:2px 10px; font-family:arial;'>To update your password, click the button below:</td>
                </tr>
                <tr>
                  <td style='color:#333; font-size:14px; text-align:left; padding:2px 10px; font-family:arial;'>&nbsp;</td>
                </tr>
                <tr>
                  <td style='color:#333; font-size:14px; text-align:left; padding:2px 10px;
                   font-family:arial;'><a style='text-decoration:none !important;
                   color:#fff !important; background:#7064ca; border-radius:5px;
                   display:inline-block; padding:10px; font-family:arial;display: inline-block;  '
                    href=".base_url()."index.php/Teachers/reset_password?token=".$encode_email.">Reset my password</a></td>
                </tr>
                <tr>
                  <td style='color:#333; font-size:14px; text-align:left; padding:2px 10px; font-family:arial;'>&nbsp;</td>
                </tr>
                <tr>
                  <td style='color:#333; font-size:14px; text-align:left; padding:2px 10px; font-family:arial;'>Cheers,<br>
           Team Putnam</td>
                </tr>
                <tr>
                  <td style='color:#333; font-size:14px; text-align:left; padding:2px 10px; font-family:arial;'>&nbsp;</td>
                </tr>

              </tbody>
            </table>
            </div>
            </body>
            </html>";

									/******** EMAIL FUNCTION *********/
								$config = array();
								$config['useragent'] = "CodeIgniter";
								$config['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
								$config['protocol'] = "smtp";
								$config['smtp_host'] = "ssl://smtp.gmail.com";
								$config['smtp_port'] = "465";
								$config['smtp_user'] = "testingteaminficare@gmail.com";
								$config['smtp_pass'] = "inficare123";
								$config['mailtype'] = 'html';
								$config['charset']  = 'utf-8';
								$config['newline']  = "\r\n";
								$config['wordwrap'] = TRUE;
								$this->load->library('email');
								$this->email->initialize($config);
                $this->email->set_newline("\r\n");
								$this->email->from('testingteaminficare@gmail.com', 'Putnum');
								$this->email->to($email_verify[0]->email);
								$this->email->subject("Password Reset");
								$this->email->message($body);
								$this->email->send();
								// $info = $this->email->print_debugger();
								/******** FINAL RESPONSE *********/
              $info->success = 'success';
              $this->set_response($info, REST_Controller::HTTP_OK);
				 }else{
          $info->error = 'Invalid email id';
          $this->set_response($info, REST_Controller::HTTP_OK);
         }

 }


 function get_teacher_grades_get(){
   $grade = $this->Teacher->get_grade_detail_for_teacher($this->get('teacher_id'));
   $itinerary = $this->Teacher->get_itinerary_for_teacher($this->get('teacher_id'));
   $accesstoken_exist = $this->Teacher->chk_access_token($this->get('access_token'));
   $check_teacher = $this->Teacher->check_if_correct_teacher($this->get('teacher_id'));
   if($check_teacher == 0){
      echo json_encode(array("error"=>"Incorrect teacher id"));
   }elseif($accesstoken_exist == 1){  
    echo json_encode(array("grade"=>$grade, "itinerary"=>$itinerary, "success" => "success"));
  }else{
     echo json_encode(array("error"=>"Session expired"));
  }

 }


}?>
