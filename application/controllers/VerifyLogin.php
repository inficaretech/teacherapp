<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class VerifyLogin extends CI_Controller {

 function __construct()
 {
   parent::__construct();
   $this->load->model('Teacher','',TRUE);
   $this->load->library('session');
 
 }

 function index()
 {
    $this->load->library('form_validation');
    $this->form_validation->set_rules('email', 'Email', 'trim|required');
    $this->form_validation->set_rules('password', 'Password', 'trim|required|callback_check_database');
   if($this->form_validation->run() == FALSE)
   {
     //Field validation failed.  User redirected to login page
     $this->load->view('admin/login');
   }
   else
   {
     //Go to private area
    redirect('Categories/index', 'refresh');
  //  print_r($_SESSION);
   //echo 'Success'; die();
   }

 }

 function signup_post()
 {
   //This method will have the credentials validation
   $this->load->library('form_validation');
   $this->form_validation->set_rules('email', 'Email', 'trim|required');
   $this->form_validation->set_rules('password', 'Password', 'trim|required|callback_check_database');
   if($this->form_validation->run() == FALSE )
   {
     //Field validation failed.  User redirected to login page
     $this->load->view('admin/login');

   }
   else
   {
     //Go to private area
    redirect('home', 'refresh');
   //  print_r($_SESSION);
   // echo 'Success'; die();
   }

 }


 function check_database($password)
 {
   //Field validation succeeded.  Validate against database
   $email = $this->input->post('email');
   //query the database
   $result = $this->Teacher->login($email, $password);
   if($result)
   {
     $sess_array = array();
     foreach($result as $row)
     {
       $sess_array = array(
         'id' => $row->id,
         'email' => $row->email,
         'password' => $row->password,
         'role' => $row->role
       );
      $this->session->set_userdata('logged_in', $sess_array);
     }
     return TRUE;
   }
   else
   {
     $this->form_validation->set_message('check_database', 'Invalid email or password');
     return false;
   }
 }
}
?>
