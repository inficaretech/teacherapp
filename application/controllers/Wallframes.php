<?php
	error_reporting(0);
	defined('BASEPATH') OR exit('No direct script access allowed');
	require (APPPATH.'libraries/REST_Controller.php');

 class Wallframes extends REST_Controller
 {

 	function __construct()
 	{
 	  parent::__construct();
      $this->load->model('Wallframe');
 	}

//  function index_get(){
// 	$chaperon = $this->Wallframe->get_chaperon_entries();
// 	foreach ($chaperon as $k1 => $value1) {
// 		$group_score = $this->Wallframe->get_group_score($value1->id);
// 		foreach ($group_score as $k2 => $value2) {
// 			$group = $this->Wallframe->get_group($value2->group_id);
// 			$result[$k1]['name'] = $value1->name;
// 			$result[$k1]['school'] = $value1->school;
// 			$result[$k1]['score'] = $value2->score;
// 			$result[$k1]['group'] = $group[0]->name;
// 			}
//  		}
//  		if(empty($result)){
//  		$this->set_response(array(), REST_Controller::HTTP_OK);
//  		}else{
// 		$this->set_response(array("result"=>array_values($result),"success"=>"success"), REST_Controller::HTTP_OK);
// 	}
// }


 	function index_get(){
 	   $posts = array();
 	   $grades = array('1','2','3','4','5');
 	   $gradetype = $this->config->item('grade_array'); 
 	   foreach ($grades as $key => $value) {
 	   	  $this->db->select('id,grade,teacher_id,MAX(score) as score');
          $this->db->from('group_score');
          $this->db->where('grade', $grades[$key]);
          $this->db->group_by('grade');
          $this->db->order_by('	created_time','asc');
          $query = $this->db->get();
           // echo $this->db->last_query(); die;
          $result = $query->result_array();
          foreach ($result as $value) {
          	$results = "";
          	$grade = $value['grade'];
          	$results->id = $value['id'];
          	$results->grade = $gradetype[$grade];
          	$results->teacher = $this->Wallframe->get_teacher_name_by_id($value['teacher_id']);
          	$results->school = $this->Wallframe->get_teacher_school_by_id($value['teacher_id']);
          	$results->score = $value['score'];
          	array_push($posts, $results);
          }
 	   }
 	   $this->set_response(array("result"=>$posts,"success"=>"success"), REST_Controller::HTTP_OK);
 	}


}
?>
