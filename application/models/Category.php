<?php

/**
*
*/
class Category extends CI_Model
{
	function __construct()
	{
	  parent::__construct();
	}

	public function get_entries(){
 	 $query = $this->db->get('master_category');
     return $query->result();
	}

	function Chk_data_exist($category, $id){
	  $this->db->select('*');
      $this->db->from('master_category');
      $this->db->where('category',$category);
			$this->db->where('id !=',$id);
		//		echo $this->db->last_query(); die;
      $query = $this->db->get();
	  if($query->num_rows() > 0){
	  	$rows = '1';
	  }else{
	  	$rows = '0';
	  }
	  return $rows;
	}

	function chk_access_token($access_token){
	  $this->db->select('*');
      $this->db->from('access_token');
      $this->db->where('access_token',$access_token);
      $query = $this->db->get();
   // echo $this->db->last_query(); 
	  if($query->num_rows() > 0){
	  	$rows = '1';
	  }else{
	  	$rows = '0';
	  }
	  return $rows;
	}

	function get_category_id_by_subcategory($subcat_id){
	  $this->db->select('category_id');
      $this->db->from('master_subcategory');
      $this->db->where('id',$subcat_id);
      $query = $this->db->get();
	  $result = $query->result_array();
	  foreach ($result as $value) {
	  	  $id = $value['category_id'];
	  	}	

	  	if(!empty($id)){
	  		return $id;
	  	}else{
	  		return "";
	  	}

	}
	// function check_if_selected_category($teacher_id, $category_id){
	//   $this->db->select('*');
 //      $this->db->from('itinerary');
 //      $this->db->where('admin_id',$teacher_id);
 //      $query = $this->db->get();
 //      $result = $query->result_array();
 //      // echo $this->db->last_query();
 //      foreach ($result as $key => $value) {
 //       $categoryid = $this->get_category_id_by_subcategory($result['master_subcategory_id'][$key]);
 //          if($categoryid == $category_id){
	//       	return "1";
	//       }else{
	//       	return "0";
	//       }
	//   }

	// }


	function check_if_selected_category($teacher_id, $category_id){
	  $this->db->select('*');
      $this->db->from('master_subcategory');
      $this->db->where('category_id',$category_id);
      $query = $this->db->get();
      $result = $query->result_array();

      foreach ($result as $value) {
      	$subcat_id = $value['id'];
      	$this->db->select('*');
        $this->db->from('itinerary');
        $this->db->where('admin_id',$teacher_id);
        $this->db->where('master_subcategory_id',$subcat_id);
        $query1 = $this->db->get();
        $result1 = $query1->result_array();
      }

      if(!empty($result1)){
      	return "1";
      }else{
      	return "0";
      }
	}
}

?>
