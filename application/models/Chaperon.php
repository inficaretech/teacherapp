<?php
/**
*
*/
class Chaperon extends CI_Model
{

	function __construct()
	{
	 parent::__construct();
	}

	function get_chaperon_entries($id){
	  	$this->db->select('*');
		$this->db->from('chaperon');
		$this->db->where('created_by',$id);
	  	$query = $this->db->get();
		return $query->result();
	}


	function check_if_chaperon_already_assigned($chaperon_id){
		$posts = array();
		$this->db->select('*');
		$this->db->from('group_chaperon');
		$this->db->where('chaperon_id',$chaperon_id);
	  	$query = $this->db->get();
	  	$result =$query->result();
	  	// echo $this->db->last_query(); die;
	  	if(empty($result)){
	  		$response = 0;
	  	}else{
	  		$response = 1;
	  	}
	  	return $response;
	}

	function get_group_id_by_chep_id($chaperon_id){
		$posts = array();
		$this->db->select('*');
		$this->db->from('group_chaperon');
		$this->db->where('chaperon_id',$chaperon_id);
	  	$query = $this->db->get();
	  	// echo $this->db->last_query(); die;
	  	$result =$query->result_array();
		foreach ($result as $value) {
			$id = $value['group_id'];
		}
		return $id;
	}

	function get_type_by_chep_id($chaperon_id){
		$posts = array();
		$this->db->select('*');
		$this->db->from('group_chaperon');
		$this->db->where('chaperon_id',$chaperon_id);
	  	$query = $this->db->get();
	  	// echo $this->db->last_query(); die;
	  	$result =$query->result_array();
		foreach ($result as $value) {
			$type = $value['type'];
		}
		return $type;
	}

	function get_chaperon_entries_for_group($id){
		$posts =array();
	  	$this->db->select('*');
		$this->db->from('chaperon');
		$this->db->where('created_by',$id);
	  	$query = $this->db->get();
	  	// echo $this->db->last_query(); die;
	  	$result =  $query->result_array();
	  	foreach ($result as $value) {
	  		$results = "";
	  		$results->chaperon_id= $value['id'];
	  		$results->name = $value['name'];
	  		if($this->check_if_chaperon_already_assigned($value['id']) == 1){
	  		$results->group_id = $this->get_group_id_by_chep_id($value['id']);
	  		$results->type = $this->get_type_by_chep_id($value['id']);
	  		}else{
	  			$results->group_id = "";
	  			$results->type = "";
	  		}
	  		array_push($posts, $results);
	  	}

	  	return $posts;
	}
	function chk_access_token($access_token){
	  $this->db->select('*');
      $this->db->from('access_token');
      $this->db->where('access_token',$access_token);
      $query = $this->db->get();
     // echo $this->db->last_query();
	  if($query->num_rows() > 0){
	  	$rows = '1';
	  }else{
	  	$rows = '0';
	  }
	  return $rows;
	}

	function chk_id($id){
		$this->db->select('*');
			$this->db->from('chaperon');
			$this->db->where('id',$id);
			$query = $this->db->get();
		if($query->num_rows() > 0){
			$rows = '1';
		}else{
			$rows = '0';
		}
		return $rows;
	}



	// 		function assign_group_chaperon_insert($chaperon_id, $group_id, $type, $teacher_id){

	// 	 $data = array( 'chaperon_id' => $chaperon_id,
	// 	                'group_id' => $group_id,
	// 	                'type' => $type,
	// 	                'modified_time' => time()
	// 	                );
	// 		$insert = $this->db->insert('group_chaperon',$data);
 //    	if($insert){
	// 		return "1";
	// 	}else{
	// 		return "0";
	// 	}
	// }


		function check_if_correct_chaperon_id($chaperon_id){
		  $this->db->select('*');
	      $this->db->from('chaperon');
	      $this->db->where('id',$chaperon_id);
	      $query = $this->db->get();
	      if($query->num_rows() > 0){
		  	$rows = '1';
		  }else{
			$rows =  '0';
		  }
		  return $rows;
		}



		function check_if_correct_teacher_id($teacher_id){
		  $this->db->select('*');
	      $this->db->from('admin');
	      $this->db->where('id',$teacher_id);
	      $query = $this->db->get();
	      if($query->num_rows() > 0){
		  	$rows = '1';
		  }else{
			$rows =  '0';
		  }
		  return $rows;
		}


		function chek_chaperon_for_same_group($chaperon_id){
			$this->db->select('*');
				$this->db->from('group_chaperon');
				$this->db->where('chaperon_id',$chaperon_id);
				$query = $this->db->get();
				if($query->num_rows() > 0){
				$rows = '1';
			}else{
			$rows =  '0';
			}
			return $rows;
		}


		function check_if_correct_group_id($group_id){
			$this->db->select('*');
				$this->db->from('groups');
				$this->db->where('id',$group_id);
				$query = $this->db->get();
				if($query->num_rows() > 0){
				$rows = '1';
			}else{
			$rows =  '0';
			}
			return $rows;
		}


    function email_verify($email)
     {
       $this ->db-> select('*');
       $this ->db-> from('chaperon');
       $this ->db-> where('email', $email);
       $this ->db-> limit(1);
       $query = $this->db->get();
       $result = $query->result();
       if($query->num_rows() == 1)
       {
         return $result[0];
       }
       else
       {
         return '0';
       }
     }


     function get_group_data($id)
       {
       $this ->db-> select('*');
       $this ->db-> from('group_chaperon');
       $this ->db-> where('id', $id);
       $this ->db-> limit(1);
       $query = $this->db->get();
       $result = $query->result();
       if($query->num_rows() == 1)
       {
         return $result[0];
       }
       else
       {
         return '0';
       }
     }



     function get_chaperon_data($id)
       {
       $this ->db-> select('*');
       $this ->db-> from('chaperon');
       $this ->db-> where('id', $id);
       $this ->db-> limit(1);
       $query = $this->db->get();
       $result = $query->result();
       if($query->num_rows() == 1)
       {
         return $result[0];
       }
       else
       {
         return '0';
       }
     }






}

?>
