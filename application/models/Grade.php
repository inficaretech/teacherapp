<?php
/**
*
*/
class Grade extends CI_Model
{

	function __construct()
	{
	 parent::__construct();
	}


  function get_all()
  {
    $query = $this->db->get('grades');
    $result = $query->result();
     return $result;
  }


  function student_find($id){
    $this->db->select('*');
    $this->db->from('student');
		$this->db->where_in('grade_id', $id);
		$query = $this->db->get();
    $result = $query->result();
    return $result;
  }

	function get_all_groups(){
		$this->db->select('*');
		$this->db->from('groups');
		$query = $this->db->get();
    $result = $query->result();
    return $result;
		}


  function chk_access_token($access_token){
    $this->db->select('*');
      $this->db->from('access_token');
      $this->db->where('access_token',$access_token);
      $query = $this->db->get();
     // echo $this->db->last_query();
    if($query->num_rows() > 0){
      $rows = '1';
    }else{
      $rows = '0';
    }
    return $rows;
  }


    function check_if_correct_teacher_id($teacher_id){
      $this->db->select('*');
      $this->db->from('admin');
      $this->db->where('id',$teacher_id);
      $this->db->where('role',2);
      $query = $this->db->get();
     // echo $this->db->last_query(); die;
      if($query->num_rows() > 0){
       $rows = '1';
      }else{
       $rows =  '0';
       }
    return $rows;   
  } 	





}

  ?>
