<?php
class Group extends CI_Model
{

	function __construct()
	{
	 parent::__construct();
	}

  function get_entries(){
    $query = $this->db->get('groups');
     return $query->result();
   }

	 function group_score_get_all(){
     $query = $this->db->get('group_score');
     $result = $query->result();
      return $result;
   }


	function get_group_name_by_id($id){
	$this->db->select('*');
	$this->db->from('groups');
	$this->db->where('id',$id);
	$query = $this->db->get();
	$response = $query->result_array();
	return  $response[0]['name'];
	}

	function Chk_data_exist($name, $id){
		$this->db->select('*');
		$this->db->from('groups');
		$this->db->where('name',$name);
		$this->db->where('id !=',$id);
		//echo $this->db->last_query(); die;
		$query = $this->db->get();
		if($query->num_rows() > 0){
				$rows = '1';
			}else{
				$rows = '0';
			}
			return $rows;
		}

    function chk_access_token($access_token){
	  $this->db->select('*');
      $this->db->from('access_token');
      $this->db->where('access_token',$access_token);
      $query = $this->db->get();
     // echo $this->db->last_query();
	  if($query->num_rows() > 0){
	  	$rows = '1';
	  }else{
	  	$rows = '0';
	  }
	  return $rows;
	}

	function check_student_id_for_group($student_id){
	  $this->db->select('*');
      $this->db->from('group_students');
      $this->db->where('student_id',$student_id);
      $query = $this->db->get();
  	  if($query->num_rows() > 0){
	  	$rows = '1';
	  }else{
	  	$rows = '0';
	  }
	  return $rows;
	}


	function chek_student_for_same_group($student_id, $group_id){
	  $this->db->select('*');
      $this->db->from('group_students');
      $this->db->where('student_id',$student_id);
      $this->db->where('group_id',$group_id);
      $query = $this->db->get();
      $response = $query->result_array();
      if($query->num_rows() > 0){
	  	$rows = '1';
		  }else{
			$rows =  '0';
		  }
	  return $rows;
	}


	function check_if_correct_student_id($student_id){
	  $this->db->select('*');
      $this->db->from('student');
      $this->db->where('id',$student_id);
      $query = $this->db->get();
      //echo $this->db->last_query(); die;
      if($query->num_rows() > 0){
	  	$rows = '1';
	  }else{
		$rows =  '0';
	  }
	  return $rows;
	}

	function check_if_correct_teacher_id($admin_id){
	  $this->db->select('*');
      $this->db->from('admin');
      $this->db->where('id',$admin_id);
      $query = $this->db->get();
      //echo $this->db->last_query(); die;
      if($query->num_rows() > 0){
	  	$rows = '1';
	  }else{
		$rows =  '0';
	  }
	  return $rows;
	}


	function check_if_correct_group_id($group_id){
	  $this->db->select('*');
      $this->db->from('groups');
      $this->db->where('id',$group_id);
      $query = $this->db->get();
      //echo $this->db->last_query(); die;
      if($query->num_rows() > 0){
	  	$rows = '1';
	  }else{
		$rows =  '0';
	  }
	  return $rows;
	}


	function get_students_by_group_id($group_id, $teacher_id){
		$posts = array();
  	  $this->db->select('student.*');
      $this->db->from('group_students');
      $this->db->join('student', "group_students.student_id = student.id and student.created_by = $teacher_id", 'inner');
      $this->db->where('group_students.group_id', $group_id);
      $query = $this->db->get();
      // echo $this->db->last_query();die;
        if($query->num_rows() > 0){
     		 $result = $query->result_array();
     		 foreach ($result as $value) {
     		 	$results = "";
     		 	$results->id = $value['id'];
     		 	$results->f_name = $value['f_name'];
     		 	$results->l_name = $value['l_name'];
     		 if(!empty($value['contact1'])){
     		 	$results->contact1 = $value['contact1'];
     		 }else{
     		 	$results->contact1 = "";
     		 }
     		 if(!empty($value['contact2'])){
     		 	$results->contact2 = $value['contact2'];
     		 }else{
     		 	$results->contact2 = "";
     		 }


     		 if(!empty($value['class'])){
     		 	$results->class = $value['class'];
     		 }else{
     		 	$results->class = "";
     		 }
     		 	$results->modified_time = $value['modified_time'];

     		 	array_push($posts, $results);

     		 }
     		 return $posts;
     	 }else{
     	 	return "0";
      	}
	}

	function assign_group_student_insert($student_id,$group_id,$teacher_id){

		 $data = array( 'student_id' => $student_id,
		                'group_id' => $group_id,
		                'created_by' => $teacher_id,
		                'modified_time' => time()
		                );
		$insert = $this->db->insert('group_students',$data);
    if($insert){
			return "1";
		}else{
			return "0";
		}
	}

	function assign_group_student_update($student_id,$group_id,$teacher_id){

		 $data = array( 'student_id' => $student_id,
										'group_id' => $group_id,
										'created_by' => $teacher_id,
										'modified_time' => time()
										);
										 $this->db->where('student_id',$student_id);
										$update = $this->db->update('group_students',$data);
		  if($update){
				return "1";
			}else{
				return "0";
			}
	}




	 function get_score_detail(){
	  $this->db->select('student.*');
      $this->db->from('group_students');
      $this->db->join('student', 'group_students.student_id = student.id', 'inner');
      $this->db->where('group_students.group_id', $group_id);
      $query = $this->db->get();
        if($query->num_rows() > 0){
     		 return $query->result();
     	 }else{
      		return "No student for this group";
      	}
	 }


	//  function get_student_detail_by_grp_id($grp_id){
	//   $this->db->select('*');
 //      $this->db->from('group_students');
 //      $this->db->where('group_id',$grp_id);
 //      $query = $this->db->get();
 //      $result = $this->
	//   return $rows;
	// }

	function get_grade_by_group_id($group_id){
	  $this->db->select('student_id');
      $this->db->from('group_students');
      $this->db->where('group_id',$grp_id);
      $query = $this->db->get();
      $result = $this->$query->result();

      foreach ($result as $grpdetails) {
     	 $grpdetails = "";
      }
	  return $rows;


	}



}

?>
