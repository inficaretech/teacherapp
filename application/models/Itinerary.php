<?php
error_reporting(0);
class Itinerary extends CI_Model
{

	function __construct()
	{
	 parent::__construct();
	}

	function chk_access_token($access_token){
	  $this->db->select('*');
      $this->db->from('access_token');
      $this->db->where('access_token',$access_token);
      $query = $this->db->get();
     // echo $this->db->last_query();
	  if($query->num_rows() > 0){
	  	$rows = '1';
	  }else{
	  	$rows = '0';
	  }
	  return $rows;
	}

  // function get_current_itinerary($teacher_id,grade){
  //   $date = $this->get_itinerary_date_teacher_id();
  //   $grade = 
  // }

	function get_itinerary_teacher($teacher_id, $grade, $datetime){
    $imageurl = base_url()."uploads/subcategory_images/";
		$posts = array();
 	  $this->db->select('master_subcategory.*,itinerary.itinerary_datetime ');
      $this->db->from('itinerary');
      $this->db->join('master_subcategory', 'itinerary.master_subcategory_id = master_subcategory.id', 'inner');
      $this->db->where('itinerary.admin_id', $teacher_id);
      $this->db->where('itinerary.grade', $grade);
      $this->db->where('itinerary.itinerary_datetime', $datetime);
      $query = $this->db->get();
        // echo $this->db->last_query();
      $data = $query->result_array();
       if($query->num_rows() > 0){
     		 foreach($data as $iti){
     		 	$result = "";
          $result->id = $iti['id'];
     		 	$result->title = $iti['title'];
     		 	$result->description = $iti['description'];
     		 	// $result->datetime = $iti['itinerary_datetime'];
          $result->time_duration = $iti['time_duration'];
            if(!empty($iti['image'])){
             $result->image = $imageurl.$iti['image'];
            }else{
             $result->image = "";
            }
     		 	array_push($posts, $result);
     		 }
     		 return $posts;
     	 }else{
      		return $posts;
      	}

	}


  function get_itinerary_date_teacher_id($teacher_id,$grade ){
    $posts = array();
    $this->db->select('master_subcategory.*,itinerary.itinerary_datetime ');
      $this->db->from('itinerary');
      $this->db->join('master_subcategory', 'itinerary.master_subcategory_id = master_subcategory.id', 'inner');
      $this->db->where('itinerary.admin_id', $teacher_id);
      $this->db->where('itinerary.grade', $grade);
      $query = $this->db->get();
      $data = $query->result_array();
       if($query->num_rows() > 0){
         foreach($data as $iti){
          $result = $iti['itinerary_datetime'];
         }
         return $result;
       }else{
          return "";
        }

  }


  function check_if_correct_teacher_id($teacher_id){
      $this->db->select('*');
      $this->db->from('admin');
      $this->db->where('id',$teacher_id);
      $this->db->where('role',2);
      $query = $this->db->get();
     // echo $this->db->last_query(); die;
      if($query->num_rows() > 0){
       $rows = '1';
      }else{
       $rows =  '0';
       }
    return $rows;
  }


function check_if_saved_itinerary($teacher_id,$item_id){
         $this->db->select('*');
         $this->db->from('itinerary');
         $this->db->where('master_subcategory_id',$item_id);
         $this->db->where('admin_id',$teacher_id);
         $query = $this->db->get();
         // echo $this->db->last_query();
         $result =  $query->result();
          // print_r($result);
        if($query->num_rows() > 0){
          $rows = '1';
      }else{
        $rows = '0';
      }
      return $rows;
      }


  function recom_itinerary($teacher_id){
    $imageurl = base_url()."uploads/subcategory_images/";
    $posts =array();
    $query = $this->db->get('recommanded_items');
    $result = $query->result();
    if(!empty($result)){
    foreach($result as $rec_item){
      $result = "";
      $result->id = $rec_item->master_subcategory_id;
      $this->db->select('*');
      $this->db->from('master_subcategory');
      $this->db->where('id',$result->id);
      $query = $this->db->get();
      // $result1 = "";
      $result1 = $query->result_array();
      foreach($result1 as $iti){
          $result2 = "";
          $result2->id = $iti['id'];
          $result2->title = $iti['title'];
          $result2->description = $iti['description'];
          // $result->datetime = $iti['itinerary_datetime'];
          $result2->time_duration = $iti['time_duration'];
            if(!empty($iti['image'])){
             $result2->image = $imageurl.$iti['image'];
            }else{
             $result2->image = "";
            }
            // echo $teacher_id;
          $result2->already_saved = $this->check_if_saved_itinerary($teacher_id, $iti['id']);

          array_push($posts, $result2);
         }
     }
    }else{ $posts = "No items"; }
     return $posts;
  }


  function get_recom_itinerary_title(){
    $this->db->select('rec_title');
    $this->db->from('recommanded_items');
    $this->db->limit('1');
    $query = $this->db->get();
    // echo $this->db->last_query();
    $result = $query->result_array();
    foreach ($result as $value) {
      $title = $value['rec_title'];
    }
    return $title;
  }


  function check_if_correct_sub_category_id($id){
    $this->db->select('*');
      $this->db->from('master_subcategory');
      $this->db->where('id',$id);
      $query = $this->db->get();
       // echo $this->db->last_query(); die;
      if($query->num_rows() > 0){
      $rows = '1';
    }else{
    $rows =  '0';
    }
    return $rows;
  }


  function check_if_data_present($subcat_id, $teacher_id, $grade){
    $this->db->select('*');
      $this->db->from('itinerary');
      $this->db->where('admin_id',$teacher_id);
      $this->db->where('master_subcategory_id',$subcat_id);
      $this->db->where('grade',$grade);
      $query = $this->db->get();
      // echo $this->db->last_query(); die;
      if($query->num_rows() > 0){
      $rows = '1';
    }else{
    $rows =  '0';
    }
    return $rows;
  }

  function add_to_itinerary($teacher_id, $subcat_id, $itinry_date, $grade, $trip_id){
		$data1 = array('itinerary_datetime' => $itinry_date);
    $data = array(
      'admin_id' => $teacher_id,
      'master_subcategory_id' => $subcat_id,
      'itinerary_datetime' => $itinry_date,
      'grade' => $grade,
      'trip_id' => $trip_id,
      'created_by' => $teacher_id,
      'modified_time' => time()
      );
   // print_r($data); die;
    $this->db->select('*');
    $this->db->from('itinerary');
    $this->db->where('admin_id',$teacher_id);
    $this->db->where('master_subcategory_id',$subcat_id);
    $this->db->where('itinerary_datetime',$itinry_date);
    $this->db->where('grade',$grade);
    $query = $this->db->get();
     // echo $this->db->last_query(); die;
			// $this->db->where('admin_id', $teacher_id);
			// $this->db->update('itinerary',$data1);
      if($query->num_rows() <=  0){
        $result =  $this->db->insert('itinerary', $data);
     }else{
        return "2";
     }
    if(!empty($result)){
      return  "1";
    }else{
      return "0";
    }

  }
function chk_grade_and_date($grade,$teacher_id,$itinerary_date){
    $this->db->select('*');
    $this->db->from('itinerary');
    $this->db->where('admin_id',$teacher_id);
    $this->db->where('grade',$grade);
    $this->db->where('itinerary_datetime',$itinerary_date);
    $query = $this->db->get();

    //  echo $this->db->last_query();
    if($query->num_rows() > 0){
      return "1";
   }else{
      return "2";
   }
  }



  function chk_trip_id($grade,$teacher_id,$itinerary_date){
    $this->db->select('trip_id');
    $this->db->from('itinerary');
    $this->db->where('admin_id',$teacher_id);
    $this->db->where('grade',$grade);
    $this->db->where('itinerary_datetime',$itinerary_date);
    $query = $this->db->get();
    $result = $query->result_array();
      // echo $this->db->last_query();
    if($query->num_rows() >  0){
      return $result[0]['trip_id'];
   }else{
      return "2";
   }

  }

  function chk_max_trip_id($teacher_id){
    $this->db->select('max(id)');
    $this->db->from('trip');
    $this->db->where('admin_id',$teacher_id);
    $query = $this->db->get();
    $result = $query->result_array();
      // echo $this->db->last_query();
    if($query->num_rows() >  0){
      return $result[0]['max(id)'];
   }else{
      return "2";
   }
  }


}

?>
