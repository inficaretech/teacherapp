<?php

/**
*
*/
class Quescategory extends CI_Model
{

	function __construct()
	{
	 parent::__construct();
	}

	function get_entries(){
		$query = $this->db->get('master_question_category');
        return $query->result();
	}

	function get_all_categories(){
		$this->db->select('*');
        $this->db->from('master_category');
        $this->db->order_by('category','asc');
        $query = $this->db->get();
       return  $query->result();
	}

	function get_all_entries_with_category_name(){
	   $this->db->select('master_question_category.*,master_category.category');
       $this->db->from('master_question_category');
       $this->db->join('master_category', 'master_question_category.master_category_id = master_category.id', 'right outer');
       $this->db->where('master_question_category.master_category_id IS NOT NULL', null, false);
       $query = $this->db->get();
      return $query->result();
	}

	function get_category_name_by_id($id){
		$this->db->select('category');
        $this->db->from('master_category');
        $this->db->where('id',$id);
        $query = $this->db->get();
        $response = $query->result_array();
    	return  $response[0]['category'];
	}

	function Chk_data_exist($name, $id){
		$this->db->select('*');
			$this->db->from('master_question_category');
			$this->db->where('question_category_name',$name);
			$this->db->where('id !=',$id);
		//		echo $this->db->last_query(); die;
			$query = $this->db->get();
		if($query->num_rows() > 0){
			$rows = '1';
		}else{
			$rows = '0';
		}
		return $rows;
	}


}



?>
