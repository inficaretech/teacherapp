<?php
class Question extends CI_Model
{

	function __construct()
	{
	 parent::__construct();
	}

  function get_entries(){
    $query = $this->db->get('master_question_category');
        return $query->result();
  }

	function get_questions_entries(){
		$query = $this->db->get('questions');
				return $query->result();
	}

	function get_question_name_by_id($id){
		$this->db->select('*');
				$this->db->from('questions');
				$this->db->where('id',$id);
				$query = $this->db->get();
				$response = $query->result_array();
			return  $response[0]['question'];
	}


	function get_ques_category_name_by_id($id){
		$this->db->select('question_category_name');
		$this->db->from('master_question_category');
		$this->db->where('id',$id);
		$query = $this->db->get();
		$response = $query->result_array();
	return  $response[0]['question_category_name'];
	}


	function Chk_data_exist($name, $id){
		$this->db->select('*');
			$this->db->from('questions');
			$this->db->where('question',$name);
			$this->db->where('id !=',$id);
		//echo $this->db->last_query(); die;
			$query = $this->db->get();
		if($query->num_rows() > 0){
			$rows = '1';
		}else{
			$rows = '0';
		}
		return $rows;
	}


	// function get_all_ques(){
	//   $this->db->select('master_question_category.question_category_name,questions.*');
 //      $this->db->from('master_question_category');
 //      $this->db->join('questions', 'master_question_category.id = questions.master_question_category_id', 'inner');
 //      $query = $this->db->get();
 //      // /echo $this->db->last_query();
 //        if($query->num_rows() > 0){
 //     		 return $query->result();
 //     	 }else{
 //      		return "No data";
 //      	}
	// }

	function get_questin_by_category_id($category_id){
	 $posts = array();
	 $this->db->select('*');
	 $this->db->from('questions');
	 $this->db->where('master_question_category_id' , $category_id);
	 $query = $this->db->get();
	 $allques = $query->result_array();
	  $i = 1;
	 foreach ($allques as $result1) {

	 		$result = "";
	 		$result->id= $result1['id'];
	 		$result->Question= "Q".$i."."." ".$result1['question']. "?";
	 		$result->Answer1= $result1['ans_opt1'];
	 		$result->Answer2= $result1['ans_opt2'];
	 		$result->Answer3= $result1['ans_opt3'];
	 		$result->Answer4= $result1['ans_opt4'];
	 		$result->Correct_answer= $result1['correct_answer'];
	 		$result->Marks= $result1['marks'];
	 		array_push($posts, $result);
	 $i++;}
	 return $posts;
	}

	function get_grade_by_teacher_id($teacher_id){
	  $this->db->select('grade_type');
      $this->db->from('admin');
      $this->db->where('id',$teacher_id);
      $query = $this->db->get();
      $result = $query->result_array();
      foreach ($result as $value) {
      	$grade = $value['grade_type'];
      }
	  return $grade;
	}

	
	function get_all_ques($teacher_id){
		$posts = array();
		$grade = $this->get_grade_by_teacher_id($teacher_id);
		$this->db->select('*');
		$this->db->from('master_question_category');
	 $query = $this->db->get();
	 $allques_cat = $query->result_array();
	
	 foreach ($allques_cat as $result1) {

	 		$result = "";
	 		$result->QuestionCategory= $result1['question_category_name'];
	 		$result->Questions=$this->get_questin_by_category_id($result1['id']);
	 		array_push($posts, $result);
	 		
	 }
	 return $posts;
	}

	 function chk_access_token($access_token){
	  $this->db->select('*');
      $this->db->from('access_token');
      $this->db->where('access_token',$access_token);
      $query = $this->db->get();
      //echo $this->db->last_query();
	  if($query->num_rows() > 0){
	  	$rows = '1';
	  }else{
	  	$rows = '0';
	  }
	  return $rows;
	}

	function check_if_correct_group_id($group_id){
	  $this->db->select('*');
      $this->db->from('groups');
      $this->db->where('id',$group_id);
      $query = $this->db->get();
      //echo $this->db->last_query(); die;
      if($query->num_rows() > 0){
	  	$rows = '1';
	  }else{
		$rows =  '0';
	  }
	  return $rows;
	}

	function check_if_correct_chepron_id($cheprn_id){
	  $this->db->select('*');
      $this->db->from('chaperon');
      $this->db->where('id',$cheprn_id);
      $query = $this->db->get();
      //echo $this->db->last_query(); die;
      if($query->num_rows() > 0){
	  	$rows = '1';
	  }else{
		$rows =  '0';
	  }
	  return $rows;
	}

	function check_if_correct_question_id($question_id){
	  $this->db->select('*');
      $this->db->from('questions');
      $this->db->where('id',$question_id);
      $query = $this->db->get();
      if($query->num_rows() > 0){
	  	$rows = '1';
	  }else{
		$rows =  '0';
	  }
	  return $rows;
	}


	function get_correct_ans_by_question_id($question_id){
	  $this->db->select('correct_answer');
      $this->db->from('questions');
      $this->db->where('id',$question_id);
      $query = $this->db->get();
      $result = $query->result_array();
      foreach ($result as $value) {
      	$correct_answer = $value['correct_answer'];
      }
	  return $correct_answer;
	}

	function get_marks_by_question_id($question_id){
	  $this->db->select('marks');
      $this->db->from('questions');
      $this->db->where('id',$question_id);
      $query = $this->db->get();
      $result = $query->result_array();
      foreach ($result as $value) {
      	$marks = $value['marks'];
      }
	  return $marks;
	}

	function get_half_marks_by_question_id($question_id){
	  $this->db->select('marks');
      $this->db->from('questions');
      $this->db->where('id',$question_id);
      $query = $this->db->get();
      $result = $query->result_array();
      foreach ($result as $value) {
      	$marks = ($value['marks'])/2;
      }
	  return $marks;
	}

	function update_group_scores($group_id, $chep_id, $score){
	 $data = array('group_id'=> $group_id,
	 			   'cheperone_id' => $chep_id,
	 			   'score' => $score);
	  $this->db->select('*');
      $this->db->from('group_score');
      $this->db->where('cheperone_id',$chep_id);
      $this->db->where('group_id',$group_id);
      $query = $this->db->get();
       if($query->num_rows() > 0){
       	 $this->db->where('cheperone_id',$chep_id);
         $this->db->where('group_id',$group_id);
         $this->db->update('group_score', $data);
         // echo $this->db->last_query();
       }else{
       	  $this->db->insert('group_score', $data);
       	  echo $this->db->last_query();
       }
	}

	function get_group_total_score($group_id, $cheperone_id){
	  $this->db->select('score');
      $this->db->from('group_score');
      $this->db->where('group_id',$group_id);
      $this->db->where('cheperone_id',$cheperone_id);
      $query = $this->db->get();
      $result = $query->result_array();
      foreach ($result as $value) {
      	$score = $value['score'];
      }
	  return $score;
	}

	function get_chaperon($id){
		  $this->db->select('created_by');
			$this->db->from('chaperon');
			$this->db->where('id',$id);
			$query = $this->db->get();
			$result = $query->result_array();
			return $result[0];
	}

	function get_teacher($id){
		$this->db->select('*');
		$this->db->from('admin');
		$this->db->where('id',$id);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result[0];
	}

}

?>
