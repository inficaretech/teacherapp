<?php
/**
*
*/
class Student extends CI_Model
{

	function __construct()
	{
	 parent::__construct();
	}

	// function get_students_entries($id){
	// 	$this->db->select('*');
	// 		$this->db->from('student');
	// 		$this->db->where('created_by',$id);
	//     $query = $this->db->get();
	// 		return $query->result();
	// }

	function get_students_entries($id){
	  $posts = array();
	  $this->db->select('student.*,student.id as student_id,student_checklist.*');
      $this->db->from('student');
      $this->db->join('student_checklist', 'student.id = student_checklist.student_id', 'inner');
      $this->db->where('student.created_by', $id);
      $query = $this->db->get();
      $result1 = $query->result_array();
      foreach ($result1 as $student) {
     		$result="";
			$result->id = $student['student_id'];
      		$result->f_name = $student['f_name'];
			$result->l_name = $student['l_name'];
			// $result->contact1 = $student['contact1'];
			// $result->contact2 = $student['contact2'];
			// $result->class = $student['class'];
			$this->db->select('grade');
			$this->db->from('grades');
			$this->db->like('id', $student['grade_id']);
			$q = $this->db->get();
			$r = $q->result_array();
			// $result->grade =$class = $r[0]['grade'];

			 if(!empty($student['contact1'])){
     		 	$result->contact1 = $student['contact1'];
     		 }else{
     		 	$result->contact1 = "";
     		 }

     		 if(!empty($student['contact2'])){
     		 	$result->contact2 = $student['contact2'];
     		 }else{
     		 	$result->contact2 = "";
     		 }


     		 if(!empty($student['class'])){
     		 	$result->class = $student['class'];
     		 }else{
     		 	$result->class = "";
     		 }
			$result->created_by = $student['created_by'];
			$result->modified_time = $student['modified_time'];
			$result->created_time = $student['created_time'];
			// $result->check_list->check_list_id = $student['student_checklist.id'];
			$result->check_list->photography_allowed = $student['Is_photography_allowed'];
			$result->check_list->special_need = $student['Is_special_need'];
			$result->check_list->special_need_type = $student['special_need_type'];
			$result->check_list->medicine = $student['Is_medicine'];
			$result->check_list->spending_money = $student['Is_spending_money'];
			$result->check_list->spending_money_amt = $student['spending_money_amt'];
			$result->check_list->allergies = $student['Is_allergies'];
			$result->check_list->allergy_details = $student['allergy_details'];
      	array_push($posts, $result);
      }
      return $posts;
	}

  	function chk_access_token($access_token){
	  $this->db->select('*');
      $this->db->from('access_token');
      $this->db->where('access_token',$access_token);
      $query = $this->db->get();
	  if($query->num_rows() > 0){
	  	$rows = '1';
	  }else{
	  	$rows = '0';
	  }
	  return $rows;
	}

	function chk_id($id){
		$this->db->select('*');
		$this->db->from('student');
		$this->db->where('id',$id);
		$query = $this->db->get();
		if($query->num_rows() > 0){
			$rows = '1';
		}else{
			$rows = '0';
		}
		return $rows;
	}


	function get_student_id_by_group_id($group_id, $teacher_id){
	  $posts = array();
 	  $this->db->select('student_id	');
      $this->db->from('group_students');
      $this->db->where('group_id',$group_id);
		  $this->db->where('created_by',$teacher_id);
      $query = $this->db->get();
      $result = $query->result_array();
      foreach($result as $value) {
      	$student_id = $value['student_id'];
      	array_push($posts, $student_id);
      }
      return $posts;
	}

	function get_student_name_by_id($student_id){
		$this->db->select('f_name,l_name');
		$this->db->from('student');
		$this->db->where('id',$student_id);
		$query = $this->db->get();
        $result = $query->result_array();
        foreach($result as $value) {
         $student_name = $value['f_name'].' '.$value['l_name'];
        }
        return $student_name;
	}

	function check_if_correct_group_id($group_id){
	  $this->db->select('*');
      $this->db->from('groups');
      $this->db->where('id',$group_id);
      $query = $this->db->get();
      //echo $this->db->last_query(); die;
      if($query->num_rows() > 0){
	  	$rows = '1';
	  }else{
		$rows =  '0';
	  }
	  return $rows;
	}

	function get_group_by_student_id($student_id){
		$this->db->select('*');
		$this->db->from('group_students');
		$this->db->where('student_id',$student_id);
		$query = $this->db->get();
        $result = $query->result_array();
        foreach($result as $value) {
         $group_id = $value['group_id'];
        }
        return $group_id;
	}

	function get_group_name_by_id($group_id){
		$this->db->select('*');
		$this->db->from('groups');
		$this->db->where('id',$group_id);
		$query = $this->db->get();

        $result = $query->result_array();
        // echo $this->db->last_query();
        // print_r($result); die;
        foreach($result as $value) {
         $group_name = $value['name'];
        }
        return $group_name;
	}

	function get_all_group(){
		$this->db->select('*');
		$this->db->from('groups');
		$query = $this->db->get();
        $result = $query->result_array();
        return $result;
	}
	function get_all_student_with_group($teacher_id){
		$posts= array();
		$this->db->select('*');
		$this->db->from('student');
		$this->db->where('created_by',$teacher_id);
		$query = $this->db->get();
        $result = $query->result_array();
        foreach($result as $value) {
        	$group_id = $this->get_group_by_student_id($value['id']);
        	$result ="";
        	$result->id = $value['id'];
        	$result->student = $value['f_name'].' '.$value['l_name'];
        	$result->group_id = $group_id;
        	$result->group_name = $this->get_group_name_by_id($group_id);

        	array_push($posts, $result);
        }
        return $posts;
	}

	function get_all_group_by_order($orderby){
		if($orderby == 1){
			$order = "asc";
		}elseif($orderby == 2){
			$order = "desc";
		}
		$this->db->select('*');
		$this->db->from('groups');
		$this->db->order_by('name',$order);
		$query = $this->db->get();
        $result = $query->result_array();
        // echo $this->db->last_query();
        return $result;
	}

	function get_all_student_with_student_orderyby($teacher_id,$orderby){
		$posts= array();
		if($orderby == 1){
			$order = "asc";
		}elseif($orderby == 2){
			$order = "desc";
		}
		$sql = "select * from student where created_by = '$teacher_id' order by concat(f_name,l_name) $order";
		$query = $this->db->query($sql);
		 // $query = $this->db->get();
		// echo $this->db->last_query();
        $result = $query->result_array();
        foreach($result as $value) {
        	$group_id = $this->get_group_by_student_id($value['id']);
        	$result ="";
        	$result->id = $value['id'];
        	$result->student = $value['f_name'].' '.$value['l_name'];
        	$result->group_id = $group_id;
        	$result->group_name = $this->get_group_name_by_id($group_id);
        	array_push($posts, $result);
        }
        return $posts;
	}

	function get_all_student_with_group_orderyby($teacher_id,$orderby){
		$posts= array();
		if($orderby == 1){
			$order = "asc";
		}elseif($orderby == 2){
			$order = "desc";
		}
		$sql = "select a.*, b.* ";
		$query = $this->db->query($sql);
		// $query = $this->db->get();
		// echo $this->db->last_query();
        $result = $query->result_array();
        foreach($result as $value) {
        	$group_id = $this->get_group_by_student_id($value['id']);
        	$result ="";
        	$result->id = $value['id'];
        	$result->student = $value['f_name'].' '.$value['l_name'];
        	$result->group_id = $group_id;
        	$result->group_name = $this->get_group_name_by_id($group_id);
        	array_push($posts, $result);
        }
        return $posts;
	}

	function check_if_correct_teacher_id($teacher_id){
		$this->db->select('*');
		$this->db->from('admin');
		$this->db->where('id',$teacher_id);
		$this->db->where('role',2);
		$query = $this->db->get();
		// echo $this->db->last_query();die;
		if($query->num_rows() > 0){
		$rows = '1';
		}else{
		$rows =  '0';
		}
		return $rows;
		}


}

?>
