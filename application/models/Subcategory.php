<?php

/**
*
*/
class Subcategory extends CI_Model
{

	function __construct()
	{
	 parent::__construct();
	}

	function get_entries(){
		$query = $this->db->get('master_subcategory');
        return $query->result();
	}

	function get_all_categories(){
		$this->db->select('*');
        $this->db->from('master_category');
        $this->db->order_by('category','asc');
        $query = $this->db->get();
       return  $query->result();
	}


	function get_category_name_by_id($id){
		$this->db->select('category');
        $this->db->from('master_category');
        $this->db->where('id',$id);
        $query = $this->db->get();
        $response = $query->result_array();
        // echo $this->db->last_query();
    	return  $response[0]['category'];
	}


	function get_all_entries_with_category_name(){
	   $this->db->select('master_subcategory.*,master_category.category');
       $this->db->from('master_subcategory');
       $this->db->join('master_category', 'master_subcategory.category_id = master_category.id', 'right outer');
       $this->db->where('master_subcategory.category_id IS NOT NULL', null, false);
       $query = $this->db->get();
      return $query->result();
	}

	function chk_access_token($access_token){
	  $this->db->select('*');
      $this->db->from('access_token');
      $this->db->where('access_token',$access_token);
      $query = $this->db->get();
     // echo $this->db->last_query();
	  if($query->num_rows() > 0){
	  	$rows = '1';
	  }else{
	  	$rows = '0';
	  }
	  return $rows;
	}


	function get_subcategory_by_category($ids){
        $this->db->select('*');
        $this->db->from('master_subcategory');
        $this->db->where_in('category_id',$ids);
         $query = $this->db->get();
         // echo $this->db->last_query();
     	// return $query->result();
         return $query;
	}



	function Chk_data_exist($name, $id){
		$this->db->select('*');
		$this->db->from('master_subcategory');
		$this->db->where('title',$name);
		$this->db->where('id !=',$id);
		//echo $this->db->last_query(); die;
		$query = $this->db->get();
		if($query->num_rows() > 0){
			$rows = '1';
		}else{
			$rows = '0';
		}
		return $rows;
	}

	function get_teacher_id_by_access_token($access_token){
	  $this->db->select('admin_id');
      $this->db->from('access_token');
      $this->db->where('access_token',$access_token);
      $query = $this->db->get();
      $result =  $query->result();
      if($query->num_rows() > 0){
      	foreach ($result as $results) {
      	$rows = $results->admin_id;
      	}
	  }else{
	  	$rows = '0';
	  }
	  return $rows;
	}


	function assign_category_group($group_id, $category_id, $teacher_id){
		$data = array(
		'master_category_id' => $category_id,
		'group_id' => $group_id,
		'teacher_id' => $teacher_id,
		'modified_time' => Time(),
		);
		$result =  $this->db->insert('group_category', $data);
		// echo $this->db->last	_query();
			if(!empty($result)){
				return  "1";
			}else{
				return "0";
			}
		}


		function delete_data($id)
		{
		$this->db->select('teacher_id');
		$this->db->from('group_category');
		$this->db->where('teacher_id',$id);
		$query = $this->db->get();
		$result =  $query->result();
	     if($query->num_rows() > 0){
			$this->db->where('teacher_id', $id);
			$this->db->delete('group_category');
		 }

		}

		function Get_sub_categroy_by_category($category_id, $grade, $teacher_id){
			$imageurl = base_url()."uploads/subcategory_images/";
			$posts = array();
		    $this->db->select('*');
		    $this->db->from('master_subcategory');
		    $this->db->where('category_id', $category_id);
			$this->db->where('grade_type', $grade);
		    $query1 = $this->db->get();
		      // echo $this->db->last_query();
		    $data1 = $query1->result_array();
		    if(!empty($data1)){
		      foreach ($data1 as $value) {
		      	$result = "";
		      	$result->sub_category_id = $value['id'];
		      	$result->title = $value['title'];
		      	$result->description = $value['description'];
		      	$result->time_duration = $value['time_duration'];
		      	if(!empty($value['image'])){
		         $result->image = $imageurl.$value['image'];
		        }else{
		         $result->image = "";
		        }
		        $result->already_saved = $this->check_if_saved_itinerary($teacher_id, $value['id']);
		      	 array_push($posts, $result);
		      }
		  }else{
		  	 $posts = "";
		  }
		    return $posts;
		}

		function check_itinerary_date($teacher_id){
			$this->db->select('itinerary_datetime');
			$this->db->from('itinerary');
			$this->db->where('admin_id', $teacher_id);
			$this->db->limit('1');
			$query = $this->db->get();
			$result = $query->result_array();
			 foreach ($result as $value) {
			   $dateitnry = $value['itinerary_datetime']; 
			 }
			 if(!empty($dateitnry)){
			 	$date = $dateitnry;
			 }else{
			 	$date = "";
			 }
				return $date;
		}

		function Get_sub_categroy_details_by_category($category_id,$grade, $teacher_id){
			$posts = array();
		    $this->db->select('*');
		    $this->db->from('master_category');
		    $this->db->where('id', $category_id);
		    $query1 = $this->db->get();
		    // echo $this->db->last_query();
		    $data1 = $query1->result_array();
		      foreach ($data1 as $value) {
		      	$allsubcats = $this->Get_sub_categroy_by_category($value['id'], $grade, $teacher_id);
		       	if(!empty($allsubcats)){
		      		$result = "";
		      		$result->id = $value['id'];
		      		$result->category_name = $this->get_category_name_by_id($value['id']);
		      		$result->sub_categories = $this->Get_sub_categroy_by_category($value['id'], $grade, $teacher_id);
		      	 	$result->date =  $this->check_itinerary_date($teacher_id);
		      	 array_push($posts, $result);
		      	 }
		      }
		    return $posts;
		}

		function check_if_saved_itinerary($teacher_id,$item_id){
		 $this->db->select('*');
     	 $this->db->from('itinerary');
         $this->db->where('master_subcategory_id',$item_id);
         $this->db->where('admin_id',$teacher_id);
         $query = $this->db->get();
         // echo $this->db->last_query();
         $result =  $query->result();
          // print_r($result);
	      if($query->num_rows() > 0){
	      	$rows = '1';
		  }else{
		  	$rows = '0';
		  }
		  return $rows;
			}

}



?>
