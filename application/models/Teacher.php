<?php

  class Teacher extends CI_Model
  {

  	function __construct()
  	{
  		parent::__construct();
  	}

    function email_verify($email)
     {
       $this ->db-> select('*');
       $this ->db-> from('admin');
       $this ->db-> where('email', $email);
       $this ->db-> limit(1);
       $query = $this->db->get();
       $result = $query->result();
       if($query->num_rows() == 1)
       {
         return $result;
       }
       else
       {
         return '0';
       }
     }


function Teacherlogin($email,$password)
 {
   $this ->db-> select('*');
   $this ->db-> from('admin');
   $this ->db-> where('email', $email);
   $this ->db-> where('password', MD5($password));
   $this ->db-> limit(1);
   $query = $this->db->get();
   if($query -> num_rows() == 1)
   {
    $teacher_details=$query->result_array();
    $id= $teacher_details[0]['id'];
    $this->db->where('id',$id);
    return $teacher_details;
   }
   else
   {
     return false;
   }
 }


function login($username, $password)
 {
   $this -> db -> select('id, email, password, role');
   $this -> db -> from('admin');
   $this -> db -> or_where('email =', $username);
   $this -> db -> where('password', MD5($password));
   $this -> db -> limit(1);
   $query = $this -> db -> get();
   if($query -> num_rows() == 1)
   {
     return $query->result();
   }
   else
   {
     return false;
   }
 }


 function chk_access_code($access_code){
   $this ->db-> select('*');
   $this ->db-> from('chaperon');
   $this ->db-> where('accesscode', $access_code);
   $query = $this->db->get();
   $result = $query->result_array();
   if (!empty($result)){
     return $result;
   }else{
     return "0";
   }
 }


 function get_access_token($id){
   $this ->db-> select('access_token');
   $this ->db-> from('access_token');
   $this ->db-> where('chaperon_id', $id);
   $query = $this->db->get();
   $result = $query->result_array();
   return $result;
 }

 function insert_accesstoken($id, $code){
     $data = array(
       'access_token' => $code,
       'chaperon_id' => $id,
     );
     $result =  $this->db->insert('access_token', $data);
     if(!empty($result)){
       return  "1";
     }else{
       return "0";
     }
   }


   function update_accesstoken($id, $code){
     $result = $this->db->update('access_token', array('access_token' => $code), array('chaperon_id' => $id));
     if(!empty($result)){
       return  "1";
     }else{
       return "0";
     }
   }

  function chk_access_code_id($id){
    $this ->db-> select('*');
    $this ->db-> from('access_token');
    $this ->db-> where('chaperon_id', $id);
    $query = $this->db->get();
    if($query -> num_rows() > 0)
    {
      return "1";
    }
    else
    {
      return "0";
    }
  }

   function get_teachername_by_id($teacherid){
    $this ->db-> select('name');
    $this ->db-> from('admin');
    $this ->db-> where('id', $teacherid);
    $query = $this->db->get();
    $result = $query->result();
     foreach ($result as $results) {
      $name = $results->name;
      }
    return $name;
   }

   function get_teachername_by_email($email){
    $this ->db-> select('name');
    $this ->db-> from('admin');
    $this ->db-> where('email', $email);
    $query = $this->db->get();
    $result = $query->result();
     foreach ($result as $results) {
      $name = $results->name;
      }
    return $name;
   }


   function check_password_for_email($email, $password){
    $this ->db-> select('password');
    $this ->db-> from('admin');
    $this ->db-> where('email', $email);
    $this ->db-> where('password', md5($password));
    $query = $this->db->get();
     // echo $this->db->last_query(); die;
    if($query -> num_rows() > 0)
    {
      return "1";
    }
    else
    {
      return "0";
    }
   }

   function get_teacher_data($access_token){
     $this ->db-> select('*');
     $this ->db-> from('access_token');
     $this ->db-> where('access_token', $access_token);
     $query = $this->db->get();
     $result = $query->result();
    //  return $result[0]->chaperon_id;
    $this ->db-> select('*');
    $this ->db-> from('chaperon');
    $this ->db-> where('id', $result[0]->chaperon_id);
    $query1 = $this->db->get();
    $result1 = $query1->result();
    return $result1[0];
   }

   function get_group_data($chaperon_id){
     $this ->db-> select('*');
     $this ->db-> from('group_chaperon');
     $this ->db-> where('chaperon_id', $chaperon_id);
     $query2 = $this->db->get();
     $result2 = $query2->result();
     $this ->db-> select('*');
     $this ->db-> from('groups');
     $this ->db-> where('id', $result2[0]->group_id);
     $query3 = $this->db->get();
     $result3 = $query3->result();
     return $result3[0];

   }

 function get_grade_detail_for_teacher($teacher_id){
     $this ->db-> select('grade_type');
     $this ->db-> from('admin');
     $this ->db-> where('id', $teacher_id);
     $query = $this->db->get();
     $result = $query->result_array();
     foreach ($result as $value) {
       $grade = $value['grade_type'];
     }
     if(!empty($grade)){
       return $grade;
      }else{
        return "";
      }
 }



 function get_itinerary_for_teacher($teacher_id){
     $this ->db-> select('*');
     $this ->db-> from('itinerary');
     $this ->db-> where('admin_id', $teacher_id);
     $query = $this->db->get();
     $result = $query->result();
     if(!empty($result)){
      return "1";
     }else{
      return "0";
     }
   
 }
   function chk_access_token($access_token){
     $this->db->select('*');
     $this->db->from('access_token');
     $this->db->where('access_token',$access_token);
     $query = $this->db->get();
     // echo $this->db->last_query();
    if($query->num_rows() > 0){
      $rows = '1';
    }else{
      $rows = '0';
    }
    return $rows;
  }

  
  function check_if_correct_teacher($teacher_id){
    $this->db->select('*');
    $this->db->from('admin');
    $this->db->where('id',$teacher_id);
    $query = $this->db->get();
      // echo $this->db->last_query();
    if($query->num_rows() > 0){
      $rows = '1';
    }else{
      $rows = '0';
    }
    return $rows;
  }

 function check_score_for_chapereone($chaperon_id){
    $this->db->select('score');
    $this->db->from('group_score');
    $this->db->where('cheperone_id',$chaperon_id);
    $query = $this->db->get();
     $result = $query->result_array();
     foreach ($result as $value) {
       $score = $value['score'];
     }
     return $score;
 }

  }// end of first class



?>
