<?php

  class Wallframe extends CI_Model
  {

  	function __construct()
  	{
  		parent::__construct();
  	}


    	 function get_chaperon_entries(){
    				$this->db->select('*');
    				$this->db->from('chaperon');
    				$query = $this->db->get();
    				return $query->result();
    	 }

       function get_group_score($id){
            $this->db->select('*');
            $this->db->from('group_score');
            $this->db->where('cheperone_id', $id);
            $query = $this->db->get();
            return $query->result();
       }

       function get_group($id){
            $this->db->select('*');
            $this->db->from('groups');
            $this->db->where('id', $id);
            $query = $this->db->get();
            return $query->result();
       }

       function get_teacher_name_by_id($teacher_id){
        $this->db->select('name');
        $this->db->from('admin');
        $this->db->where('id',$teacher_id);
        $query = $this->db->get();
        $result = $query->result_array();
        foreach($result as $value) {
         $name = $value['name'];
        }
        return $name;
       }

       function get_teacher_school_by_id($teacher_id){
        $this->db->select('school_id');
        $this->db->from('admin');
        $this->db->where('id',$teacher_id);
        $query = $this->db->get();
        $result = $query->result_array();
        foreach($result as $value) {
         $school = $this->get_school_by_id($value['school_id']);
        }
        return $school;
       }

       function get_school_by_id($school_id){
        $this->db->select('school');
        $this->db->from('school');
        $this->db->where('id',$school_id);
        $query = $this->db->get();
        $result = $query->result_array();
        foreach($result as $value) {
         $schoolname = $value['school'];
        }
        return $schoolname;
       }

       function chk_access_token($access_token){
         $this->db->select('*');
            $this->db->from('access_token');
            $this->db->where('access_token',$access_token);
            $query = $this->db->get();
           // echo $this->db->last_query();
         if($query->num_rows() > 0){
           $rows = '1';
         }else{
           $rows = '0';
         }
         return $rows;
       }
  }

?>
