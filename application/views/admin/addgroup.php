<?php $this->load->view('admin/layout/header.php'); ?>
<?php $this->load->view('admin/layout/left.php'); ?>

<?php if($this->session->flashdata('flassuccess')!='')
{
?>
<div class="alert alert-success alert-block fade in">
<button type="button" class="close close-sm" data-dismiss="alert">
<i class="fa fa-times"></i>
</button>
<h4>
<i class="fa fa-ok-sign"></i>
Success!
</h4>
<p> <?php print_r($this->session->flashdata('flassuccess')); ?></p>
</div>
<?php

}
else if($this->session->flashdata('flaserror')!='')
{
?>
<div class="alert alert-block alert-danger fade in">
                       <button type="button" class="close close-sm" data-dismiss="alert">
                           <i class="fa fa-times"></i>
                       </button>
                       <strong>Error!</strong> <?php print_r($this->session->flashdata('flaserror')); ?>
                   </div>
<?php
}
?>

 <div class="container-fluid" >
        <div class="col-sm-10 paddingleft5">
         <h5 class="headings">Add Group</h5>
         <form method="POST" role="form" id="addgroup" action='<?= base_url(); ?>index.php/Groups/addnewgroup' enctype="multipart/form-data" >

         <!-- <form id='test' action="SomePage.php" method="post"> -->

             <label class="label"> <strong> Group Names <strong> </label>
             <div class="table-responsive">
                 <table id="form_table" class="table table-bordered">
                     <tr>
                         <th>S. No</th>
                         <th>Names</th>
                     </tr>
                     <tr class='case'>
                         <td><span id='snum'>1.</span></td>
                         <td><input class="form-control" type='text' name='name[]' required/></td>
                     </tr>
                 </table>
                 <button type="button" class='btn btn-success addmore'>+ add new Group</button> <br>
             </div>
             <input type="submit" name="submit" value="Submit" class="btn btn-info">
         </form>

         <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
         <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
         <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
         <script>
             $(document).ready(function(){
                 $(".addmore").on('click', function () {
                     var count = $('table tr').length;
                     var data = "<tr class='case'><td><span id='snum" + count + "'>" + count + ".</span></td>";
                     data += "<td><input id='num" + count + "' class='form-control' type='text' name='name[]' required/><a class='remove' onclick=call("+count+")>Remove</a></td></tr>";
                     $('#form_table').append(data);
                     i++;
                 });

             });

             function call(a){
               var id_name = "#num"+a;
               console.log(id_name);
               var htn = $(id_name).parent().parent().remove();
             }


         </script>











  </div>
   <!-- /#rows -->
</div>
<!-- /#container -->
</div>
  <!-- /#page-content-wrapper -->
</div>
 <!-- /#wrapper -->
<div id="domain" class="modal fade" role="dialog"></div>
