        <div id="wrapper" class="toggled">

        <div class="logout"><a href="<?php echo base_url();?>index.php/Admin/logout">Logout</a></div>
        <!-- Sidebar -->
        <nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper" role="navigation">
            <ul class="nav sidebar-nav">
                <li class="sidebar-brand">
                    <a href="#">
                      Teacher APP
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url();?>index.php/Categories/index">Category</a>
                </li>
                <li>
                    <a href="<?php echo base_url();?>index.php/Subcategories/index">Sub Category</a>
                </li>

                <li>
                    <a href="<?php echo base_url();?>index.php/Groups/index">Groups</a>
                </li>

                <li>
                    <a href="<?php echo base_url();?>index.php/RecommandedItems/index">Recommanded Items</a>
                </li>

                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">QnA Section<span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu">
                    <li></li>
                    <li><a href="<?php echo base_url();?>index.php/Quescategories/index">Question Category</a></li>
                    <li><a href="<?php echo base_url();?>index.php/Questions/index">Questions</a></li>
                  </ul>
                </li>

            </ul>
        </nav>
        <!-- /#sidebar-wrapper -->

        <!-- Page Content -->
        <div id="page-content-wrapper">
          <button type="button" class="hamburger is-closed animated fadeInLeft" data-toggle="offcanvas">
            <span class="hamb-top"></span>
            <span class="hamb-middle"></span>
            <span class="hamb-bottom"></span>
          </button>
