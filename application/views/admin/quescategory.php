  <?php $this->load->view('admin/layout/header.php'); ?>
  <?php $this->load->view('admin/layout/left.php'); ?>

  <?php if($this->session->flashdata('flassuccess')!='')
 {
 ?>
 <div class="alert alert-success alert-block fade in">
 <button type="button" class="close close-sm" data-dismiss="alert">
 <i class="fa fa-times"></i>
 </button>
 <h4>
 <i class="fa fa-ok-sign"></i>
 Success!
 </h4>
 <p> <?php print_r($this->session->flashdata('flassuccess')); ?></p>
 </div>
 <?php

 }
 else if($this->session->flashdata('flaserror')!='')
 {
 ?>
 <div class="alert alert-block alert-danger fade in">
                         <button type="button" class="close close-sm" data-dismiss="alert">
                             <i class="fa fa-times"></i>
                         </button>
                         <strong>Error!</strong> <?php print_r($this->session->flashdata('flaserror')); ?>
                     </div>
 <?php
 }
 ?>

   <div class="container-fluid" >
          <div class="col-sm-10 paddingleft5">
              <div class="pull-right"><a href="" onclick="addnew()" data-toggle="modal" data-target="#domain"  class="changestatus icon-2 info-tooltip">Add</a></div>
           <h5 class="headings">Question Categories</h5>
         	<?php // print_r($results); ?>
  <table class= "table table-bordered" id= "dataTables-example">
  <thead>
    <th>Master Category</th>
    <th>Question Category</th>
    <th>Action</th>
  </thead>
  <tbody>
  	<?php  if(!empty($results)){?>
  	<?php foreach($results as $data){?>
    <tr id="row<?php echo $data->id;?>">
     <td><?php echo $data->category; ?></td>
     <td><?php echo $data->question_category_name; ?></td>
  	 <td><a href="javascript:void(0)" title="Edit" onclick="edit(<?php echo $data->id;?>)" data-toggle="modal" data-target="#domain" class="icon-1 info-tooltip">Edit</a>
        <a href="javascript:void(0)" title="Delete" onclick="deleteque(<?php echo $data->id;?>)" class="icon-2 info-tooltip">Delete</a>
       </td>
  	</tr>

  	<?php }}?>
  	</tbody>
  </table>
	  </div>
	   <!-- /#rows -->
 </div>
 	<!-- /#container -->
 </div>
    <!-- /#page-content-wrapper -->
 </div>
   <!-- /#wrapper -->
<div id="domain" class="modal fade" role="dialog"></div>

 <?php $this->load->view('admin/layout/bottom.php'); ?>
 <!-- <script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.4/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.4/css/jquery.dataTables.css"> -->
<script>
$(document).ready( function () {
$('#dataTables-example').DataTable({
  stateSave: true
});
});

</script>

<!-- Functions for popup anad ajax -->
<script>

    function addnew()
    {
      $.ajax({
       url:"<?php echo site_url('Quescategories/add');?>",
       success:function(data)
      {
        console.log(data);
        $('#domain').html(data);
       }
     });

    }

  function edit(id){
    var id = id;
    $.ajax({
     url:"<?php echo site_url('Quescategories/edit');?>/"+id,
       success:function(data)
      {
        $('#domain').html(data);
        $('.hide').hide();
       }
    })

  }


   function Update(id)
    {
        var id = id;
        var data = $("#edit_que_category"+id).serialize();   // edit_category is id or form
        $.ajax({
                type:"POST",
                url:"<?php echo site_url('Quescategories/update');?>/"+id,
                data:data,
                success:function(data)
                {
                  // console.log(data);
                  // $('#row'+id).html(data);
                  location.reload();

                }
              });
    }


    function deleteque(id)
    {
        if (confirm("Are you sure!") == true) {
        var id = id;
        $.ajax({
           url:"<?php echo site_url('Quescategories/delete');?>/"+id,
            success:function(data)
            {
              console.log(data);
              $('#row'+id).hide();
            }
            });
        }
    }

</script>
