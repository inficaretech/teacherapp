<?php $this->load->view('admin/layout/header.php'); ?>
<?php $this->load->view('admin/layout/left.php'); ?>

<?php if($this->session->flashdata('flassuccess')!='')
{
?>
<div class="alert alert-success alert-block fade in">
<button type="button" class="close close-sm" data-dismiss="alert">
<i class="fa fa-times"></i>
</button>
<h4>
<i class="fa fa-ok-sign"></i>
Success!
</h4>
<p> <?php print_r($this->session->flashdata('flassuccess')); ?></p>
</div>
<?php

}
else if($this->session->flashdata('flaserror')!='')
{
?>
<div class="alert alert-block alert-danger fade in">
    <button type="button" class="close close-sm" data-dismiss="alert">
    <i class="fa fa-times"></i>
    </button>
    <strong>Error!</strong> <?php print_r($this->session->flashdata('flaserror')); ?>
    </div>
<?php } ?>

 <div class="container-fluid" >
        <div class="col-sm-10 paddingleft5">
            <div class="pull-right"><a href="" onclick="addnew()" data-toggle="modal" data-target="#domain"  class="changestatus icon-2 info-tooltip">Add</a></div>
         <h5 class="headings">Recommanded Items</h5>
<table class= "table table-bordered" id= "dataTables-example">
<thead>
  <th>Title</th>
  <th>RecommandedItems</th>
  <th>Action</th>
</thead>
<tbody>
  <?php  if(!empty($results)){?>
    <td><?php print_r($results[0]->rec_title); ?></td>
  <?php foreach($results as $data){
        	$this->db->select('title');
          $this->db->from('master_subcategory');
          $this->db->where("id = $data->master_subcategory_id");
          $query = $this->db->get();
          $result[] =  $query->result();

            }}?>

    <td><?php
     foreach($result as $data){
         $list .= $data[0]->title."</br>";
     }
      echo $list; ?></td>
   <td><a href="javascript:void(0)" title="Edit" onclick="edit(<?php echo $data->id;?>)" data-toggle="modal" data-target="#domain" class="icon-1 info-tooltip">Edit</a>
      <a href="javascript:void(0)" title="Delete" onclick="deleteque(<?php echo $data->id;?>)" class="icon-2 info-tooltip">Delete</a>
     </td>
  </tr>


  </tbody>
</table>
  </div>
   <!-- /#rows -->
</div>
<!-- /#container -->
</div>
  <!-- /#page-content-wrapper -->
</div>
 <!-- /#wrapper -->
<div id="domain" class="modal fade" role="dialog"></div>

<?php $this->load->view('admin/layout/bottom.php'); ?>
<!-- <script>
$(document).ready( function () {
$('#dataTables-example').DataTable({
stateSave: true
});
});

</script> -->

<!-- Functions for popup anad ajax -->
<script>

  function addnew()
  {
    $.ajax({
     url:"<?php echo site_url('RecommandedItems/additem');?>",
     success:function(data)
    {
      // alert(data);
      console.log(data);
      $('#domain').html(data);
     }
   });

  }

function edit(id){
  var id = id;
  $.ajax({
   url:"<?php echo site_url('RecommandedItems/edit');?>/"+id,
     success:function(data)
    {
      $('#domain').html(data);
      $('.hide').hide();
     }
  })

}


 function Update(id)
  {
      var id = id;
      var data = $("#edit_que_category"+id).serialize();   // edit_category is id or form
      $.ajax({
              type:"POST",
              url:"<?php echo site_url('RecommandedItems/update');?>/"+id,
              data:data,
              success:function(data)
              {
                // console.log(data);
                // $('#row'+id).html(data);
                location.reload();

              }
            });
  }


  function deleteque(id)
  {
      if (confirm("Are you sure!") == true) {
      var id = id;
      $.ajax({
         url:"<?php echo site_url('RecommandedItems/delete');?>/"+id,
          success:function(data)
          {
            console.log(data);
            $('#row'+id).hide();
          }
          });
      }
  }

</script>
