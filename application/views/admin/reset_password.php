<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Change Password</title>
<style type="text/css">
body{ background:#7064ca;}
.chnage_password{ text-align:center; width:300px; margin:0 auto; margin-top:20%;}
.chnage_password h1{ text-align:center; font-family:Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif; color:#fff;}
.chnage_password input[type="password"]{ text-align:center; width:100%; padding:8px; border:#eee solid 1px; margin-bottom:10px; border-radius:5px;box-sizing: border-box;}
.chnage_password input[type="submit"]{ background:#000; width:100%; cursor:pointer; color:#fff;  border:none; border-radius:3px; padding: 8px 12px;text-transform: uppercase;}
.chnage_password input[type="submit"]:hover{ opacity:.7;}
</style>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript">
    $(function () {
        $("#btnSubmit").click(function () {
            var password = $("#txtPassword").val();
            var confirmPassword = $("#txtConfirmPassword").val();
			 if (password == '') {
                alert("Please enter password");
                return false;
            }else if (password.length < 8) {
                alert("Please enter minimum 8 digit password");
                return false;
            }else if (password.length > 15) {
                alert("Please enter maximum 15 digit password");
                return false;
            }
			 else if (confirmPassword == '') {
                alert("Please enter Confirm Password");
                return false;
            }
            else if (password != confirmPassword) {
                alert("Passwords do not match.");
                return false;
            }
            return true;
        });
    });
</script>
</head>

<body>
<div class="chnage_password">
	<h1>Teacherapp</h1>
    <form method="post" action="<?php echo base_url()?>index.php/Teachers/change_password/">
        <input type ="hidden" name="email" value="<?php echo base64_decode($_REQUEST['token']); ?>">
    	<input id="txtPassword" name ="password" type="password" value="" placeholder="Password" maxlength="15" />
        <input id="txtConfirmPassword" name ="cnf_password" type="password" value="" maxlength="15"  placeholder="Confirm Password" />
        <input type="submit" value="Save" id="btnSubmit" />
    </form>
</div>
</body>
</html>
